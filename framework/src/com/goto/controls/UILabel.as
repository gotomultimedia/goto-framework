//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{	
	import com.goto.display.components.GenericComponent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  23.10.2011
	 */
	public class UILabel extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UILabel(aText:String = null, aFontClass:Class = null) 
		{
			_text = aText;
			_fontClass = aFontClass;
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _textField:TextField;
		protected var _fontClass:Class;
		protected var _text:String;
		protected var _color:uint = 0x000000;
		protected var _marginTop:int = 0;
		protected var _marginBottom:int = 0;
		protected var _multiline:Boolean = false;
		protected var _fitToSize:Boolean = true;
		protected var _sizeToFit:Boolean = false;

		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		public function get fontClass():Class
		{
			return _fontClass;
		}
		
		public function set fontClass(value:Class):void
		{
			if (value === _fontClass) return;
			
			_fontClass = value;
			createTextField();
			draw();
		}
				
		public function get text():String
		{
			return _text;
		}

		public function set text(value:String):void
		{
			setText(value);
		}

		public function setText(value:String):*
		{
			if (value !== _text)
			{
				_text = value;
				
				if (isHTML(value)) 
				{
					_textField.htmlText = value;
				}
				else
				{
					_textField.text = value || "";
					_textField.textColor = _color;
				}
				
				draw();
			}
			
			return this;
		}
		
		public function get color():uint
		{
			return _color;
		}

		public function set color(value:uint):void
		{
			setColor(value);
		}
		
		public function setColor(value:uint, forceColor:Boolean = false):*
		{
			_color = value;
			
			// Cuando el texto es HTML es posible que se quiera especificar el color por el códugo HTML,
			// entonces no debe ser aplicado el color al menos que el parámetro forceColor lo indique
			if (!isHTML(_text) || forceColor)
				_textField.textColor = _color
							
			return this;
		}
		
		public function get marginTop():int
		{
			return _marginTop;
		}

		public function set marginTop(value:int):void
		{
			if (value !== _marginTop)
			{
				_marginTop = value;
				draw();
			}
		}
		
		public function get marginBottom():int
		{
			return _marginBottom;
		}

		public function set marginBottom(value:int):void
		{
			if (value !== _marginBottom)
			{
				_marginBottom = value;
				draw();
			}
		}
		
		public function get multiline():Boolean
		{
			return _multiline;
		}

		public function set multiline(value:Boolean):void
		{
			if (value !== _multiline)
			{
				_multiline = value;
				
				_textField.multiline = _multiline;
				_textField.wordWrap = _multiline;
				
				draw();
			}
		}
		
		/**
		 * @private
		 */
		public function get textField():TextField
		{
			return _textField;
		}
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static function labelWithText(aText:String, color:uint = 0x000000):*
		{
			var l:UILabel = new UILabel(aText);
			l.color = color;
			return l;
		}
		
		/**
		 * @private
		 */
		public static function labelWithFont(aText:String, aFontClass:Class, color:uint = 0x000000):*
		{
			var l:UILabel = new UILabel(aText, aFontClass);
			l.color = color;
			
			return l;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function labelFor(aView:*):*
		{
			if (!aView) 
				return this;
				
			if(!aView.hasOwnProperty("focus"))
				throw new Error("[UILabel] imcompatible view, must be a GenericComponent Subclass");
				
			onClick(aView.focus, false);
			
			return this;
		}
		
		public function sizeToFit(minTextSize:uint = 8, maxTextSize:uint = 72):*
		{
			_sizeToFit = true;
			_fitToSize = false;
			_multiline = false;
			
			if (_textField)
			{
				_textField.autoSize = TextFieldAutoSize.NONE;
				_textField.multiline = false;
				_textField.wordWrap = false;
			}
			
			draw();
			
			return this;
		}
		
		public function fitToSize():*
		{
			_sizeToFit = false;
			_multiline = false;
			_fitToSize = true;
			
			if (_textField)
			{
				_textField.autoSize = TextFieldAutoSize.NONE;
				_textField.multiline = false;
				_textField.wordWrap = false;
			}
			
			draw();
			
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			createTextField();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			// Text is require
			if (!_text)
			{
				super.draw();
				return;
			}
			
			if (_multiline)
			{
				_textField.width = __width;
				__height = _textField.height + _marginTop + _marginBottom;
				_textField.y = _marginTop;
			}
			else
			{
				if (_sizeToFit)
				{
					_textField.width = __width;
					_textField.height = __height;
					
					var f:TextFormat = _textField.getTextFormat();
					f.size = _textField.width > _textField.height ? _textField.width : _textField.height;
					_textField.setTextFormat(f);

					while (_textField.textWidth > _textField.width - 4 || _textField.textHeight > _textField.height - 6)
					{
						f.size = int(f.size) - 1;
						_textField.setTextFormat(f);
					}
					
					// Aligns vertically to middle
					_textField.y = (__height - _textField.textHeight - 6) / 2;
				}
				else if (_fitToSize)
				{
					_textField.width = _textField.textWidth + 4;
					_textField.height = _textField.textHeight + 4;
					__width = _textField.width;
					__height = _textField.height + _marginTop + _marginBottom;
					_textField.y = _marginTop;
				}
				else
				{
					__width = _textField.width;
					__height = _textField.height + _marginTop + _marginBottom;
					_textField.y = _marginTop;
				}
				
			}
			
						
			super.draw();
		}	
		
		
		/**
		 * @private
		 */
		protected function createTextField():TextField
		{
			var field:TextField;
			
			if (_textField)
			{
				removeChild(_textField);
				_textField = null;
			}
			
			if (_fontClass)
			{
				var font:* = new _fontClass();
				
				if (!font.hasOwnProperty("tf"))
					throw new Error("[UILabel] la propiedad fontClass debe contener un campo de texto con el nombre 'tf'");
					
				field = font.tf;
			}
			else
			{
				var format:TextFormat = new TextFormat();
				format.font = "Verdana";
				format.size = 12;
				//format.color = 0xFF0000;
				
				field = new TextField();
				field.defaultTextFormat = format;
				field.height = 20;
			}
			
			//field.border = true;
			field.selectable = false;
			field.type = TextFieldType.DYNAMIC;
			if (_sizeToFit)
			{
				field.autoSize = TextFieldAutoSize.NONE;
				field.multiline = false;
				field.wordWrap = false;
			}
			else
			{
				field.autoSize = TextFieldAutoSize.LEFT;
				field.multiline = _multiline;
				field.wordWrap = _multiline;
			}
			
			if (isHTML(_text)) 
			{
				field.htmlText = _text;
			}	
			else
			{
				field.textColor = _color;
				field.text = _text || "";
			}
			//field.border = true;
			
			
			_textField = field;
			addChild(_textField);
						
			return field;
		}
		
		/**
		 * @private
		 */
		protected function isHTML(target:String):Boolean
		{
			if (!target) 
				return false;
			
			return (/<[^>]*>/).test(target);
		}	
	
	}
}