//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.geom.Rectangle;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import com.greensock.TweenLite;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  17.10.2011
	 */
	public class UIButton extends UIControl implements IControl 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIButton() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static function buttonWithStates(normal:BitmapData = null, hover:BitmapData = null, down:BitmapData = null,
												innerRect:Rectangle = null,
												outerRect:Rectangle = null):*
		{
			return new UIButton().setRectangles(innerRect, outerRect).setStates(normal, hover, down);
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @todo selected, disable
		 */
		public function setStates(normal:BitmapData, hover:BitmapData = null, down:BitmapData = null):*
		{
			registerState("normal", normal).
			registerState("hover", hover).
			registerState("down", down);
			
			changeState(_currentState);
			
			if (hover)
			{
				if (!hasEventListener(MouseEvent.MOUSE_OVER)) addEventListener(MouseEvent.MOUSE_OVER, mouseHandler, false, 0, true);
				if (!hasEventListener(MouseEvent.MOUSE_OUT)) addEventListener(MouseEvent.MOUSE_OUT, mouseHandler, false, 0, true);
			}
			
			if (down)
			{
				if (!hasEventListener(MouseEvent.MOUSE_DOWN)) addEventListener(MouseEvent.MOUSE_DOWN, mouseHandler, false, 0, true);
				if (!hasEventListener(MouseEvent.MOUSE_UP)) addEventListener(MouseEvent.MOUSE_UP, mouseHandler, false, 0, true);
			}
			
			return this;
		}
		
		public function focus():*
		{
			if (!stage) return this;
			
			stage.focus = this;
			changeState("hover");
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			super.destroy();
			//
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			buttonMode = true;
			mouseChildren = false;
			
			super.onComponentReady();
		}
		
		
		
		/**
		 * @private
		 */
		protected function mouseHandler(event:MouseEvent):void
		{
			switch (event.type)
			{
				case MouseEvent.MOUSE_OVER :
					changeState("hover");
					break;
					
				case MouseEvent.MOUSE_OUT :
					changeState("normal");
					break;
				
				case MouseEvent.MOUSE_DOWN :
					changeState("down");
					break;
				
				case MouseEvent.MOUSE_UP :
					TweenLite.delayedCall(0.1, changeState, ["normal"]);
					//changeState("normal");
					break;
			}
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------			
	}
}