//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.geom.Rectangle;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import com.greensock.TweenLite;
	import com.goto.controls.UIPopover;
	import com.goto.utils.DisplayObjectUtils;
	import flash.display.DisplayObject;
	import flash.events.Event;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  17.10.2011
	 */
	public class UIPopoverButton extends UIButton
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIPopoverButton() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/** @private Guarda la referencia de la instacia de UIPopover que aparece al presionar el Boton */
		protected var _popover:UIPopover;
		/** @private Guarda la referencia de la etiqueta del Botón */
		protected var _label:UILabel;
		/** @private Guarda una referencia del contedido del Popover */
		protected var _contentView:DisplayObject;
		/** @private Guarda el valor del control */
		protected var _value:String = "";
		/** @private */
		protected var _popoverIsPresented:Boolean;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		public function get contentView():DisplayObject
		{
			return _contentView;
		}

		public function set contentView(value:DisplayObject):void
		{
			_contentView = value;
		}
		
		public function get label():UILabel
		{
			return _label;
		}
		
		/**
		 * @private
		 */
		public function get value():String
		{
			return _value;
		}
		
		public function set value(value:String):void
		{
			setValue(value);
		}
		
		public function setValue(value:String):*
		{
			if (value === _value) return;
			
			_value = value;
			_label.text = _value;
			draw();
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function get popoverIsPresented():Boolean
		{
			return _popoverIsPresented;
		}
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static function withStates(normal:BitmapData, 
										  hover:BitmapData = null, 
										  down:BitmapData = null,
										  innerRect:Rectangle = null,
										  outerRect:Rectangle = null):*
		{
			return new UIPopoverButton().setRectangles(innerRect, outerRect).setStates(normal, hover, down);
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_label.destroy();
			
			if (_popover) 
				_popover.destroy();
			
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function presentPopover():void
		{
			_popoverIsPresented = true;
			
			// Bloquea el boton temporalmente
			enabled = false;
			
			// Crea una instancia de Popover y lo configura en un estilo de cadena
			_popover = new UIPopover()
				// Cuando el Popover termina de cerrarse se destruye y se habilita el boton nuevamente
				.onOutComplete(willDestroyPopover, false, null) // once
				// Aplica las propiedades al popup
				.setProperties(propertiesForPopup())
				// Muestra el Popover
				.presentPopoverFromView(this, viewForPopover())
		}
		
		/**
		 * @private
		 */
		public function dismissPopover():void
		{
			if (!_popover) return;
			_popover.dismissPopup();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw():void
		{
			var rightSide:uint;
			
			if (_innerRect)
			{
				_label.x = _innerRect.x;
				rightSide = _view.skin.width - _innerRect.width - _innerRect.x;
			}
			
			// = _in
			__width = _label.x + _label.width + rightSide;
			__height = _view.height;
			
			/*
			// Calculates the minimun component size
			const MARGIN:uint = 3;
			
			if (__width < _label.width + MARGIN) 
				__width = _label.width + MARGIN;
			
			if (__height < _label.height + MARGIN) 
				__height = _label.height + MARGIN;
			
			*/
			
			_label.y = centerY - _label.centerY;
			
			super.draw();
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();

			onClick(presentPopover, false);
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			_value = defaultValue();
			_label = UILabel.labelWithFont(_value, fontForLabel()).addInto(this);
			
			// Disable user interactions of label
			_label.enabled = false;
		}
		
		/**
		 * @private
		 */
		protected function fontForLabel():Class
		{
			return null;
		}
		
		/**
		 * @private
		 */
		protected function defaultValue():String
		{
			return "UIPopoverButton";
		}
		
		/**
		 * @private
		 */
		protected function viewForPopover():DisplayObject
		{
			return _contentView;
		}
		
		/**
		 * @private
		 * Destruye el popover cuando se cierra a sí mismo y abilita la intereacción del boton
		 */
		protected function willDestroyPopover():void
		{
			_popoverIsPresented = false;
			_popover.destroy();
			_popover = null;
			enabled = true;
		}
		
		/**
		 * @private
		 */
		protected function propertiesForPopup():Object
		{
			return null;
		}
	}
}