//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import com.goto.display.MultiStateView;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  24.10.2011
	 */
	public class UIControl extends MultiStateView 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIControl() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _marginRight:int;
		protected var _marginLeft:int;
		protected var _marginTop:int;
		protected var _marginBottom:int;
		protected var _paddingRight:int;
		protected var _paddingLeft:int;
		protected var _paddingTop:int;
		protected var _paddingBottom:int;
		protected var _tabIndex:int = -1;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		/** The width of the top margin. */
		public function get marginTop():int
		{
			return _marginTop;
		}
		
		/** @private */
		public function set marginTop(value:int):void
		{
			if (value !== _marginTop)
			{
				_marginTop = value;
				draw();
			}
		}
		
		/** The width of the right margin. */
		public function get marginRight():int
		{
			return _marginRight;
		}
		
		/** @private */
		public function set marginRight(value:int):void
		{
			if (value !== _marginRight)
			{
				_marginRight = value;
				draw();
			}
		}
		
		/** The width of the bottom margin. */
		public function get marginBottom():int
		{
			return _marginBottom;
		}
		
		/** @private */
		public function set marginBottom(value:int):void
		{
			if (value !== _marginBottom)
			{
				_marginBottom = value;
				draw();
			}
		}
		
		/** The width of the left margin. */
		public function get marginLeft():int
		{
			return _marginLeft;
		}
		
		/** @private */
		public function set marginLeft(value:int):void
		{
			if (value !== _marginLeft)
			{
				_marginLeft = value;
				draw();
			}
		}
		
		/** The width of the top padding. */
		public function get paddingTop():int
		{
			return _paddingTop;
		}

		/** @private */
		public function set paddingTop(value:int):void
		{
			if (value !== _paddingTop)
			{
				_paddingTop = value;
				draw();
			}
		}
		
		/** The width of the bottom padding. */
		public function get paddingBottom():int
		{
			return _paddingBottom;
		}
		
		/** @private */
		public function set paddingBottom(value:int):void
		{
			if (value !== _paddingBottom)
			{
				_paddingBottom = value;
				draw();
			}
		}
		
		/** The width of the right padding. */
		public function get paddingRight():int
		{
			return _paddingRight;
		}
		
		/** @private */
		public function set paddingRight(value:int):void
		{
			if (value !== _paddingRight)
			{
				_paddingRight = value;
				draw();
			}
		}
		
		/** The width of the left padding. */
		public function get paddingLeft():int
		{
			return _paddingLeft;
		}
		
		/** @private */
		public function set paddingLeft(value:int):void
		{
			if (value !== _paddingLeft)
			{
				_paddingLeft = value;
				draw();
			}
		}
		

		override public function get tabIndex():int
		{
			return _tabIndex;
		}

		override public function set tabIndex(value:int):void
		{
			_tabIndex = value;
		}
				
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}

		/**
		 * Defines the width of an element’s inner-view padding.
		 * @param top Defines the width of the top padding of an view.
		 * @param right Defines the width of the right padding of an view.
		 * @param bottom Defines the width of the bottom padding of an view.
		 * @param left Defines the width of the left padding of an view.
		 * @param needsLayout Defines if the view needs redraws.
		 * @return
		 */
		public function setPadding(top:int = 0, right:int = 0, bottom:int = 0, left:int = 0, needsLayout:Boolean = true):*
		{
			_paddingTop = top;
			_paddingRight = right;
			_paddingBottom = bottom;
			_paddingLeft = left;
			
			if (needsLayout) draw();
			
			return this;
		}
		
		/**
		 * Defines the width of an view’s outer-view margin.
		 * @param top Defines the width of the top margin of a view.
		 * @param right Defines the width of the right margin of a view.
		 * @param bottom Defines the width of the bottom margin of a view.
		 * @param left Defines the width of the left margin of a view.
		 * @param needsLayout Defines if the view needs redraws.
		 * @return
		 */
		public function setMargin(top:int = 0, right:int = 0, bottom:int = 0, left:int = 0, needsLayout:Boolean = true):*
		{
			_marginTop = top;
			_marginRight = right;
			_marginBottom = bottom;
			_marginLeft = left;
			
			if (needsLayout) draw();
			
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
				
		/**
		 * @inheritDoc
		 */
		/*
		override public function draw ():void
		{
			var i:uint = numChildren;
			var widerChild:Number = 0;
			var tallerChild:Number = 0;
			
			while (--i)
			{
				widerChild = Math.max(widerChild, getChildAt(i).width);
				tallerChild = Math.max(tallerChild, getChildAt(i).height);
			}
						
			__minWidth = _paddingRight + _paddingLeft + widerChild;
			__minHeight = _paddingTop + _paddingBottom + tallerChild;
						
			super.draw();
		}*/		
	}
}

