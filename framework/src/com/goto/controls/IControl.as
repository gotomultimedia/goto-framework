//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{

	/**
	 * Interface describing the contract for...
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  24.10.2011
	 */
	public interface IControl
	{
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		function focus():*
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
	}

}

