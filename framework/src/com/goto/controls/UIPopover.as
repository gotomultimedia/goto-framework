//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import flash.display.Bitmap;
	import flash.display.Stage;
	//import com.goto.utils.Console;
	import com.greensock.TweenLite;
	import flash.geom.Point;
	import fl.motion.easing.Back;
	import flash.geom.Rectangle;
	import flash.display.DisplayObject;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  27.10.2011
	 */
	public class UIPopover extends UIPopup 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const ARROW_DIRECTION_NONE:uint = 0;	// 0
		public static const ARROW_DIRECTION_UP:uint = 1;	// 1
		public static const ARROW_DIRECTION_DOWN:uint = 2;	// 2
		public static const ARROW_DIRECTION_LEFT:uint = 4;	// 4
		public static const ARROW_DIRECTION_RIGHT:uint = 8;	// 8
		public static const ARROW_DIRECTION_ANY:uint = ARROW_DIRECTION_UP | ARROW_DIRECTION_DOWN | 
													   ARROW_DIRECTION_LEFT | ARROW_DIRECTION_RIGHT; // 15
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIPopover(stageRef:Stage = null) 
		{
			super(stageRef);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/** @private */
		protected var _arrow:PopoverArrow;
		/** @private */
		protected var _viewRef:GenericComponent;
		/** @private */
		protected var _arrowDirection:uint = ARROW_DIRECTION_ANY;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function presentPopoverFromView(aView:GenericComponent, aContentView:DisplayObject = null):*
		{
			if (!aView)
				throw new Error("[Popup] El argumento aView y contentView no puede ser indefinido");
			
			if (!aView.stage)
				throw new Error("[UIPopover] el argumento viewRef debe estar agregadp a las lista de visualización");
				
			_viewRef = aView;
			_stage = _viewRef.stage;
			
			contentView = aContentView;
			
			if (!contentView)
				trace ("[Popup] Warning: There isn't contentView");
			
			_isVisible = true;
			
			presentBackground();
			_stage.addChild(this);
			draw();
			
			if (!_animation.duration) 
				return this;
			
			startsInAnimation();
			
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function init():void
		{
			_backgroundAlpha = 0.0;
			animation.timingFuctionForIn = Back.easeOut;
			//animation.timingFuctionForOut = Back.easeIn;
			animation.duration = 0.5;
			
			super.init();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_arrow = new PopoverArrow(BPopoverArrowSkin).addInto(this);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			if (!_viewRef) return;
			
			super.draw();
			
			const BCX:int = _background.centerX;
			const BCY:int = _background.centerY;
			const BW:int = _background.width;
			const BH:int = _background.height;
			
			const REF_FRAME:Rectangle = _viewRef.globalFrame;
			const REF_CX:int = REF_FRAME.x + _viewRef.centerX; // Ref Global Center X
			const REF_CY:int = REF_FRAME.y + _viewRef.centerY; // Ref Global Center Y
			
			const SEPARATION:int = 14;
			const MARGIN:int = 20;
			const ARROW_MARGIN:int = 20;
			
			var pos:Point = new Point(REF_CX - centerX); // center by default
			var arrowPosX:uint;
					
			// Top
			if (REF_CY < BCY)
			{
				setPosX(REF_CX < BCX);
				setPosY(true);
			}
			// Bottom
			else
			{
				setPosX(REF_CX < BCX);
				setPosY(false);
			}
			
			function setPosX (isLeft:Boolean):void
			{
				// Left
				if (isLeft)
				{
					pos.x = pos.x < MARGIN ? MARGIN : pos.x;
					arrowPosX = REF_FRAME.x - pos.x + _viewRef.centerX;
					arrowPosX = (arrowPosX < ARROW_MARGIN) ? ARROW_MARGIN : arrowPosX;
				}
				// Right
				else
				{
					pos.x = pos.x + __width > BW - MARGIN ? BW - MARGIN - __width : pos.x;
					arrowPosX = REF_FRAME.x - pos.x + _viewRef.centerX;
					arrowPosX = (arrowPosX > __width - ARROW_MARGIN) ? __width - ARROW_MARGIN : arrowPosX;
				}
			}
			
			function setPosY (isTop:Boolean):void
			{
				// Up
				if (REF_FRAME.y > __height + SEPARATION + MARGIN)
				{
					pos.y = REF_FRAME.y - __height - SEPARATION;
					_arrow.direction = ARROW_DIRECTION_DOWN;
					_arrow.y =  __height;
				}
				// Down
				else
				{
					pos.y = REF_FRAME.y + REF_FRAME.height + SEPARATION;
					_arrow.direction = ARROW_DIRECTION_UP;
					_arrow.y =  0;
				}
			}
			
			_arrow.x = arrowPosX || centerX;
			move(pos.x, pos.y);	
		}
		
		/**
		 * @private
		 */
		override protected function startsInAnimation():void
		{
			var snap:Bitmap = takeSnapShot();
			
			_stage.addChild(snap);
			_stage.removeChild(this);
			
			var fromScale:Number = 0.0,
				fromX:Number = _viewRef.globalX + _viewRef.center.x,
				fromY:Number = _viewRef.globalY + _viewRef.center.y;
			
			TweenLite.from(snap, _animation.duration, {alpha:0, 
													   transformMatrix:{x:fromX, y:fromY, scaleX:fromScale, scaleY:fromScale}, 
													   ease:_animation.timingFuctionForIn, 
													   onComplete:tweenComplete, 
													   onCompleteParams:[this]});
			
			function tweenComplete (target:UIPopup):void
			{
				_stage.removeChild(snap);
				_stage.addChild(target);
				
				snap.bitmapData.dispose();
				snap = null;
				
				dispatchEvent(new Event(ANIMATION_COMPLETE));
				dispatchEvent(new Event(ANIMATION_COMPLETE_IN));
			}
		}
		
		/**
		 * @private
		 */
		override protected function startsOutAnimation():void
		{
			var snap:Bitmap = takeSnapShot();	
			
			_stage.addChild(snap);
			_stage.removeChild(this);

			var toScale:Number = 0.0,
				toX:Number = _viewRef.globalX + _viewRef.center.x,
				toY:Number = _viewRef.globalY + _viewRef.center.y;
			
			TweenLite.to(snap, _animation.duration, {alpha:0, 
														   transformMatrix:{x:toX, y:toY, scaleX:toScale, scaleY:toScale}, 
														   ease:_animation.timingFuctionForOut, 
														   onComplete:tweenComplete});

			function tweenComplete ():void
			{
				_isVisible = false;
				_stage.removeChild(snap);
				snap.bitmapData.dispose();
				snap = null;
				
				dispatchEvent(new Event(ANIMATION_COMPLETE));
				dispatchEvent(new Event(ANIMATION_COMPLETE_OUT));
			}
		}	
	}
}

/**
 * PopoverArrow
 * 
 * @langversion ActionScript 3
 * @playerversion Flash 9.0.0
 * 
 * @author fenixkim
 * @since  27.10.2011
 */

import com.goto.display.components.GenericComponent;
import com.goto.controls.UIPopover;
import flash.display.Bitmap;
import flash.display.BitmapData;
	
internal class PopoverArrow extends GenericComponent
{
	
	//---------------------------------------
	// CLASS CONSTANTS
	//---------------------------------------
	
	/**
	 * @private
	 */
		
	//---------------------------------------
	// PRIVATE VARIABLES
	//---------------------------------------
	
	/** @private */
	private var _skin:Bitmap;
	private var _skinClass:Class;
	private var _direction:uint;
	
	/**
	 * @constructor
	 */
	public function PopoverArrow(skinClass:Class, direction:uint = 1)
	{
		_direction = direction;
		_skinClass = skinClass;
		
		super();
	}
	
	//---------------------------------------
	// GETTER / SETTERS
	//---------------------------------------
	
	/**
	 * @private
	 */
	override public function get centerX():Number
	{
		return _skin.width >> 1 + 0.5 | 0;
	}
	
	public function get direction():uint
	{
		return _direction;
	}

	public function set direction(value:uint):void
	{
		if (value !== _direction)
		{
			_direction = value;
			draw();
		}
	}
	
	//---------------------------------------
	// PROTECTED METHODS
	//---------------------------------------
	
	/**
	 * @inheritDoc
	 */
	override protected function addChildren():void
	{
		super.addChildren();
		
		//Add subviews here
		_skin = new Bitmap((new _skinClass()) as BitmapData);
		addChild(_skin);
	}
	
	/**
	 * @inheritDoc
	 */
	override public function draw ():void
	{
		const W:uint = __width;
		const H:uint = __height;
		const X:uint = W >> 1 + 0.5 | 0; // 600% Faster than Math.round((W / 2));
		const Y:uint = H >> 1 + 0.5 | 0; // Same
		
		// y
		switch (_direction)
		{
			case UIPopover.ARROW_DIRECTION_UP :
				_skin.x = -X;
				_skin.y = -H;
				break;
			case UIPopover.ARROW_DIRECTION_DOWN :
				_skin.x = -X;
				_skin.y = H;
				_skin.scaleY = -1;
				break;
			case UIPopover.ARROW_DIRECTION_LEFT :
				_skin.rotation = -90;
				_skin.x = -H;
				_skin.y = X;
				break;
			case UIPopover.ARROW_DIRECTION_RIGHT :
				_skin.rotation = 90;
				_skin.x = H;
				_skin.y = -X;
				break;
		}
		
		_skin.visible = _direction != UIPopover.ARROW_DIRECTION_NONE;
		
		super.draw();
	}

}