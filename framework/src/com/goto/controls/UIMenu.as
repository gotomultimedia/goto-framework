//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import com.goto.display.layout.VLayout;
	import flash.events.MouseEvent;
	import flash.display.DisplayObject;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  26.11.2011
	 */
	public class UIMenu extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const ITEM_CLICKED:String = "itemClicked";
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIMenu() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _data:Array;
		protected var _items:Array;
		protected var _layout:*;
		protected var _selected:UIMenuItem;
		protected var _selectedIndex:int = -1;
		protected var _fireEvent:Boolean = true;
		protected var _menuItemStates:Object = {};
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		public function get data():Array
		{
			return _data;
		}
		
		/**
		 * @private
		 */
		public function get items():Array
		{
			return _items;
		}
		
		
		public function get selected():UIMenuItem
		{
			return _selected;
		}
		
		/**
		 * @private
		 */
		public function get selectedIndex():int
		{
			return _selectedIndex;
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			destroyItems(_items);
			_items = null;
			
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function click(index:uint, fireEvent:Boolean = true):void
		{
			_fireEvent = fireEvent;
			
			// Fix index offset
			if (index > _items.length-1) 
				index = _items.length-1;
			
			_items[index].dispatchEvent(new MouseEvent(MouseEvent.CLICK)); // CLICK
		}
		
		/**
		 * @private
		 */
		public function onItemClicked(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ITEM_CLICKED, listener, sendEventToListener, args, once);
			return this;
		}
		
		/**
		 * @private
		 */
		public function setData(value:Array):*
		{
			_data = value;
			createItems();
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function setClassesForItemStates(normal:Class, hover:Class = null, down:Class = null, selected:Class = null):*
		{
			_menuItemStates = {
				normal: normal,
				hover: hover,
				down: down,
				selected: selected
			}
			
			if (_items)
			{
				applyMenuItemStates();
			}
			
			return this;
		}
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function menuItemClicked(item:UIMenuItem):void
		{
			if (_selected && _selected != item)
			{
				_selected.changeState("normal");
			}

			_selected = item;
			_selectedIndex = _selected.index;
			_selected.changeState("selected");
			
			if (_fireEvent) 
				dispatchEvent(new Event(ITEM_CLICKED)); // ITEM_CLICKED
				
			_fireEvent = true;
		}
		
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_layout = layoutForMenu();
			addChild(_layout);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			__width = _layout.width;
			__height = _layout.height;
			
			super.draw();
		}
		
		/**
		 * @private
		 */
		protected function createItems():void
		{	
			// destroy items first
			destroyItems(_items);
			_items = null;
			
			var index:uint;
			var menuItem:UIMenuItem;
			
			for (var p:String in _data)
			{
				index = uint(p);
				
				menuItem = itemForRowAtIndex(index);
				
				if (!_items) _items = [];
				_items.push(menuItem);
			
				menuItem.addInto(_layout)
					.setIndex(index)
					.onClick(menuItemClicked, false, [menuItem]);
			}
			
			draw();
		}
		
		/**
		 * @private
		 */
		protected function layoutForMenu():*
		{
			return new VLayout();
		}
					
		/**
		 * @private
		 */
		protected function itemForRowAtIndex(index:uint):UIMenuItem
		{				
			return new UIMenuItem()
				.setValue(_data[index])
				.setStates(
					stateForItemAtIndex("normal", index),
					stateForItemAtIndex("hover", index),
					stateForItemAtIndex("down", index),
					stateForItemAtIndex("selected", index)
				);
		}
		
		/**
		 * @private
		 */
		protected function stateForItemAtIndex(stateName:String, index:uint):DisplayObject
		{
			var state:Class = _menuItemStates[stateName];
			
			if (state) 
				return new state;
			
			return null;
		}
		
		/**
		 * @private
		 */
		protected function applyMenuItemStates():void
		{
			if (!_items) return;
			
			var index:uint;
			
			for (var p:String in _items)
			{
				index = uint(p);
				
				_items[index].setStates(
					stateForItemAtIndex("normal", index),
					stateForItemAtIndex("hover", index),
					stateForItemAtIndex("down", index),
					stateForItemAtIndex("selected", index)
				)
			}
		}	
	}
}