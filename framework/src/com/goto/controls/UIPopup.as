//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import com.goto.display.components.GenericComponent;
	import com.goto.display.*;
	import flash.geom.Rectangle;
	import flash.display.Stage;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import fl.motion.easing.Exponential;
	import flash.display.Bitmap;
	import flash.events.Event;
	import com.greensock.TweenLite;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.TransformMatrixPlugin;
	import flash.events.MouseEvent;
	
	/**
	 * Crea un Popup
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  15.10.2011
	 */
	public class UIPopup extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static const ANIMATION_COMPLETE:String = "animationComplete";
		public static const ANIMATION_COMPLETE_IN:String = "animationCompleteIn";
		public static const ANIMATION_COMPLETE_OUT:String = "animationCompleteOut";
		public static const ANIMATION_START:String = "animationStart";
		public static const ANIMATION_START_IN:String = "animationStartIn";
		public static const ANIMATION_START_OUT:String = "animationStartOut";
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIPopup(stageRef:Stage = null) 
		{
			TweenPlugin.activate([TransformMatrixPlugin]);
			_stage = stageRef;
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/**
		 * @private
		 */
		protected var _holder:Sprite;
		protected var _stage:Stage;
		protected var _background:PopupBackground;
		protected var _backgroundColor:uint = 0;
		protected var _backgroundAlpha:Number = 0.3;
		protected var _base:ScalableObject;
		protected var _isVisible:Boolean;
		protected var _contentView:DisplayObject;
		protected var _contentMargin:Object = [0, 0, 0, 0];
		protected var _dismissOnBackgroundClick:Boolean = true;
		protected var _animation:Object = 
		{
			duration: 0.7,
			timingFuctionForIn: Exponential.easeOut,
			timingFuctionForOut: Exponential.easeIn
		};
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		public function get isVisible():Boolean
		{
			return _isVisible;
		}

		public function get contentView():DisplayObject
		{
			return _contentView;
		}

		public function set contentView(value:DisplayObject):void
		{
			if (value !== _contentView)
			{
				_contentView = value;
				addChild(_contentView);
			}
		}
		
		public function get backgroundColor():uint
		{
			return _backgroundColor;
		}

		public function set backgroundColor(value:uint):void
		{
			if (value !== _backgroundColor)
			{
				_backgroundColor = value;
			}
		}
		
		public function get backgroundAlpha():Number
		{
			return _backgroundAlpha;
		}

		public function set backgroundAlpha(value:Number):void
		{
			if (value !== _backgroundAlpha)
			{
				_backgroundAlpha = value;
			}
		}
		
		/**
		 * Define los márgenes para el contenido del popup.
		 * Si value es un Número el márgen será ese mismo en todos sus lados.
		 * Si value es un Arreglo, debe estar especificado en el orden de la dirección de la manecillas del reloj: [top, right, bottom, left] ó [top, right]
		 * Si value es un Objeto debe contener las propiedades: top, right, bottom y left: Eje: {top:10, bottom:10}
		 */
		public function get contentMargin():Object
		{
			return _contentMargin;
		}
		
		/** @private */
		public function set contentMargin(value:Object):void
		{
			if (value !== _contentMargin)
			{
				var p:String;
				
				// Null
				if (value == null)
				{
					return;
				}
				// Number
				if (value is Number)
				{
					_contentMargin = [value, value, value, value];
				}
				// Array
				else if (value.hasOwnProperty("length"))
				{
					for (p in value)
					{
						_contentMargin[p] = value[p];
					}
				}
				// Object
				else
				{
					if (value.hasOwnProperty("top")) 	_contentMargin[0] = value.top;
					if (value.hasOwnProperty("right")) 	_contentMargin[1] = value.right;
					if (value.hasOwnProperty("bottom")) _contentMargin[2] = value.bottom;
					if (value.hasOwnProperty("left")) 	_contentMargin[3] = value.left;
				}
			}
		}
		
		/**
		 * Returna un objeto con las propiedades:
		 * duration: Duración en segundos de la animación. Por defecto es 0 sin animación.
		 * timingFuctionForIn: Una función de tiempo como lo es Exponential.easeIn para la animación de entrada. Por defecto es Exponential.easeInOut
		 * timingFuctionForOut: Una función de tiempo como lo es Exponential.easeOut para la animación de salida. Por defecto es Exponential.easeIn
		 * @private
		 */
		public function get animation():Object
		{
			return _animation;
		}
		
		public function get dismissOnBackgroundClick():Boolean
		{
			return _dismissOnBackgroundClick;
		}

		public function set dismissOnBackgroundClick(value:Boolean):void
		{
			_dismissOnBackgroundClick = value;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
			
		/**
		 * @private
		 */
		public function presentPopupWithContentView(aView:DisplayObject, stageRef:Stage = null):*
		{
			if (!aView)
				throw new Error("[Popup] view no puede ser indefinido");
			
			contentView = aView;
			
			return presentPopupFromStage(stageRef);
		}
		
		/**
		 * @private
		 */
		public function presentPopupFromStage(stageRef:Stage = null):*
		{
			if (stageRef)
				_stage = stageRef;
				
			if (!_stage)
				throw new Error("[Popup] Stage no puede ser null");
			
			if (!_contentView)
				trace ("[Popup] Warning: There isn't contentView");
				
			_isVisible = true;
			
			presentBackground();
			_stage.addChild(this);
			draw();
			
			if (!_animation.duration) 
				return this;
			
			startsInAnimation();
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function dismissPopup():*
		{
			// Si no es visible ó no esta agregado al escenario se debe salir de la función
			// Esto evita que se haga una transición de salida si el objeto esta en alguna de las
			// situaciones anteriores. Suele suceder cuando se hace click rápidamente
			if (!_isVisible || !this.stage) 
				return;
				
			_isVisible = false;
			
			// No animation
			if (!_animation.duration)
			{
				dismissBackground();
				_stage.removeChild(this);	
				_stage = null;
			}
			else
			{
				startsOutAnimation();
				
				dismissBackground();
			}
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_background.destroy();
			_base.destroy();
			super.destroy();	
		}
	    
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		public function onInComplete(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_COMPLETE_IN, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onOutComplete(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_COMPLETE_OUT, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onAnimationComplete(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onInStart(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_START_IN, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onOutStart(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_START_OUT, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onAnimationStart(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(ANIMATION_START, listener, sendEventToListener, args, once);
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * Maneja el click para la base del Popup.
		 * Si se hace click por fuera de los limites de 'frame', entonces se entiene que se dió por fuera del Popup.
		 * A pesar que la sombra hace parte del Popup no se cuenta como zona interactiva como tal
		 * @private
		 */
		protected function baseClicked(event:MouseEvent):void
		{
			event.stopImmediatePropagation();
					
			if (_dismissOnBackgroundClick && (event.localX < 0 ||
				event.localY < 0 ||
				event.localX > __width ||
				event.localY > __height))
			{
				dismissPopup();
			}
		}
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_base = ScalableObject.fromBitmapData(new BPopupBaseSkin(0, 0), 
												  new Rectangle(73, 62, 20, 20), 
												  new Rectangle(33, 22, 100, 100));
			
			// La base puede ser interactiva
			_base.setProperties({mouseEnabled: true}).onClick(baseClicked, true);

			addChild(_base);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			if (!_isVisible) return;
			
			// Gets margins
			var t:Number = _contentMargin[0],
				r:Number = _contentMargin[1],
				b:Number = _contentMargin[2],
				l:Number = _contentMargin[3];
			
			if (_contentView) 
			{
				_contentView.x = l;
				_contentView.y = t;
				__width = (_contentView.width + r + l) + 0.5 | 0;		// Round 
				__height = (_contentView.height + t + b) + 0.5 | 0;		// Round
			}
			
			_base.setSize(__width, __height);
			move((_background.width - __width) * 0.5, (_background.height - __height) * 0.5);
			
			super.draw();
		}
		
		/**
		 * @private
		 */
		protected function takeSnapShot():Bitmap
		{
			var t:Number = 0, r:Number = 0, b:Number = 0, l:Number = 0;
			
			if (_base.outerRect)
			{
				t = _base.outerRect.y;
				r = _base.skin.width - (_base.outerRect.x + _base.outerRect.width);
				b = _base.skin.height - (_base.outerRect.y + _base.outerRect.height);
				l = _base.outerRect.x;
			}
			
			var snap:Bitmap = Rasterize.snapShot(this, 				// target
									  			 __width + r + l, 	// width
									  			 __height + b + t, 	// height
									  			 l, t, true, true);	// offsetx, offsety, smoothing, transparet
			snap.x = x - l;
			snap.y = y - t;
				
			return snap;
		}
		
		/**
		 * @private
		 */
		protected function startsInAnimation():void
		{   
			dispatchEvent(new Event(ANIMATION_START));
			dispatchEvent(new Event(ANIMATION_START_IN));
			
			var snap:Bitmap = takeSnapShot();
			
			_stage.addChild(snap);
			_stage.removeChild(this);
			
			var fromScale:Number = 0.5,
				fromX:Number = (_background.width - (snap.bitmapData.width * fromScale)) * 0.5,
				fromY:Number = (_background.height - (snap.bitmapData.height * fromScale)) * 0.5;
			
			TweenLite.from(snap, _animation.duration, {alpha:0, 
													   transformMatrix:{x:fromX, y:fromY, scaleX:fromScale, scaleY:fromScale}, 
													   ease:_animation.timingFuctionForIn, 
													   onComplete:tweenComplete, 
													   onCompleteParams:[this]});
			
			function tweenComplete (target:UIPopup):void
			{
				_stage.removeChild(snap);
				_stage.addChild(target);
				
				snap.bitmapData.dispose();
				snap = null;
				
				dispatchEvent(new Event(ANIMATION_COMPLETE));
				dispatchEvent(new Event(ANIMATION_COMPLETE_IN));
			}
		}
		
		/**
		 * @private
		 */
		protected function startsOutAnimation():void
		{
			dispatchEvent(new Event(ANIMATION_START));
			dispatchEvent(new Event(ANIMATION_START_OUT));
			
			var snap:Bitmap = takeSnapShot();	
			
			_stage.addChild(snap);
			_stage.removeChild(this);

			var toScale:Number = 0.5,
				toX:Number = (_background.width - (snap.bitmapData.width * toScale)) * 0.5,
				toY:Number = (_background.height - (snap.bitmapData.height * toScale)) * 0.5;
			
			TweenLite.to(snap, _animation.duration * 0.5, {alpha:0, 
														   transformMatrix:{x:toX, y:toY, scaleX:toScale, scaleY:toScale}, 
														   ease:_animation.timingFuctionForOut, 
														   onComplete:tweenComplete});

			function tweenComplete ():void
			{
				_isVisible = false;
				_stage.removeChild(snap);
				snap.bitmapData.dispose();
				snap = null;
				
				dispatchEvent(new Event(ANIMATION_COMPLETE));
				dispatchEvent(new Event(ANIMATION_COMPLETE_OUT));
			}
		}
				
		/**
		 * @private
		 */
		protected function presentBackground():void
		{
			_background = new PopupBackground(_stage);
			_background.color = _backgroundColor;
			_background.maxAlpha = _backgroundAlpha;
			_background.show(_animation.duration).onDraw(draw, false);
			
			if (_dismissOnBackgroundClick) 
			{
				_background.onClick(dismissPopup, false, null);
			}
				
		}
		
		/**
		 * @private
		 */
		protected function dismissBackground():void
		{
			_background.hide(_animation.duration);
		}
		
	}
}