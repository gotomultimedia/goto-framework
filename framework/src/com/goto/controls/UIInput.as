//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFieldType;
	import flash.events.FocusEvent;
	import flash.events.TextEvent;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  22.10.2011
	 */
	public class UIInput extends UIControl implements IControl
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const STATE_NORMAL:String = "normal";
		public static const STATE_ERROR:String = "error";
		public static const STATE_FOCUSED:String = "focused";
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIInput() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _inputField:TextField;
		protected var _placeholderField:TextField;
		protected var _fontClass:Class;
		protected var _inputNormalColor:uint = 0x636363;
		protected var _inputFocusedColor:uint = 0x000000;
		protected var _inputErrorColor:uint = 0xff0000;
		protected var _placeholderColor:uint = 0xc8c8c8;
		protected var _placeholder:String;
		protected var _displayAsPassword:Boolean;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override public function set enabled(value:Boolean):void
		{
			_inputField.selectable = value;
			
			super.enabled = value;
		}
		
		public function get inputField():TextField
		{
			return _inputField;
		}
		
		public function get placeholderField():TextField
		{
			return _placeholderField;
		}
		
		public function get fontClass():Class
		{
			return _fontClass;
		}

		public function set fontClass(value:Class):void
		{
			if (value === _fontClass) return;
			
			_fontClass = value;
			createTextFields();
			draw();
		}
		
		public function get placeholder():String
		{
			return _placeholder;
		}

		public function set placeholder(value:String):void
		{
			if (value !== _placeholder)
			{
				_placeholder = value;
				_placeholderField.text = _placeholder;
				
				checkPlaceholderVisibility();
			}
		}
		
		public function get value():String
		{
			return _inputField.text;
		}

		public function set value(value:String):void
		{
			if (value !== _inputField.text)
			{
				_inputField.text = value;
				changeState(STATE_NORMAL);
			}
		}
		
		public function get displayAsPassword():Boolean
		{
			return _displayAsPassword;
		}

		public function set displayAsPassword(value:Boolean):void
		{
			if (value !== _displayAsPassword)
			{
				_displayAsPassword = value;
				_inputField.displayAsPassword = _displayAsPassword;
			}
		}
		
		override public function set tabIndex(value:int):void
		{
			if (value !== _tabIndex)
			{
				_tabIndex = value;
				_inputField.tabIndex = _tabIndex;
			}
		}
		
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static function inputWithStates(normal:Object = null, focused:Object = null, error:Object = null, 
											   innerRect:Rectangle = null, 
											   outerRect:Rectangle = null):*
		{
			return new UIInput().setStates(normal, focused, error).setRectangles(innerRect, outerRect, false);
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function setStates(normal:Object, focused:Object = null, error:Object = null):*
		{
			registerState(STATE_NORMAL, normal);
			if (focused) registerState(STATE_FOCUSED, focused);
			if (error) registerState(STATE_ERROR, error);
			
			changeState(STATE_NORMAL);
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function setInputColors(normal:uint = 0x636363, focused:uint = 0x000000, error:uint = 0xff0000, placeholder:uint = 0xc8c8c8):*
		{
			_inputNormalColor = normal;
			_inputFocusedColor = focused;
			_inputErrorColor = error;
			_placeholderColor = placeholder;
			
			_inputField.textColor = _inputNormalColor;
			_placeholderField.textColor = _placeholderColor;
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function changeState(stateName:String):*
		{
			super.changeState(stateName);
			
			if (!_inputField) return;
			
			switch (stateName)
			{
				case STATE_NORMAL :
					_inputField.textColor = _inputNormalColor;
					_placeholderField.textColor = _placeholderColor;
					checkPlaceholderVisibility();
					break;
					
				case STATE_FOCUSED :
					_inputField.textColor = _inputFocusedColor;
					_placeholderField.textColor = _inputFocusedColor;
					break;
				
				case STATE_ERROR :
					_inputField.textColor = _inputErrorColor;
					break;
			}
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function focus():*
		{	
			if ((!_inputField && !stage) || !__enabled) return this;
			
			stage.focus = _inputField;
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			removeInputEvents();
			super.destroy();
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function focusHandler(event:FocusEvent):void
		{
			switch (event.type)
			{
				case FocusEvent.FOCUS_IN :
					changeState(STATE_FOCUSED);
					break;
					
				case FocusEvent.FOCUS_OUT :
					changeState(STATE_NORMAL);
					break;
			}
		}
		
		/**
		 * @private
		 */
		protected function changeHandler(event:*):void
		{
			checkPlaceholderVisibility();
		}
			
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function init():void
		{
			setPadding(5, 5, 5, 5, false);
			super.init();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();

			onClick(focus, false);
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			createTextFields();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			super.draw();
			
			_inputField.width = __width - _paddingLeft - _paddingRight;
			_inputField.x = _paddingLeft;
			_inputField.y = (__height - _inputField.height) >> 1 + 0.5 | 0; // Same that Math.random
			
			// Same dimensions and position
			_placeholderField.width = _inputField.width;
			_placeholderField.x = _inputField.x;
			_placeholderField.y = _inputField.y;
		}		
		
		/**
		 * @private
		 */
		protected function createTextFields():void
		{
			var currentValue:String;
			
			// clear current fields
			if (_inputField)
			{
				removeInputEvents();
				currentValue = _inputField.text;
				removeChild(_inputField);
				_inputField = null;
				
				removeChild(_placeholderField);
				_placeholderField = null;
			}
			
			// PlaceHolder
			_placeholderField = createTextField();
			_placeholderField.type = TextFieldType.DYNAMIC;
			_placeholderField.selectable = false;
			_placeholderField.mouseEnabled = false;
			_placeholderField.textColor = _placeholderColor;
			if (_placeholder) _placeholderField.text = _placeholder;
			addChild(_placeholderField);
			
			// InputField
			_inputField = createTextField();
			_inputField.type = TextFieldType.INPUT;
			_inputField.displayAsPassword = _displayAsPassword;
			if (currentValue) _inputField.text = currentValue;
			_inputField.textColor = _inputNormalColor;
			_inputField.tabIndex = _tabIndex;
			addChild(_inputField);
			
			addInputEvents();
		}
		
		/**
		 * @private
		 */
		protected function createTextField():TextField
		{
			var field:TextField;
			
			if (_fontClass)
			{
				var font:* = new _fontClass();
				
				if (!font.hasOwnProperty("tf"))
					throw new Error("[UIInput] la propiedad fontClass debe contener un campo de texto con el nombre 'tf'");
					
				field = font.tf;
				field.text = ""; // clear default text
			}
			else
			{
				var format:TextFormat = new TextFormat();
				format.font = "Verdana";
				format.size = 12;
				//format.color = 0xFF0000;
				
				field = new TextField();
				field.defaultTextFormat = format;
				field.height = 20;
			}
			
			field.autoSize = TextFieldAutoSize.NONE;
			field.multiline = false;
			field.wordWrap = false;
			
			return field;
		}
		
		/**
		 * @private
		 */
		protected function addInputEvents():void
		{
			_inputField.addEventListener(FocusEvent.FOCUS_IN, focusHandler, false, 0, true);
			_inputField.addEventListener(FocusEvent.FOCUS_OUT, focusHandler, false, 0, true);
			_inputField.addEventListener(Event.CHANGE, changeHandler, false, 0, true);
		}
		
		/**
		 * @private
		 */
		protected function removeInputEvents():void
		{
			_inputField.removeEventListener(FocusEvent.FOCUS_IN, focusHandler);
			_inputField.removeEventListener(FocusEvent.FOCUS_OUT, focusHandler);
			_inputField.removeEventListener(Event.CHANGE, changeHandler);
		}
	
		/**
		 * @private
		 */
		protected function checkPlaceholderVisibility():void
		{
			if (_inputField.text == "")
				showPlaceholder();
			else
				hidePlaceholder();
		}
		
		/**
		 * @private
		 */
		protected function showPlaceholder():void
		{
			_placeholderField.visible = true;
		}
		
		/**
		 * @private
		 */
		protected function hidePlaceholder():void
		{
			_placeholderField.visible = false;
		}
	}
}