//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import flash.display.DisplayObject;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  04.12.2011
	 */
	public class UIPopoverSelect extends UIPopoverButton
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UIPopoverSelect() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/**
		 * @private
		 */
		protected var _menu:UIMenu;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function get selectedIndex():int
		{
			return _menu.selectedIndex;
		}
		
		//---------------------------------------
		// PUBLIC ACCESSORS CHAIN METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public function setMenuData(data:Array, defaultIndex:uint = 0):*
		{
			// fix limit
			defaultIndex = defaultIndex > data.length-1 ? data.length-1 : defaultIndex;
			
			// Set data to menu
			_menu.setData(data);
			// Click to the default selected item
			_menu.click(defaultIndex, false);
			// Update value
			setValue(data[defaultIndex]);
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function setSelectedIndex(index:int, fireEvent:Boolean = false):void
		{
			if (fireEvent)
			{
				_menu.click(index);
			}
			else
			{
				_menu.click(index, false);
				// Update value
				setValue(_menu.data[index]);
			}
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_menu.destroy();
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function onChange(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			_menu.onItemClicked(listener, sendEventToListener, args, once);
			
			return this;
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function menuHandler():void
		{
			setValue(_menu.selected.value);
			
			if (_popover) 
				_popover.dismissPopup();
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			// By default select the first menu item
			if (!_menu.selected)
				_menu.click(0, false);
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			//Add subviews here
			_menu = menuForPopover()
				.setData(dataForMenu())
				.onItemClicked(menuHandler, false);
			
			super.addChildren();
		}
		
		/**
		 * @inheritDoc
		 */
		/*
		override public function draw ():void
		{
			super.draw();
		}*/
		
		/**
		 * @private
		 */
		override protected function viewForPopover():DisplayObject
		{
			return _menu;
		}
		
		/**
		 * @private
		 */
		protected function menuForPopover():UIMenu
		{
			return new UIMenu();
		}
		
		/**
		 * @private
		 */
		protected function dataForMenu():Array
		{
			return ["Default value 1", "Default value 2", "Default value 3", "Default value 4"];
		}
		
		/**
		 * @private
		 */
		override protected function defaultValue():String
		{
			return _menu.items[0].value;
			//return "UIPopoverSelect";
		}
	}
}