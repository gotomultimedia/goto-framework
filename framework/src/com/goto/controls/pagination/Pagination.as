/*
Pagination.as

Created by Silverfenix on 11.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.controls.pagination
{
	import com.goto.display.View;
	import com.goto.display.layout.HLayout;
	import com.goto.utils.ObjectUtils;
	import flash.events.MouseEvent;
	import com.goto.utils.Console;

	public class Pagination extends View 
	{
		
		protected var __layout:HLayout;
		protected var __elements:Array;
		protected var __selected:PaginationItem;
		protected var __selectedIndex:Number = NaN;
		protected var __fireEvent:Boolean = true;
		
		/**
	 	* @constructor
	 	*/
		public function Pagination($settings:Object = null)
		{
			settings = {
				pages: 0,
				itemDefinition: PaginationItem,
				separationBetweenItems: 8
			}
			
			if ($settings) ObjectUtils.extend(settings, $settings);
			
			super();
		}
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		public var settings:Object;
		
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		/*
		override protected function onComponentReady():void {
			// sentences
		}
		*/
		override protected function addChildren():void
		{
			super.addChildren();
			
			__layout = new HLayout;
			__layout.gab = settings.separationBetweenItems;
			addChild(__layout);
		}
		
		//////////////////////////////////////////////////////////
		// Public Chain Methods
		//////////////////////////////////////////////////////////
		
		public function setPages(value:uint):*
		{
			settings.pages = value;
			
			createElements();
			
			return this;
		}
		
		public function setSeparationBetweenItems(value:uint):*
		{
			settings.separationBetweenItems = value;
			__layout.gab = value;
			
			draw();
			
			return this;
		}
		
		public function setPaginationItem(value:Class):*
		{
			if (!value) return;
			
			settings.itemDefinition = value;
			
			// Crea los elementos nuevamente pero con el nuevo tipo de Elemento
			if (__elements) createElements();
			
			return this;
		}
		
		public function onPageClicked(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(PaginationEvent.PAGE_CLICKED, listener, sendEventToListener, args, once);
			return this;
		}
		
		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			// Start
			if (settings.itemDefinition) setPaginationItem(settings.itemDefinition);
			if (settings.pages) setPages(settings.pages);
		}
		
		override public function draw ():void
		{
			__width = __layout.width;
			__height = __layout.height;
			
			super.draw();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			destroyElements();
			super.destroy();
		}
		
		public function click(newItemIndex:uint, fireEvent:Boolean = true):void
		{
			if (!__elements) {
				trace("[Pagination::click] Error: No es posible seleccionar una pagina si no se han establecido el número de paginas, use la propiedad 'pages' para establecer el número de paginas");
				return;
			}
			
			if (newItemIndex > __elements.length - 1) {
				trace("[Pagination::click] Warning: El elemento que desea seleccionar no puede ser mayor al numero total de paginas entonces se seleccionará la pagina # " + __elements.length);
				newItemIndex = __elements.length - 1;
			}
			
			__fireEvent = fireEvent;
			__elements[newItemIndex].dispatchEvent(new MouseEvent(MouseEvent.CLICK));
		}
		
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		protected function mouseHandler(e:MouseEvent):void
		{
			e.stopImmediatePropagation();
			
			var item:PaginationItem = e.currentTarget as PaginationItem;
			
			if (__selected && __selected != item)
			{
				__selected.selected = false;
			}
			
			__selected = item;
			__selected.selected = true;
			__selectedIndex = __selected.index;
			
			if (__fireEvent) 
				dispatchEvent(new PaginationEvent(PaginationEvent.PAGE_CLICKED, {target:this, item:__selected, index:__selectedIndex}));
			
			__fireEvent = true;
		}
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		protected function createElements():void
		{
			if (!settings.pages) return;
			
			if (__elements) destroyElements();
			
			var item:PaginationItem;
			var ItemClass:Class = settings.itemDefinition;
			
			for (var i:int = 0; i < settings.pages; i++)
			{
				item = new ItemClass(ObjectUtils.propsStartingIn("item", settings, true, ["itemDefinition"])) as PaginationItem; //ignore itemDefinition
				item.addEventListener(MouseEvent.CLICK, mouseHandler, false, 0, true);
				item.index = i;
				item.name = "paginationItem" + i;
				
				if (!__elements) __elements = [];
				__elements[i] = item;
				
				if (i == __selectedIndex) item.selected;
				
				__layout.addChild(item);
			}
			
			draw();
		}
		
		protected function destroyElements():void
		{
			if (!__elements) return;
			
			for (var i:int = 0; i < __elements.length; i++)
			{
				var item:PaginationItem = __elements[i];
				item.removeEventListener(MouseEvent.CLICK, mouseHandler);
				__layout.removeChild(item);
				item.destroy();
				item = null;
			}
			
			__elements = null;
		}
		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
		
		public function set pages(value:uint):void
		{
			setPages(value);
		}
		
		public function get pages():uint
		{
			return settings.pages;
		}
		
		public function set separationBetweenItems(value:int):void
		{
			setSeparationBetweenItems(value);
		}
		
		public function get separationBetweenItems():int
		{
			return settings.separationBetweenItems;
		}
		
		public function set paginationItem(value:Class):void
		{
			setPaginationItem(value);
		}
		
		public function get paginationItem():Class
		{
			return settings.itemDefinition;
		}
		
		public function get selected():PaginationItem
		{
			return __selected;
		}
		
		public function get selectedIndex():int
		{
			return __selectedIndex;
		}
	}
}