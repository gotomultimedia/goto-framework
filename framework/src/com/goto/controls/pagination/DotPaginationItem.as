/*
PaginationItem.as

Created by Silverfenix on 10.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.controls.pagination
{
	import flash.filters.DropShadowFilter;

	public class DotPaginationItem extends PaginationItem
	{
		/**
	 	* @constructor
	 	*/
		public function DotPaginationItem()
		{
			buttonMode = true;
			super();
		}

		//////////////////////////////////////////////////////////
		// Generic component Methods
		//////////////////////////////////////////////////////////

		override protected function init():void 
		{
			super.init();
			
			width = 10;
			height = 10;
		}

		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////
		
		override public function destroy():void
		{
			super.destroy();
			removeFilters();
		}
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		override protected function createSkin():void
		{
			super.createSkin();		
			showFilters = true;
		}
					
		protected function applyFilters():void
		{
			// dropshadow params: distance:Number = 4.0, angle:Number = 45, color:uint = 0, alpha:Number = 1.0, blurX:Number = 4.0, blurY:Number = 4.0, strength:Number = 1.0, quality:int = 1, inner:Boolean = false, knockout:Boolean = false, hideObject:Boolean = false
			var innerShadow:DropShadowFilter = new DropShadowFilter (1, 90, 0x00, 1, 1, 1, 0.5, 1, true, false);
			if (!__over.filters.length) __over.filters = [innerShadow];
			if (!__up.filters.length) __up.filters = [innerShadow];
			if (!__down.filters.length) __down.filters = [innerShadow];
		}
		
		protected function removeFilters():void
		{
			__over.filters = __up.filters = __down.filters = null;
		}
		
		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
		
		public function set showFilters(value:Boolean):void
		{
			if (value) applyFilters() else removeFilters();
		}
	}
}