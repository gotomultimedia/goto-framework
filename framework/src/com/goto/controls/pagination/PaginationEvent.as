/*
FullViewEvent.as

Created by Silverfenix on 12.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.controls.pagination 
{
    import flash.events.Event;  
	/**
	 * Contains all the information of the VPlayer events.
	 */
    public class PaginationEvent extends Event
    {
		/** Un objeto con parametros extras */
		public var params:Object; 
		
		public static const PAGE_CLICKED:String = "pageClicked";
				
		/**
		 * @param _type Event type.
		 * @param _params An object with the extra parameters that you want to send when a PaginationEvent is dispatched.;
		 * @param _bubbles Determines if the event object participates in the event flux propagation phase. Default value is <code>false</code>.;
		 * @param _cancelable  Determines if the Event object can be cancelled. Default value is <code>false</code>.;
		 */
        public function PaginationEvent(_type:String, _params:Object = null, _bubbles:Boolean = false, _cancelable:Boolean = false)
        {
            super(_type, _bubbles, _cancelable);
            params = _params;
        }
		
		/**
		 * Duplicates an instance of the Event subclass.
		 */
        public override function clone():Event
        {
            return new PaginationEvent(type, params, bubbles, cancelable);
        }
		
		/**
		 * A useful function to implement the toString() method in the customized Event class of ActionScript 3.0. It is recommended to anulate the toString() method, but isn't necessary.
		 */
        public override function toString():String
        {
            return formatToString("PaginationEvent", "params", "type", "bubbles", "cancelable");
        }
    }
}