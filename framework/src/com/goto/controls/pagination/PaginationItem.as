/*
PaginationItem.as

Created by Silverfenix on 10.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.controls.pagination
{
	import com.goto.display.View;
	import com.goto.display.Draw;
	import com.goto.utils.ObjectUtils;
	import flash.display.DisplayObject;
	import flash.filters.DropShadowFilter;
	import flash.events.MouseEvent;
	import flash.display.Sprite;

	public class PaginationItem extends View 
	{
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		public var settings:Object;
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		protected var __hit:Sprite;
		protected var __over:DisplayObject;
		protected var __up:DisplayObject;
		protected var __down:DisplayObject;
		protected var __index:uint;
		protected var __selected:Boolean;
		protected var __isOver:Boolean;

		/**
	 	* @constructor
	 	*/
		public function PaginationItem($settings:Object = null)
		{
			settings = {
				// color
				overColor: 0xCCCCCC,
				upColor: 0xCCCCCC,
				downColor: 0xFFFFFF,
				// alpha
				overAlpha: 0.5,
				upAlpha: 0.2,
				downAlpha: 1.0
			}
			
			if ($settings) ObjectUtils.extend(settings, $settings);
			
			mouseChildren = false;
			//buttonMode = true;
			
			super();
		}
		
		//////////////////////////////////////////////////////////
		// Generic component Methods
		//////////////////////////////////////////////////////////

		override protected function onComponentReady():void 
		{
			this.addListener(MouseEvent.MOUSE_OVER, mouseHandler)
				//.addListener(MouseEvent.MOUSE_DOWN, mouseHandler)
				.addListener(MouseEvent.MOUSE_OUT, mouseHandler);
			
			super.onComponentReady();
		}
		
		override protected function init():void 
		{
			__width = 8;
			__height = 8;
			
			super.init();
		}

		override protected function addChildren():void
		{
			super.addChildren();
			
			__hit = Draw.rectangle(__width, __height, 0xff0000, 0);
			addChild(__hit);
			
			createSkin();
			
			__over.name = "over";
			__up.name = "up";
			__down.name = "down";
			
			addChild(__up);
		}

		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////
		
		override public function draw ():void
		{
			//__width = __hit.width;
			//__height = __hit.height;
			
			__hit.width = __width;
			__hit.height = __height;
			
			__up.width = __width;
			__up.height = __height;
		
			__over.width = __width;
			__over.height = __height;
		
			__down.width = __width;
			__down.height = __height;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			settings = null;
			
			super.destroy();
		}
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		protected function mouseHandler(e:MouseEvent):void
		{	
			switch (e.type)
			{
				case MouseEvent.MOUSE_DOWN :
					stage.addEventListener(MouseEvent.MOUSE_UP, mouseHandler, false, 0, true);
					showSkin("down");
					break;
					
				case MouseEvent.MOUSE_UP :
					stage.removeEventListener(MouseEvent.MOUSE_UP, mouseHandler);

					if (__selected) 
						showSkin("down");
					else
						showSkin(__isOver ? "over" : "up");

					break;
				case MouseEvent.MOUSE_OVER :
					__isOver = true;
					showSkin("over");
					break;
					
				case MouseEvent.MOUSE_OUT :
					__isOver = false;
					showSkin(!__selected ? "up" : "down");
					break;
			}
		}
		
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		protected function createSkin():void
		{
			var size:uint = __height * 0.5;
			
			__over 	= Draw.circle(size, settings.overColor, settings.overAlpha);
			__up	= Draw.circle(size, settings.upColor, settings.upAlpha);
			__down 	= Draw.circle(size, settings.downColor, settings.downAlpha);
		}
				
		protected function showSkin(newSkinName:String):void
		{
			var newSkin:DisplayObject = this["__" + newSkinName];
			addChild(newSkin);
			
			for each (var skin in [__over, __up, __down]) 
			{
				if (skin == newSkin) continue;
				
				if (skin.parent) 
				{
					removeChild(skin);
				}
			}
		}
		
		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
		
		public function set selected(value:Boolean):void
		{
			__selected = value;
			enabled = !value;
			
			showSkin(__selected ? "down" : (__isOver ? "over" : "up"));
		}
		
		public function get selected():Boolean
		{
			return __selected;
		}
		
		override public function set enabled (value:Boolean):void
		{
			super.enabled = value;	
			if (__enabled) 
				dispatchEvent(new MouseEvent(MouseEvent.MOUSE_OUT));
		}
		
		public function set index(value:uint):void
		{
			__index = value;
		}
		
		public function get index():uint
		{
			return __index;
		}
	}
}