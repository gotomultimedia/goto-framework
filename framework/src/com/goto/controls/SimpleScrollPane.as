/*
SimpleScrollPane.as
labs

Created by Alexander Ruiz Ponce on 25/09/09.
Copyright 2009 goTo! Multimedia. All rights reserved.
*/
package com.goto.controls {
	
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.text.TextField;
	
	import com.goto.display.Draw;
	import com.goto.display.components.GenericComponent;
	
	public class SimpleScrollPane extends SimpleScrollBar {
		
		private var __mask:Sprite;
		private var __padding:Number = 5;
		private var __paddingLeft:Number = 0;
		private var __content:Sprite;
		private var __gosthBG:Sprite;
		private var __enableScrollBar:Boolean = true;
		private var __scrollBarSpace:uint;
		
		public function SimpleScrollPane (_target:* = null) {
			__target = _target;
			super(__target);
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////
		
		override protected function init ():void
		{
			setMinSize(100, 49, false);
			super.init();
			
			
			if (__target) {
				content = __target;
			}
		}
		
		override protected function addChildren():void
		{
			__gosthBG = Draw.rectangle(__minWidth, __minHeight, 0xffff00, 0);
			addChild(__gosthBG);
			
			super.addChildren();
			
			__mask = Draw.rectangle(__minWidth, __minHeight, 0xff0000, 0);
			addChild(__mask);
		}
		
		override protected function verifyVisibility ():void
		{
			if (__enableScrollBar) {
				var v:Boolean = true;
				
				if (__target) {
					if (__target.height < __height) {
						v = false;
					} else {
						v = true;
					}
				} else {
					v = false;
				}
				__scroll.visible = v;
			}
		}
		
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		override public function draw ():void
		{
			if (!__componentReady) return;
			
			__gosthBG.width = __width;
			__gosthBG.height = __height;
			
			super.draw();
			
			
			if(__enableScrollBar){
				__scrollBarSpace = __trackHolder.width + __padding;
				__scroll.x  = __width - __trackHolder.width;
			}else{
				__scrollBarSpace = 0;
				__scroll.x  = __width;
			}
			
			__mask.width = __width - __scrollBarSpace;
			__mask.height = __height;
						
			if (__target is TextField || __target is GenericComponent) {
				__target.width = __mask.width - __paddingLeft;
			}
			
			__target.x = __paddingLeft;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		private function clear ():void {
			if (__target) {
				if (contains(__target)) {
					removeChild(__target);
				}
			}
		}
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		public function set padding (value:Number):void {
			__padding = value;
			draw();
		}
		
		/**
		 * @private
		 */
		public function set paddingLeft(value:int):void
		{
			__paddingLeft = value;
			draw();
		}
		
		public function set content (value:*):void {
			clear();
			if (!value) return;
			
			__thumbHolder.y = 0;
			__target = value;
			__target.x = __target.y = 0;
			if (__target is TextField) {
				if (__target.autoSize == "none") {
					__target.autoSize = "left";
				}
			}
			__target.mask = __mask;
			addChild(__target);
			draw();
		}
		
		public function set enableScrollBar (value:Boolean):void
		{
			__enableScrollBar = value;
			__scroll.visible = value;
			enableMouseWheel = value;
			
			draw();
		}
		
		public function get contentWidth ():Number
		{
			return __mask.width;
		}
	}
}
