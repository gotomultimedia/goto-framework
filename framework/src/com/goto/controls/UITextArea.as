//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.controls
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  23.10.2011
	 */
	public class UITextArea extends UIInput 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function UITextArea() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------

		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
				
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * @private
		 */
		public static function textAreaWithStates(normal:Object = null, focused:Object = null, error:Object = null, 
												  innerRect:Rectangle = null, 
												  outerRect:Rectangle = null):*
		{
			return new UITextArea().setStates(normal, focused, error).setRectangles(innerRect, outerRect, false);
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
			
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			super.draw();
			
			_inputField.width = __width - _paddingLeft - _paddingRight;
			_inputField.height = __height - _paddingTop - _paddingBottom;
			_inputField.x = _paddingLeft;
			_inputField.y = _paddingTop;
			
			// Same dimensions and position
			_placeholderField.width = _inputField.width;
			_placeholderField.x = _inputField.x;
			_placeholderField.y = _inputField.y;
		}
		
		/**
		 * @private
		 */
		override protected function createTextFields():void
		{
			super.createTextFields();
			
			_placeholderField.multiline = true;
			_placeholderField.wordWrap = true;
			
			_inputField.multiline = true;
			_inputField.wordWrap = true;
		}		
	}
}