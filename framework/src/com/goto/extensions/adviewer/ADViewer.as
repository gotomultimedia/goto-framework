//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.adviewer
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.*;
	import com.goto.utils.ObjectUtils;
	import flash.display.*;
	import com.goto.utils.Console;
	
	/**
	 * View Subclass
	 * A simple adversising visualizer with an opcional banner in the top position
	 * 
	 * <pre>
	 * var advSettings:Object = {
	 *	bannerText: "Menssage for Banner", // leave as null if you don't want banner
	 *	bannerTextYOffset: 3,
	 *	bannerIndex: 0,
	 *	bannerDelay: 1.0,
	 *	galleryPaginationItem: SliderPaginationItem,
	 *	galleryPaginationSeparation: -5,
	 *	galleryPaginationYOffset: -35,
	 *	ads: [{src:"banner1.jpg", href:"#/internal/url"},
	 * 		  {src:"banner2.jpg", href:"http://external-url.com"}]
	 * }
	 * 
	 * var adv:ADViewer = new ADViewer(advSettings).addInto(stage);
	 * </pre>
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  04.05.2012
	 */
	public class ADViewer extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function ADViewer(userSettings:Object = null) 
		{
			// Default settings
			settings = {
				ads: null, // this is set via instance
				backgroundTexture: new ADBackgroundPattern(),
				closeButtonView: new Bitmap(new ADCloseButton()),
				shadowTexture: new ADShadow(),
				bannerIndex: 0,
				bannerDelay: 3.0,
				bannerSize: 40,
				// Events
				onBannerOpening: null,
				onBannerClosing: null,
				onOpening: null,
				onClosing: null,
				onChange: null,
				onBannerOpeningParams: null,
				onBannerClosingParams: null,
				onOpeningParams: null,
				onClosingParams: null,
				onChangeParams: null
			}
			
			ObjectUtils.extend(settings, userSettings);
			
			super();
		}
			
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		/**
		 * Stores the component settings
		 */
		public var settings:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/** @private */
		protected var _fill:Fill;
		/** @private */
		protected var _holder:Sprite;
		/** @private */
		protected var _banner:Banner;
		/** @private */
		protected var _isOpen:Boolean = false;
		/** @private */
		protected var _bannerIsOpen:Boolean = false;
		/** @private */
		protected var _closeButton:View;
		/** @private */
		protected var _shadow:Bitmap;
		/** @private */
		protected var _gallery:Gallery;
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function get currentHeight():Number
		{
			return __height+_holder.y;
		}
		
		/**
		 * @private
		 */
		public function get bannerIsOpen():Boolean
		{
			return _bannerIsOpen;
		}
		
		/**
		 * @private
		 */
		public function get isOpen():Boolean
		{
			return _isOpen;
		}
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		/**
		 * Opens the banner after delay
		 * @param delay The delay in seconds to open the banner, default 2
		 * @return  
		 */
		public function openBannerAfterDelay(delay:Number = 2.0):*
		{
			delayedCall(delay, openBanner);
			return this;
		}
		
		/**
		 * @private
		 */
		public function openBanner():*
		{
			enabled = true;
			_bannerIsOpen = true;
			
			tweenTo({target:_holder, y:positionForBanner(), onUpdate:notify, onUpdateParams:["onBannerOpening"]});
			tweenTo({target:_shadow, alpha:1});
			_closeButton.show(0.7, 0.5);
			_banner.show();
			
			return this;
		}
		
		public function open(index:uint = 0):*
		{
			killDelayedCall(openBanner);
			
			enabled = true;
			_isOpen = true;
			_bannerIsOpen = false;
			
			// Al completar carga la imagen de la galería
			tweenTo({y:0, target:_holder, onUpdate:notify, onUpdateParams:["onOpening"], onComplete:createGallery, onCompleteParams:[index]}, 1.0);
			tweenTo({target:_shadow, alpha:0}, 1.5);
			_banner.hide();
			_closeButton.show();
			
			return this;
		}
		
		public function close():*
		{
			enabled = false;
			
			var updateCallBackName:String = (_isOpen) ? "onClosing" : "onBannerClosing";
			_isOpen = false;
			_bannerIsOpen = false;
			
			_closeButton.hide(0.3);
			destroyGallery(true); // soft destroy
			tweenTo({target:_holder, y:-__height, onUpdate:notify, onUpdateParams:[updateCallBackName]}, 1.0);
			tweenTo({target:_shadow, alpha:0});
			
			return this;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_closeButton.destroy();
			_banner.destroy();
			_fill.destroy();
			
			destroyGallery();
			
			super.destroy();
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function onParentResize(event:Event = null):void
		{
			var pSize:Object = parentSize();
			
			setSize(pSize.width, pSize.height);
		}
			
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onStage():void
		{
			parent.addEventListener(Event.RESIZE, onParentResize, false, 0, true);
			onParentResize();
			
			super.onStage();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function onRemovedFromStage():void
		{
			parent.removeEventListener(Event.RESIZE, onParentResize);
			
			super.onRemovedFromStage();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			if (settings.bannerText) 
				openBannerAfterDelay(settings.bannerDelay)
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_holder = new Sprite();
			addChild(_holder);
			
			// The AD fill
			_fill = Fill.pattern(settings.backgroundTexture).addInto(_holder);
			
			
			// The Banner
			_banner = new Banner(ObjectUtils.propsStartingIn("banner", settings))
				.onClick(open, false, [settings.bannerIndex])
				.setSize(100, settings.bannerSize)
				.addInto(_holder);
			
			// Closebutton
			_closeButton = new View()
				.setSize(settings.closeButtonView.width, settings.closeButtonView.height)
				.setProperties({buttonMode:true})
				.hide(0)
				.addInto(this);
			
			_closeButton
				.onClick(close)
				.onMouseOver(_closeButton.fadeTo, false, [0.5])
				.onMouseOut(_closeButton.fadeTo, false, [1.0]);
			
			_closeButton.addChild(settings.closeButtonView);
			
			// Shadow
			_shadow = new Bitmap(settings.shadowTexture);
			_shadow.alpha = 0;
			_holder.addChild(_shadow);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			_fill.setSize(__width, __height);
			
			_banner.width = __width;
			_banner.y = __height - _banner.height;
			
			_closeButton.move(__width - _closeButton.width - 10, (settings.bannerSize - _closeButton.height) >> 1 + 0.5);
			
			_shadow.width = __width;
			_shadow.y = __height - _shadow.height + 1;
			
			if (_gallery) 
				_gallery.move((__width - _gallery.width) >> 1, (__height - _gallery.height) >> 1);
			
			if (_bannerIsOpen)
				_holder.y = positionForBanner();
			else if (_isOpen)
				_holder.y = 0;			
			else
				_holder.y = -__height;
			
			super.draw();
		}
		
		/**
		 * @private
		 */
		protected function parentSize():Object
		{
			if (!parent) return {width:20, height:20};
			
			if (parent is Stage)
				return {width:stage.stageWidth, height:stage.stageHeight};
			
			return {width:parent.width, height:parent.height};
		}	
		
		/**
		 * @private
		 */
		protected function positionForBanner():Number
		{
			return -__height+settings.bannerSize;
		}
		
		/**
		 * Ejecuta la función especificada en el parametro eventName y onChange de settings
		 * 
		 * @private
		 */
		protected function notify(eventName:String):void
		{
			if (settings[eventName])
				settings[eventName].apply(this, settings[eventName+"Params"]);
			
			if (settings.onChange)
				settings.onChange.apply(this, settings.onChangeParams);
		}
		
		/**
		 * @private
		 */
		protected function createGallery(index:uint = 0):void
		{
			var galleryHandler:Function = function (event:ADViewerEvent) {
				if (event.params.hasInternallURL) close();
			}
			
			_gallery = new Gallery(ObjectUtils.propsStartingIn("gallery", settings))
				.onGalleryItemClicked(galleryHandler, true)
				.setData(settings.ads, index)
				.hide(0).show()
				.addInto(_holder);
			
			// Center
			_gallery.move(centerX - _gallery.centerX, centerY - _gallery.centerY);
		}
		
		/**
		 * @private
		 */
		protected function destroyGallery(softDestroy:Boolean = false):void
		{
			if (_gallery)
			{
				if (softDestroy)
				{
					_gallery.hide(0.7, {onComplete:destroyGallery});
				}
				else
				{
					_gallery.destroy();
					_gallery = null;
				}
			}
		}
	}
}