//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.adviewer
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.*;
	import com.goto.utils.ObjectUtils;
	import com.goto.utils.DisplayObjectUtils;
	import com.goto.extensions.ImageViewer;
	import com.goto.controls.pagination.*;
	import flash.geom.Rectangle;
	import flash.net.*;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  08.05.2012
	 */
	public class Gallery extends ImageViewer 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function Gallery(userSettings:Object = null) 
		{
			// Default settings
			var defaults:Object = {
				base: new ADGalleryBase(0, 0),
				baseInnerRect: new Rectangle(38, 33, 60, 60),
				baseOuterRect: new Rectangle(18, 13, 100, 100),
				width: 800,
				height: 500,
				paginationItem: null,
				paginationSeparation: 3,
				paginationAlign: "bottom",
				paginationXOffset: 0,
				paginationYOffset: 0
			}
			
			ObjectUtils.extend(defaults, userSettings);
			
			super(defaults);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _base:ScalableObject;
		/** @private Arreglo de Objetos, cada item podría contener: src, alt, scaleMode */
		protected var _data:Array;
		protected var _selectedIndex:uint;
		protected var _pagination:Pagination;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		/**
		 * Asigna los datos de la galería con la opción de especificar cual es el item seleccionado por defecto.
		 * @param value Arreglo con la información de las imágenes de la galería, debe ser un arreglo de objetos, cada objeto podría contener las propiedades <code>tn, src, alt, scaleMode</code>.
		 * @param defaultItemIndex El indice del item que se mostrará por defecto despues de asignar los datos. No puede ser mayor que el tamaño del arreglo recibido en el parametro <code>value</code>.
		 * @return La instancia dueña de este método.
		 */
		public function setData(value:Array, defaultItemIndex:uint = 0):*
		{
			_selectedIndex = defaultItemIndex > value.length-1 ? value.length-1 : defaultItemIndex;
			
			_data = value;
			
			_pagination.visible = _data.length > 1;
			_pagination.pages = _data.length;
			_pagination.click(_selectedIndex, false);
			
			setImageData(_data[_selectedIndex]);
			
			draw();
			
			return this;
		}
		
		public function onGalleryItemClicked(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(ADViewerEvent.GALLERY_ITEM_CLICKED, listener, sendEventToListener, args, once);
			return this;
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_pagination.destroy();
			_base.destroy();
			
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function clear():void
		{
			_imageView.clear();
			if (_imageLoader) _imageLoader.clear();
		}
		
		/**
		 * @private
		 */
		public function click(index:uint):*
		{
			// fix offset
			if (index > _data.length-1) 
				index = _data.length-1;
			
			_pagination.click(index, false);
			
			_direction = (index < _selectedIndex) ? "previous" : "next";
			
			_selectedIndex = index;
			
			setImageData(_data[_selectedIndex]);
			
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			_imageHolder.buttonMode = true;
			_imageView.onClick(function () {
				if (_data[_selectedIndex].href)
				{
					var target:String;
					var hasInternallURL:Boolean = _data[_selectedIndex].href.indexOf("#") == 0;
					
					if (hasInternallURL)
						target = "_self";
					else
						target = "_blank";
					
					dispatchEvent(new ADViewerEvent(ADViewerEvent.GALLERY_ITEM_CLICKED, {hasInternallURL:hasInternallURL})); // GALLERY_ITEM_CLICKED
					
					navigateToURL(new URLRequest(_data[_selectedIndex].href), _data[_selectedIndex].target || target);
				}
			});
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			//Add subviews here
			_base = ScalableObject.fromBitmapData(settings.base, settings.baseInnerRect, settings.baseOuterRect).addInto(this);
			
			super.addChildren();
			
			_pagination = new Pagination()
				.onPageClicked(function (event:PaginationEvent) { click(event.params.index) }, true)
				.setPaginationItem(settings.paginationItem)
				.setSeparationBetweenItems(settings.paginationSeparation)
				.addInto(this)
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			__width = settings.width;
			__height = settings.height;
			
			_base.setSize(__width, __height);
			
			super.draw();
			
			_imageHolder.scrollRect = new Rectangle(0, 0, __width, __height);
			
			DisplayObjectUtils.align(_pagination, this, settings.paginationAlign, settings.paginationXOffset, settings.paginationYOffset);
			DisplayObjectUtils.align(_preloader, this, "center");
		}		
	}
}