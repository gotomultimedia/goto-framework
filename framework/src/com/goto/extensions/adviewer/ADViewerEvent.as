//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2012 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.adviewer
{

	import flash.events.Event;

	/**
	 * Event subclass.
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  09.05.2012
	 */
	public class ADViewerEvent extends Event
	{
	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
	
		public static const GALLERY_ITEM_CLICKED:String = "galleryItemClicked";
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		public var params:Object;
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		
		/**
		 *	@constructor
		 */
		public function ADViewerEvent(type:String, params:Object = null, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.params = params;
		}
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		override public function clone():Event
		{
			return new ADViewerEvent(type, params, bubbles, cancelable);
		}
		
		override public function toString():String
        {
            return formatToString("ADViewerEvent", "params", "type", "bubbles", "cancelable");
        }
	}
	
}

