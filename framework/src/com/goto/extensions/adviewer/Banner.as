//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.adviewer
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.*;
	import com.goto.utils.ObjectUtils;
	import com.goto.controls.UILabel;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  07.05.2012
	 */
	public class Banner extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function Banner($settings:Object = null) 
		{
			mouseChildren = false;
			buttonMode = true;
			
			// Default settings
			settings = {
				backgroundColor: 0x00ffff,
				backgroundAlpha: 0,
				text: null,
				font: ADBannerFont,
				fontColor: 0xFFFFFF,
				textYOffset: 0,
				textXOffset: 0
			}
			
			ObjectUtils.extend(settings, $settings);
			
			super();
		}
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		/**
		 * Stores the component settings
		 */
		public var settings:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		/** @private */
		protected var _fill:Fill;
		/** @private */
		protected var _label:UILabel;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_fill = Fill.solid(settings.backgroundColor, settings.backgroundAlpha).addInto(this);
			
			if (settings.text) 
				_label = UILabel.labelWithFont(settings.text, settings.font, settings.fontColor).addInto(this);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			_fill.setSize(__width, __height);
			
			if (_label)
				_label.move(((__width-_label.width) >> 1)  + settings.textXOffset, 
							((__height-_label.height) >> 1)+ settings.textYOffset);
				
			super.draw();
		}		
	}
}

