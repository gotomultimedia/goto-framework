//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions
{
	
	import flash.display.*;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import com.goto.display.*;
	import com.goto.controls.PiePreloader;
	import com.goto.utils.ObjectUtils;
	import fl.motion.easing.Exponential;
	
	//import com.goto.utils.Console;
	
	/**
	 * A Simple image visualizer with push transition
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  08.05.2012
	 */
	public class ImageViewer extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function ImageViewer(userSettings:Object = null) 
		{
			// Default settings
			settings = {
				time: 0.7,
				easeInOut: Exponential.easeInOut,
				easeOut: Exponential.easeOut,
				preloaderColor: 0xffffff,
				loaderContext: null
			}
			
			ObjectUtils.extend(settings, userSettings);
			
			super();
		}
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		/**
		 * Stores the component settings
		 */
		public var settings:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _imageLoader:ImageView;
		protected var _imageView:ImageView;
		protected var _direction:String = "next";
		protected var _imageHolder:Sprite;
		protected var _preloader:PiePreloader;
		protected var _loaderInfo:LoaderInfo;
		protected var _transitionComplete:Boolean;
				
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		protected var _imageData:Object;

		public function get imageData():Object
		{
			return _imageData;
		}

		public function setImageData(value:Object):*
		{
			if (value !== _imageData)
			{
				_imageData = value;
				load();
			}
			
			return this;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			if (_imageView) 
				_imageView.destroy();
			
			if (_imageLoader) 
				_imageLoader.destroy();
			
			destroyLoaderInfo();
			_preloader.destroy();
			_preloader = null;
			
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function loadWithDirection(data:Object, direction:String):void
		{
			_imageData = data;
			_direction = direction;
			
			load();
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		protected function onLoadComplete ():void
		{
			hidePreloader();
			transitionStart();
			
			_transitionComplete = false;
			
			if (_imageView.image)
			{
				var posX1:int;
				var posX2:int;
				
				_imageLoader.setSize(__width, __height);
			
				if (_direction == "next") 
				{
					_imageLoader.x = __width;
					posX1 = 0;
					posX2 = -__width;
				} 
				else 
				{
					_imageLoader.x = -__width;
					posX1 = 0;
					posX2 = __width;
				}
				
				tweenTo({target:_imageLoader, x:posX1, ease:settings.easeInOut}, settings.time);
				tweenTo({target:_imageView, x:posX2, ease:settings.easeInOut, onComplete:swapImages}, settings.time);
				
				//TweenMax.to(_imageLoader, Global.settings.time, {x:posX1, ease:Global.settings.easeInOut});
				//TweenMax.to(_imageView, Global.settings.time, {x:posX2, ease:Global.settings.easeInOut, onComplete:swapImages});
			
				function swapImages ():void
				{
					_imageView.image = _imageLoader.image;
					_imageView.scaleMode = _imageLoader.scaleMode;
					_imageView.align = _imageLoader.align;
					_imageView.x = 0;
					
					_imageHolder.removeChild(_imageLoader);
					
					_transitionComplete = true;
					
					// Finished
					transitionComplete();
				}
			}
			else
			{
				_imageView.image = _imageLoader.image;
				_imageView.align = _imageLoader.align;
				_imageView.scaleMode = _imageLoader.scaleMode;
				
				_imageView.setSize(__width, __height);
				
				tweenFrom({target:_imageView, x:__width, ease:settings.easeOut, onComplete:function () {					
					_transitionComplete = true;
					transitionComplete();
				}}, settings.time);
				
				/*
				TweenMax.from(_imageView, Global.settings.time, {x:__width, ease:Global.settings.easeOut, onComplete:function () {
					_transitionComplete = true;
					transitionComplete();
				}});
				*/
			}
		}
		
		/**
		 * @private
		 */
		protected function currentImageProgressHandler(event:ProgressEvent):void
		{
			var progress = (event.bytesLoaded * 100) / event.bytesTotal;
			_preloader.progress = progress;
		}
				
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			_imageHolder = new Sprite();
			addChild(_imageHolder);
			
			//Add subviews here
			_imageView = new ImageView();
			_imageView.name = "imageView";
			_imageHolder.addChild(_imageView);
			
			// Preloader
			_preloader = new PiePreloader();
			_preloader.backgroundColor = settings.preloaderColor;
			_preloader.backgroundAlpha = 0.6;
			_preloader.alpha = 0;
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			_imageView.setSize(__width, __height);
			
			if (_preloader.stage)
			{
				_preloader.move((__width - _preloader.width) * 0.5, 
								(__height - _preloader.height) * 0.5);
			}
			
			super.draw();
		}	
		
		/**
		 * @private
		 */
		protected function load ():void
		{
			if (!_imageLoader) 
			{
				_imageLoader = new ImageView();
				_imageLoader.name = "imageLoader";
				_imageLoader.onLoadComplete(onLoadComplete);
				_imageLoader.loaderContext = settings.loaderContext;
			}

			_imageHolder.addChild(_imageLoader);
			_imageLoader.source = _imageData.src;
			
			showPreloader(0.5);
			
			if (_imageData.scaleMode) _imageLoader.scaleMode = _imageData.scaleMode;
			if (_imageData.align) _imageLoader.align = _imageData.align;
		}
		
		/**
		 * @private
		 */
		protected function showPreloader(delay:Number = 0):void
		{
			destroyLoaderInfo();
			
			_loaderInfo = _imageLoader.loader.contentLoaderInfo;
			_loaderInfo.addEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler, false, 0, true);
			
			_preloader
				.addInto(this)
				.move((__width - _preloader.width) * 0.5, (__height - _preloader.height) * 0.5);
				
			tweenTo({target:_preloader, alpha:1, delay:delay}, 0.7);
		}
		
		/**
		 * @private
		 */
		protected function hidePreloader():void
		{
			destroyLoaderInfo();
			
			TweenEngine.killTweensOf(_preloader); // Destroy delay
			tweenTo({target:_preloader, alpha:0, onComplete:function () {
				_preloader.progress = 0.0;
				if (_preloader.stage) removeChild(_preloader);
			}}, 0.7);
		}
		
		/**
		 * @private
		 */
		protected function destroyLoaderInfo():void
		{
			if (!_loaderInfo) return;
			
			_loaderInfo.removeEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler);
			_loaderInfo = null;
		}
		
		/**
		 * @private
		 */
		protected function transitionStart():void
		{
			// implement this
		}
		
		/**
		 * @private
		 */
		protected function transitionComplete():void
		{
			// implement this
		}
				
	}
}

