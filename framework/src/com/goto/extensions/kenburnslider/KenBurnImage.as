//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.kenburnslider
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.ImageView;
	import com.goto.display.Draw;
	import fl.motion.easing.Linear;
	import fl.motion.easing.Sine;
	import com.greensock.TweenMax;
	import com.goto.display.View;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Silverfenix
	 * @since  20.08.2011
	 */
	public class KenBurnImage extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const SMALL_TO_BIG:String = "smallToBig";
		public static const BIG_TO_SMALL:String = "bigToSmall";
		public static const FADE_IN:String = "fadeIn";
		public static const FADE_OUT:String = "fadeOut";
		public static const FADE_NONE:String = "fadeNone";
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function KenBurnImage() 
		{
			super(false);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _background:Sprite;
		protected var _scalableHolder:Sprite;
		protected var _imageView:ImageView;
		protected var _defaultTransition:String;
		protected var _autoPlay:Boolean = false;
		
		protected var _transitionTime:uint = 2;
		protected var _displayTime:uint = 10;
		protected var _scaleFactor:Number = 1.25;
		
		protected var _isDisplaying:Boolean;
		
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_scalableHolder = new Sprite();
			addChild(_scalableHolder);
			
			_imageView = new ImageView(false)
				.setScaleMode("full")
				.onLoadComplete(loadCompleteHandler, true)
				.addInto(_scalableHolder)
			
			_background = Draw.rectangle(100, 100, 0);
			
			// @todo
			// if (_imageView.scaleMode == "fitToWidthAndHeight") addChildAt(_background, 0);
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			super.draw();
			
			_scalableHolder.x = 0.5 * __width;
			_scalableHolder.y = 0.5 * __height;
			
			_background.width = __width;
			_background.height = __height;
			
			_imageView.setSize(__width, __height);
			_imageView.move(-_scalableHolder.x, -_scalableHolder.y);
		}		
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		public function set source(value:String):void
		{
			TweenMax.killTweensOf(notify);
			_isDisplaying = false;
			_imageView.source = value;
		}
		
		public function get source():String
		{
			return _imageView.source;
		}
		
		public function get autoPlay():Boolean
		{
			return _autoPlay;
		}

		public function set autoPlay(value:Boolean):void
		{
			if (value !== _autoPlay)
			{
				_autoPlay = value;
			}
		}
		
		public function get defaultTransition():String
		{
			return _defaultTransition;
		}

		public function set defaultTransition(value:String):void
		{
			if (value !== _defaultTransition)
			{
				_defaultTransition = value;
			}
		}
		
		public function get isLoading():Boolean
		{
			return _imageView.isLoading;
		}
		
		public function get transitionTime():uint
		{
			return _transitionTime;
		}

		public function set transitionTime(value:uint):void
		{
			if (value !== _transitionTime)
			{
				_transitionTime = value;
			}
		}

		public function get displayTime():uint
		{
			return _displayTime;
		}

		public function set displayTime(value:uint):void
		{
			if (value !== _displayTime)
			{
				_displayTime = value;
			}
		}

		public function get scaleFactor():Number
		{
			return _scaleFactor;
		}

		public function set scaleFactor(value:Number):void
		{
			if (value !== _scaleFactor)
			{
				_scaleFactor = value;
			}
		}
		
		public function get isDisplaying():Boolean
		{
			return _isDisplaying;
		}
		
		public function get imageView():ImageView
		{
			return _imageView;
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function startTransition (transition:String = SMALL_TO_BIG, fadeMode:String = FADE_IN):void
		{
			_isDisplaying = true;
			
			TweenMax.killTweensOf(_scalableHolder);
			
			if (_defaultTransition) 
				transition = _defaultTransition;
			
			if (transition == SMALL_TO_BIG)
			{
				_scalableHolder.scaleX = _scalableHolder.scaleY = 1;
				TweenMax.to(_scalableHolder, _displayTime + _transitionTime*2, {scaleX:_scaleFactor, scaleY:_scaleFactor, ease:Linear.easeNone, onComplete:notify, onCompleteParams:[KenBurnSliderEvent.SCALE_COMPLETE]});
			} 
			else
			{
				_scalableHolder.scaleX = _scalableHolder.scaleY = _scaleFactor;
				TweenMax.to(_scalableHolder, _displayTime + _transitionTime*2, {scaleX:1, scaleY:1, ease:Linear.easeNone});
			}
			
			if (fadeMode) fade(fadeMode);
		}
		
		public function fade(fadeMode:String = FADE_IN):void
		{
			if (fadeMode == FADE_IN)
			{
				_scalableHolder.alpha = _background.alpha = 0;
				TweenMax.allTo([_scalableHolder, _background], _transitionTime, {alpha:1, ease:Sine.easeOut}, 0, notify, [KenBurnSliderEvent.FADE_IN_COMPLETE]);
			}
			else
			{
				//_scalableHolder.alpha = 1;
				TweenMax.allTo([_scalableHolder, _background], _transitionTime, {alpha:0, ease:Sine.easeOut}, 0, notify, [KenBurnSliderEvent.FADE_OUT_COMPLETE]);
			}
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			TweenMax.killTweensOf(_scalableHolder);
			TweenMax.killTweensOf(notify);
			
			_scalableHolder.removeChild(_imageView);
			_imageView.destroy();
			_imageView = null;
			
			super.destroy();
		}
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS FOR EVENT HANDLING
		//---------------------------------------
		
		public function onLoadComplete(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(KenBurnSliderEvent.LOAD_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onFadeInComplete(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(KenBurnSliderEvent.FADE_IN_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onFadeOutComplete(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(KenBurnSliderEvent.FADE_OUT_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onScaleComplete(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(KenBurnSliderEvent.SCALE_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onDisplayTimeComplete(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(KenBurnSliderEvent.DISPLAY_TIME_COMPLETE, listener, sendEventToListener, args, once);
			return this;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		protected function loadCompleteHandler(e:Event):void
		{
			draw();
			
			_scalableHolder.alpha = 0;
			if (_autoPlay) startTransition(BIG_TO_SMALL);
			
			notify(KenBurnSliderEvent.LOAD_COMPLETE, {imageView:e.currentTarget as ImageView});
		}
		
		protected function notify(notification:String, params:Object = null):void
		{
			if (notification == KenBurnSliderEvent.FADE_IN_COMPLETE)
			{
				startDisplayTimeout();
			}
			
			var p:Object = {target:this};
			
			if (params) {
				p = params;
				p.target = this;
			}
			
			dispatchEvent(new KenBurnSliderEvent(notification, p));
		}
		
		protected function startDisplayTimeout():void
		{
			TweenMax.delayedCall(_displayTime, notify, [KenBurnSliderEvent.DISPLAY_TIME_COMPLETE]);
		}		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
	}
}