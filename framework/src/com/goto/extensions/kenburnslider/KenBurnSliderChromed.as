//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.kenburnslider
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import com.goto.utils.DisplayObjectUtils;
	import com.goto.utils.ObjectUtils;
	import com.goto.controls.pagination.*;
	import com.goto.controls.PiePreloader;
	import flash.display.LoaderInfo;
	import flash.events.ProgressEvent;
	import com.greensock.*;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Silverfenix
	 * @since  22.08.2011
	 */
	public class KenBurnSliderChromed extends KenBurnSlider 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function KenBurnSliderChromed($settings:Object = null) 
		{
			super($settings);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _pagination:Pagination;
		protected var _preloader:PiePreloader;		
		protected var _loaderInfo:LoaderInfo;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		override public function setData(value:Array):*
		{
			super.setData(value);
			
			_pagination.visible = _data.length > 1;
			_pagination.pages = _data.length;
			
			return this;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		override public function showImageByIndex(index:uint, slideShowMode:Boolean = false):void
		{
			super.showImageByIndex(index, slideShowMode);
			
			_pagination.click(index, false);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			super.destroy();
			
			_pagination.destroy();
			_pagination = null;
			
			_preloader.destroy();
			_preloader = null;
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		protected function paginationHandler(event:PaginationEvent):void
		{
			showImageByIndex(event.params.index);
		}		
		
		override protected function onLoadComplete(event:KenBurnSliderEvent):void
		{
			if (!_firstImageLoaded)
			{
				hidePreloader();
			}
			
			if (_isWaitingForLoadComplete || !_slideShowMode)
			{
				hidePreloader();
			}
			
			super.onLoadComplete(event);
		}
		
		override protected function onDisplayTimeComplete(event:KenBurnSliderEvent):void
		{
			super.onDisplayTimeComplete(event);
			
			if (_currentKenBurnImage.isLoading)
			{
				showPreloader();
			}
		}
		
		/*
		override protected function onScaleComplete(event:KenBurnSliderEvent):void
		{
			super.onScaleComplete(event);
		}*/
		
		/**
		 * @private
		 */
		protected function currentImageProgressHandler(e:ProgressEvent):void
		{
			var progress = (e.bytesLoaded * 100) / e.bytesTotal;
			_preloader.progress = progress;
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			//
		}
		
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			
			_pagination = new Pagination( ObjectUtils.propsStartingIn("pagination", settings, true) )
				.onPageClicked(paginationHandler, true)
				.addInto(this);
			
			// Preloader
			_preloader = new PiePreloader();
			_preloader.alpha = 0;			
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			super.draw();
			
			DisplayObjectUtils.align(_pagination, this, "bottomLeft", 20, 20);
			DisplayObjectUtils.align(_preloader, this, "bottomRight", 20, 20);
		}
		
		override protected function startTransitionOfCurrent():void
		{
			if (_slideShowMode || !_firstImageLoaded) 
				_pagination.click(_currentIndex, false);
				
			super.startTransitionOfCurrent();
		}
		
		/**
		 * @private
		 */
		protected function showPreloader():void
		{
			destroyLoaderInfo();
			
			_loaderInfo = _currentKenBurnImage.imageView.loader.contentLoaderInfo;
			_loaderInfo.addEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler, false, 0, true);
			
			addChild(_preloader);
			TweenLite.to(_preloader, 0.7, {alpha:1});
		}
		
		/**
		 * @private
		 */
		protected function hidePreloader():void
		{
			destroyLoaderInfo();
			
			TweenLite.to(_preloader, 0.7, {alpha:0, onComplete:function () {
				_preloader.progress = 0.0;
				if (_preloader.stage) removeChild(_preloader);
			}});
		}
		
		override protected function load():void
		{
			super.load();
			
			if (!_firstImageLoaded)
			{
				showPreloader();
			}
		}
		
		/**
		 * @private
		 */
		protected function destroyLoaderInfo():void
		{
			if (!_loaderInfo) return;
			
			_loaderInfo.removeEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler);
			_loaderInfo = null;
		}
	}
}