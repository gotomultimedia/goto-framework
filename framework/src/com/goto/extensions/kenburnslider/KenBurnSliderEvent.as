//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.kenburnslider
{

	import flash.events.Event;

	/**
	 * Event subclass.
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Silverfenix
	 * @since  21.08.2011
	 */
	public class KenBurnSliderEvent extends Event
	{
	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------			
		
		public static const SCALE_COMPLETE:String = "scaleComplete";
		public static const FADE_IN_COMPLETE:String = "fadeInComplete";
		public static const FADE_OUT_COMPLETE:String = "fadeOutComplete";
		public static const LOAD_COMPLETE:String = "loadComplete";
		public static const DISPLAY_TIME_COMPLETE:String = "displayTimeComplete";
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		public var params:Object;
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		
		/**
		 *	@constructor
		 */
		public function KenBurnSliderEvent(type:String, params:Object = null, bubbles:Boolean = true, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.params = params;
		}
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		override public function clone():Event
		{
			return new KenBurnSliderEvent(type, params, bubbles, cancelable);
		}
		
		override public function toString():String
        {
            return formatToString("KenBurnSliderEvent", "params", "type", "bubbles", "cancelable");
        }
	}
	
}

