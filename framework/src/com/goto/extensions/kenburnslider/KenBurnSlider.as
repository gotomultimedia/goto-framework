//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.kenburnslider
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.ImageView;
	import com.goto.utils.ObjectUtils;
	import flash.display.Bitmap;
	import fl.motion.easing.Linear;
	import com.greensock.TweenMax;
	import flash.geom.Rectangle;
	import flash.display.BitmapData;
	
	import com.goto.utils.Console;
	import com.goto.display.View;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Silverfenix
	 * @since  19.08.2011
	 */
	public class KenBurnSlider extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		protected var _data:Array;
		protected var _nextIndex:int = 0;
		protected var _currentIndex:uint;
		protected var _isWaitingForLoadComplete:Boolean;
		protected var _slideShowMode:Boolean = true;
		
		protected var _imageHolder:Sprite;
		protected var _kenBurnImage1:KenBurnImage;
		protected var _kenBurnImage2:KenBurnImage;
		
		protected var _currentKenBurnImage:KenBurnImage;
		protected var _currentKenBurnImageDisplaying:KenBurnImage;
		protected var _firstImageLoaded:Boolean = false;
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function KenBurnSlider($settings:Object = null) 
		{
			settings = {
				transitionTime: 2,
				displayTime: 10,
				scaleFactor: 1.20
			}
			
			if ($settings) ObjectUtils.extend(settings, $settings);
			
			super();
		}
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		public var settings:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------

		//---------------------------------------
		// OVERRIDED METHODS
		//---------------------------------------
				
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			_imageHolder = new Sprite();
			addChild(_imageHolder);
			
			// Add subviews here
			_kenBurnImage1 = new KenBurnImage()
				.onLoadComplete(onLoadComplete, true)
				.onDisplayTimeComplete(onDisplayTimeComplete, true)
				.onFadeInComplete(onFadeInComplete, true)
				.onScaleComplete(onScaleComplete, true)
				.setPropertiesFromArray([
					"name", "uno",
					"defaultTransition", KenBurnImage.SMALL_TO_BIG,
					"transitionTime", settings.transitionTime,
					"displayTime", settings.displayTime,
					"scaleFactor", settings.scaleFactor
				]);
			
			_kenBurnImage2 = new KenBurnImage()
				.onLoadComplete(onLoadComplete, true)
				.onDisplayTimeComplete(onDisplayTimeComplete, true)
				.onFadeInComplete(onFadeInComplete, true)
				.onScaleComplete(onScaleComplete, true)
				.setPropertiesFromArray([
					"name", "dos",
					"defaultTransition", KenBurnImage.BIG_TO_SMALL,
					"transitionTime", settings.transitionTime,
					"displayTime", settings.displayTime,
					"scaleFactor", settings.scaleFactor
				]);			
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			super.draw();
			
			_kenBurnImage1.setSize(__width, __height);
			_kenBurnImage2.setSize(__width, __height);
			
			// mask the component
			scrollRect = new Rectangle(0, 0, __width, __height);
		}
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		public function set data(value:Array):void
		{
			setData(value);
		}

		public function get data():Array
		{
			return _data;
		}
		
		public function get currentKenBurnImageDisplaying():KenBurnImage
		{
			return _currentKenBurnImageDisplaying;
		}
		
		public function get currentKenBurnImageWaiting():KenBurnImage
		{
			return _currentKenBurnImageDisplaying == _kenBurnImage1 ? _kenBurnImage2 : _kenBurnImage1;
		}
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		public function setData(value:Array):*
		{
			_data = value;
			
			// restart
			_firstImageLoaded = false;
			_slideShowMode = _data.length > 1;
			_nextIndex = 0;
			_currentIndex = 0;
			
			load();
			
			return this;
		}		
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function showImageByIndex(index:uint, slideShowMode:Boolean = false):void
		{
			if (!_data) return;
			if (index > _data.length-1) index = _data.length-1;
			if (currentKenBurnImageDisplaying && _data[index] == currentKenBurnImageDisplaying.source) 
			{
				//Console.log("La imagen: '$1' ya se está visualizando", _data[index]);
				return;
			}
			
			_slideShowMode = slideShowMode;
			_nextIndex = index;
			_currentIndex = index;
			
			load();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 1		
			if (_kenBurnImage1.stage) _kenBurnImage1.parent.removeChild(_kenBurnImage1);
			_kenBurnImage1.destroy();
			_kenBurnImage1 = null;
			
			// 2		
			if (_kenBurnImage2.stage) _kenBurnImage2.parent.removeChild(_kenBurnImage2);
			_kenBurnImage2.destroy();
			_kenBurnImage2 = null;
			
			_currentKenBurnImage = null;
			_currentKenBurnImageDisplaying = null;
			
			super.destroy();
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		protected function onLoadComplete(event:KenBurnSliderEvent):void
		{
			//Console.log("LoadComplete of $1", event.params.target.name);
			
			//Console.log("Load Complete, is waiting: $1, First image was loaded: $2", _isWaitingForLoadComplete, _firstImageLoaded);
			
			if (_isWaitingForLoadComplete || !_slideShowMode || !_firstImageLoaded)
			{	
				_firstImageLoaded = true;
				_isWaitingForLoadComplete = false;
				startTransitionOfCurrent();
			}
		}
		
		protected function onDisplayTimeComplete(event:KenBurnSliderEvent):void
		{
			if (!_slideShowMode) return;
			
			//Console.log("DisplayTimeComplete of: $1", event.params.target.name);
			
			if (_currentKenBurnImage.isLoading)
			{
				//Console.log("waitingForLoadComplete of: $1", _currentKenBurnImage.name);
				_isWaitingForLoadComplete = true;
			}
			else
			{
				startTransitionOfCurrent();
			}
		}
		
		protected function onFadeInComplete(event:KenBurnSliderEvent):void
		{
			//Console.log("fade complete of: 	$1 ", event.params.target.name)
			if (_slideShowMode) load(); // Load Next
		}

		protected function onScaleComplete(event:KenBurnSliderEvent):void
		{
			// statements
		}
		
		protected function startTransitionOfCurrent():void
		{
			//Console.log("Start Transition in: $1", _currentKenBurnImage.name);
			
			_currentKenBurnImage.startTransition();
			_imageHolder.addChild(_currentKenBurnImage);
			
			_currentKenBurnImageDisplaying = _currentKenBurnImage;
			
			// Loop
			if (_currentIndex < _data.length-1) 
				_currentIndex++;
			else
				_currentIndex = 0;
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		protected function load():void
		{
			_currentKenBurnImage = _currentKenBurnImage ? currentKenBurnImageWaiting : _kenBurnImage1;

			// NO slide show mode
			if (!_slideShowMode) 
			{				
				//Console.log("Loading Source '$1' in kenBurImage: '$2'", _data[_nextIndex], _currentKenBurnImage.name);
				_currentKenBurnImage.source = _data[_nextIndex];
				
				return;
			}
			
			// Slide show mode
			//Console.log("Loading Source '$1' in kenBurImage: '$2'", _data[_nextIndex], _currentKenBurnImage.name);
			
			_currentKenBurnImage.source = _data[_nextIndex];
			
			// Loop
			if (_nextIndex < _data.length-1) 
				_nextIndex++;
			else
				_nextIndex = 0;
				
			//Console.log("nextIndex: $1", _nextIndex);
		}
		
		
	}
}