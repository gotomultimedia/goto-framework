//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.reel
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  23.11.2011
	 */
	public class VideoReel extends Reel 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function VideoReel() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------

		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			super.destroy();
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
				
		override protected function itemForRowAtIndex(index:uint):ReelItem
		{
			var item:*;
			
			if (_data[index].videoSrc)
			{
				item = new ReelVideoItem( dataForItemAtIndex(index) );
			}
			else
			{
				item = new ReelItem( dataForItemAtIndex(index) );
			}
				
			return  item as ReelItem;
		}
	}
}

