//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.reel
{
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	import com.goto.media.vplayer.*;
	import com.greensock.TweenLite;
	
	//import com.goto.utils.Console;
	
	/**
	 * ReelItem subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  23.11.2011
	 */
	public class ReelVideoItem extends ReelItem
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function ReelVideoItem($data:Object) 
		{
			super($data);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------

		protected var _VPlayer:VPlayer;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		override public function load():*
		{
			super.load();
			
			if (_VPlayer) _VPlayer.source = data.videoSrc;
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			destroyVPlayer(true);
			
			super.destroy();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function focus():void
		{
			super.focus();
			createVPlayer();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function unfocus():void
		{
			super.unfocus();

			destroyVPlayer(false);
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			super.draw();
			
			if (_VPlayer) _VPlayer.setSize(__width, __height);
		}
		
		
		/**
		 * @private
		 */
		protected function createVPlayer():void
		{
			var playerSettings:Object =
			{
				hideVideoOnFinish: true,
				backgroundAlpha: 0,
				controlsAutoHide: true,
				scaleMode: "fitToWidthAndHeight",	// full, fitToWidthAndHeight, noScale
				color: 0xFFFFFF,
				tooltipType: "none",
				glossy: true,
				backgroundColor: 0xCCCCCC
				//controlsScale: 3,
				//allowFullScreen: true,
				//fullScreenOnPlay: false,
				//iconsAlign: "topRight",			// topLeft, topRight, bottomLeft, bottomRight
				//grid: true,
				//gridPresset: 2,
				//bufferTime: 3,
			};
			
			if (data) playerSettings.source = data.videoSrc;
			
			// Chain style
			_VPlayer = new VPlayer()
				.setSize(__width, __height)
				.addListener(VPlayerEvent.VPLAYER_PLAY, showVideoBackground, false)
				.addListener(VPlayerEvent.VPLAYER_VIDEO_FINISH, hideVideoBackground, false)
				.setProperties(playerSettings)
				.addInto(this, getChildIndex(_image) + 1); // At child at 
		}
		
		/**
		 * @private
		 */
		protected function destroyVPlayer(immediately:Boolean):void
		{
			if (!_VPlayer) return;
			
			if (immediately)
			{
				_VPlayer.destroy();
				_VPlayer = null;
			}
			else
			{
				TweenLite.to(_VPlayer, 0.5, {alpha:0, onComplete:destroyVPlayer, onCompleteParams:[true]});
			}
		}
		
		/**
		 * @private
		 */
		protected function showVideoBackground():void
		{
			TweenLite.to(_VPlayer, 0.5, {backgroundAlpha:1});
		}	
		
		/**
		 * @private
		 */
		protected function hideVideoBackground():void
		{
			TweenLite.to(_VPlayer, 0.5, {backgroundAlpha:0});
		}
		
	}
}

