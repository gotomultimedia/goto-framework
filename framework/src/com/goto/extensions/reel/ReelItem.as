//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.reel
{
	
	import flash.events.Event;
	import com.goto.display.View;
	import com.goto.display.ImageView;
	import com.goto.utils.ObjectUtils;
	import com.greensock.TweenLite;
	import fl.motion.easing.Exponential;
	import flash.events.MouseEvent;
	import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author tonio, fenixkim
	 * @since  19.11.2011
	 */
	public class ReelItem extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function ReelItem($data:Object) 
		{
			data = {
				"src": null
			}

			ObjectUtils.extend(data, $data);

			super();
		}

		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------

		public var data:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _image:ImageView;
		protected var _index:int = -1;
		protected var _focused:Boolean;
		protected var _minAlpha:Number = 0.1;
		protected var _info:ReelItemInfo;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
					
		public function get imageView():ImageView
		{
			return _image;
		}
		
		public function get index():int
		{
			return _index;
		}

		public function set index(value:int):void
		{
			if (value !== _index)
			{
				_index = value;
			}
		}
		
		public function get focused():Boolean
		{
			return _focused;
		}

		public function set focused(value:Boolean):void
		{
			if (value !== _focused)
			{
				_focused = value;
				
				if (_focused)
					focus();
				else
					unfocus();
			}
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		public function load():*
		{
			_image.source = data.src;
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_image.destroy();
			
			if (_info) _info.destroy();
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function focus():void
		{
			buttonMode = false;
			mouseChildren = true;
			if (_image.image) TweenLite.to(_image.image, 0.7, {alpha:1});
			if (_info) TweenLite.to(_info, 0.7, {alpha:1});
		}
		
		/**
		 * @private
		 */
		public function unfocus():void
		{
			buttonMode = true;
			mouseChildren = false;
			if (_image.image) TweenLite.to(_image.image, 0.7, {alpha:_minAlpha});
			if (_info) TweenLite.to(_info, 0.7, {alpha:0});
		}

		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function onLoadComplete():void
		{
			// Add mouse events
			addEventsTolistener(mouseHandler, MouseEvent.MOUSE_OVER, MouseEvent.MOUSE_OUT);
			
			// Initial transition
			_image.image.alpha = 0;
			TweenLite.to(_image.image, 0.7, {alpha:_focused	? 1 : _minAlpha});
			
			
			if (data.infoTitle || data.infoDescription)
			{
				_info = new ReelItemInfo(ObjectUtils.propsStartingIn("info", data, true))
					.setSize(__width)
					.setProperties({alpha:0})
					.addInto(this);
							
				TweenLite.to(_info, 0.7, {alpha:_focused ? 1 : 0});
			}
		}
		
		/**
		 * @private
		 */
		protected function mouseHandler(event:MouseEvent):void
		{
			if (!_image.image || _focused) 
			{
				event.stopImmediatePropagation();
				return;
			}
			
			switch (event.type)
			{
				case MouseEvent.MOUSE_OVER :
					TweenLite.to(_image.image, 0.7, {alpha:0.3, ease:Exponential.easeOut});
					break;
					
				case MouseEvent.MOUSE_OUT :
					TweenLite.to(_image.image, 0.7, {alpha:_minAlpha, ease:Exponential.easeOut});
					break;
			}
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function init():void
		{
			buttonMode = true;
			mouseChildren = false;
			_focused = false;
			
			super.init();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_image = new ImageView().onLoadComplete(onLoadComplete);
			_image.scaleMode = "fitToHeight";
			addChild(_image);
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			super.draw();
			
			_image.height = __height;
			__width = _image.width;
			
			if (_info)
			{
				_info.width = __width;
				_info.y = __height - _info.height;
			}
		}
			
	}
}