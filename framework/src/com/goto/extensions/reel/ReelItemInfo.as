//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.extensions.reel
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.View;
	import com.goto.display.Draw;
	import com.goto.utils.ColorUtils;
	import com.goto.utils.ObjectUtils;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import fl.motion.easing.Exponential;
	
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  23.11.2011
	 */
	public class ReelItemInfo extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		
		/**
		 *	@constructor
		 */
		public function ReelItemInfo($data:Object) 
		{
			data = {
				backgroundColor: 0xCCCCCC,
				descriptionColor: 0x666666,
				titleColor: 0x666666,
				title: null,
				description: null,
				titleFont: null,
				descriptionFont: null
			}
			
			ObjectUtils.extend(data, $data);
			
			super();
		}
		
		//---------------------------------------
		// PUBLIC VARIABLES
		//---------------------------------------
		
		public var data:Object;
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _group:Sprite;
		protected var _title:TextField;
		protected var _description:TextField;
		protected var _background:Sprite;
		protected var _tH:Number = 0; 			// title height
		protected var _dH:Number = 0; 			// description height
		
		protected const MARGIN:uint = 10;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			// 
			super.destroy();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function show(duration:Number = 0.7, options:Object = null):*
		{
			if (!_isHidden) return this;
			_isHidden = false;
			
			tweenTo({target:_background, alpha:0.8}, 0.7);
			
			if (_description) 
			{
				tweenTo({target:_description, alpha:1}, 0.7);
				tweenTo({target:_group, y:0, ease:Exponential.easeOut}, 0.5);
			}			
		}
		
		/**
		 * @inheritDoc
		 */
		override public function hide(duration:Number = 0.7, options:Object = null):*
		{
			if (_isHidden) return this;
			_isHidden = true;
			
			tweenTo({target:_background, alpha:0}, 0.4);
			
			if (_description)
			{
				tweenTo({target:_description, alpha:0}, 0.4);
				tweenTo({target:_group, y:_dH, ease:Exponential.easeOut}, 0.5);
			}
		}
	
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			this.addListener(MouseEvent.ROLL_OVER, show, false)
				.addListener(MouseEvent.ROLL_OUT, hide, false)
				.hide();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			
			_group = new Sprite();
			addChild(_group);
			
			_background = Draw.rectangle(200, 50, data.backgroundColor, 0.0);
			_group.addChild(_background);
			
			if (data.title)
			{
				if (!data.titleFont) 
					throw new ArgumentError("A font class definition is required for the title");
				
				_title = (new data.titleFont).tf;
				_title.autoSize = "left";
				_title.multiline = true;
				_title.textColor = data.titleColor;
				_title.text = data.title;
				_title.x = MARGIN;
				_title.y = MARGIN;
				
				_group.addChild(_title);
			}
			
			if (data.description)
			{
				if (!data.descriptionFont) 
					throw new ArgumentError("A font class definition is required for the description");
					
				_description = (new data.descriptionFont).tf;
				_description.autoSize = "left";
				_description.multiline = true;
				_description.wordWrap = true;
				_description.htmlText = "<font color='"+ ColorUtils.numberToHexString(data.descriptionColor) +"'>"+data.description+"</font>";
				_description.x = MARGIN;
				_description.alpha = 0;
				
				_group.addChild(_description);
			}
			
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			if (_title)
			{
				_title.width = __width - MARGIN * 2;
				_tH = _title.height;
			}
			
			if (_description)
			{
				_description.width = __width - MARGIN * 2;
				_description.y = (_title) ? _title.y + _title.height : MARGIN;				
				
				_dH = _description.height;
			}
			
			__height = _tH + _dH + MARGIN * 2;
			
			_background.width = __width;
			_background.height = __height;
			
			if (_dH)
			{
				_group.y = (_isHidden) ? _dH : 0;
			}
			
			// View mask
			scrollRect = new Rectangle(0, 0, __width, __height);
			
			super.draw();
		}		
	}
}