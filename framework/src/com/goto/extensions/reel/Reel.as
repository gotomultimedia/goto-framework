//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////


package com.goto.extensions.reel
{
	
	import com.goto.display.View;
	import com.goto.display.layout.HLayout;
	import com.greensock.TweenLite;
	import fl.motion.easing.Exponential;
	import flash.events.MouseEvent;
	import flash.display.LoaderInfo;
	import com.goto.controls.PiePreloader;
	import flash.events.ProgressEvent;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author tonio, fenixkim
	 * @since  19.11.2011
	 */
	public class Reel extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function Reel() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _data:Array;
		protected var _items:Array;
		protected var _layout:HLayout;
		protected var _loadIndex:uint;
		protected var _selected:ReelItem;
		protected var _selectedIndex:uint;
		protected var _preloader:PiePreloader;
		protected var _loaderInfo:LoaderInfo;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		public function get data():Array
		{
			return _data;
		}

		public function set data(value:Array):void
		{
			if (value !== _data)
			{
				_data = value;
				createItems();
				loadNext();
			}
		}
		
		public function get selected():ReelItem
		{
			return _selected;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			destroyItems(_items);
			_items = null;
			
			if (_preloader)
			{
				_preloader.destroy();
				
				if (_preloader.stage) 
					_preloader.parent.removeChild(_preloader);
				
				_preloader = null;
			}
			
			super.destroy();
		}
		
		/**
		 * @private
		 */
		public function click(index:uint):void
		{
			// Fix index offset
			if (index > _items.length-1) 
				index = _items.length-1;
			
			_items[index].dispatchEvent(new MouseEvent(MouseEvent.CLICK)); // CLICK
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function currentImageProgressHandler(e:ProgressEvent):void
		{
			var progress = (e.bytesLoaded * 100) / e.bytesTotal;
			_preloader.progress = progress;
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_layout = new HLayout();
			addChild(_layout);
			
			// Preloader
			_preloader = new PiePreloader();
			_preloader.color = 0xECECEC;
			_preloader.x = 5;
			_preloader.y = 5;
			_preloader.alpha = 0;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			super.draw();
			
			_preloader.x = __width - _preloader.width;
			_preloader.y = __height + 15;
			
			for (var p:String in _items)
			{
				_items[p].height = __height;
			}
			
			_layout.draw();
			
			showSelectedItemWithTween(false);
		}	
		
		/**
		 * @private
		 */
		protected function createItems():void
		{
			for (var p:String in _data)
			{
				var reelItem:ReelItem = itemForRowAtIndex(uint(p));
				reelItem.addInto(this).onClick(reelItemClicked, false, [reelItem]);
				reelItem.index = int(p);
				
				//reelItem.source = _data[p];
				
				if (!_items) _items = [];
				_items.push(reelItem);
			}
		}
		
		/**
		 * @private
		 */
		protected function loadNext():void
		{
			if (_loadIndex > _items.length-1) 
				return;
				
			var reelItem:ReelItem = _items[_loadIndex];
			reelItem.imageView.onLoadComplete(addItemToLayout, false, [reelItem]);
			reelItem.load();
			
			showPreloader(reelItem.imageView.loader.contentLoaderInfo);
			
			_loadIndex++;
		}
		
		/**
		 * @private
		 */
		protected function addItemToLayout(item:ReelItem):void
		{
			hidePreloader();
			
			item.height = __height;
			item.addInto(_layout);
			
			if (_selectedIndex == item.index)
			{
				// Focus the item by index
				TweenLite.delayedCall(0.3, showSelectedItemWithTween);
			}
			
			loadNext();
		}
		
		/**
		 * @private
		 */
		protected function reelItemClicked(item:ReelItem):void
		{
			if (_selected && _selected != item)
			{
				_selected.focused = false;
			}
			
			_selected = item;
			_selected.focused = true;
			_selectedIndex = _selected.index;
			
			showSelectedItemWithTween();
		}
		
		/**
		 * @private
		 */
		protected function showSelectedItemWithTween(tween:Boolean = true):void
		{
			if (!_selected) return;
			
			if (tween)
				TweenLite.to(_layout, 1, {x:getLayoutPos(), ease:Exponential.easeInOut});
			else
				_layout.x = getLayoutPos();
		}
		
		/**
		 * @private
		 */
		protected function getLayoutPos():Number
		{
			// selected postion
			var pos:Number = -_selected.x + ((__width - _selected.width) * 0.5);
			// Avalilable right space
			var a:Number = __width - _layout.width;
			
			// fix left
			if (_layout.width < __width || pos > 0) return 0;
			
			// fix right
			if (pos < a) return a;
			
			return pos;
		}
	
		/**
		 * @private
		 */
		protected function itemForRowAtIndex(index:uint):ReelItem
		{
			return new ReelItem( dataForItemAtIndex(index) );
		}
		
		/**
		 * @private
		 */
		protected function dataForItemAtIndex(index:uint):Object
		{
			return _data[index];
		}
		
		/**
		 * @private
		 */
		protected function showPreloader(loaderInfo:LoaderInfo):void
		{
			destroyLoaderInfo();
			
			_loaderInfo = loaderInfo;
			_loaderInfo.addEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler, false, 0, true);
			
			addChild(_preloader);
			TweenLite.to(_preloader, 0.7, {alpha:1});
		}
		
		/**
		 * @private
		 */
		protected function hidePreloader():void
		{
			destroyLoaderInfo();
			
			TweenLite.to(_preloader, 0.7, {alpha:0, onComplete:function () {
				_preloader.progress = 0.0;
				removeChild(_preloader);
			}});
		}
		
		/**
		 * @private
		 */
		protected function destroyLoaderInfo():void
		{
			if (!_loaderInfo) return;
			
			_loaderInfo.removeEventListener(ProgressEvent.PROGRESS, currentImageProgressHandler);
			_loaderInfo = null;
		}
	}
}