/*
Player.as
vPlayer

Created by Silverfenix on 20/09/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
*/
package com.goto.media
{	
	import flash.display.Sprite;
	import flash.events.Event;
	
	import com.goto.display.View;
	import com.goto.display.Draw;
	import com.goto.display.Fill;
	import com.goto.utils.ColorUtils;
	import flash.geom.Rectangle;
	
	public class VideoPlayerCore extends View
	{		
		protected var _isPlaying:Boolean;
		protected var _wasPlayed:Boolean;
		protected var _looping:Boolean;
		protected var _duration:Number = 0;
		protected var _bufferTime:Number = 3;
		protected var _loop:Boolean;
		protected var _background:Fill;
		protected var _backgroundColor:uint = 0x333333;
		protected var _source:String;
		protected var _scaleMode:String = ScaleMode.PROPORTIONAL_INSIDE;
		protected var _bubbles:Boolean;
		protected var _volume:Number = 1;
		protected var _volumeCache:Number = 1;
		protected var _mute:Boolean;
		protected var _state:String = VideoState.DISCONNECTED;
		protected var _currentTime:Number;
		protected var _bytesLoaded:uint;
		protected var _bytesTotal:uint;
		protected var _type:String;
		protected var _paused:Boolean;
		
		public function VideoPlayerCore () 
		{
			super();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////
		
		
		override protected function init ():void
		{
			super.init();
			
			// Set Default Size
			__width = 320;
			__height = 240;
		}
		
		override protected function addChildren():void
		{
			_background = Fill.solidWithSize(_backgroundColor, 100, 100);
			
			addChild(_background);
		}
		
		//---------------------------------------
		// PUBLIC CHAIN EVENT METHODS
		//---------------------------------------
		
		public function onStatusChange(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(PlayerEvent.STATUS_CHANGE, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onStreamError(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(PlayerEvent.STREAM_ERROR, listener, sendEventToListener, args, once);
			return this;
		}
		
		//---------------------------------------
		// PUBLIC CHAIN SETTER METHODS
		//---------------------------------------
		
		public function setLoop(value:Boolean):*
		{
			_loop = value;
			return this;
		}
		
		public function setBackgroundColor(value:uint, alpha:Number = NaN):*
		{
			_backgroundColor = value;
			_background.setColor(_backgroundColor);
			
			if (!isNaN(alpha))
			{
				backgroundAlpha = alpha;
			}
			
			return this;
		}
		
		public function setScaleMode(value:String, redraw:Boolean = true):*
		{
			_scaleMode = value;
			
			if (redraw)	draw();
			
			return this;
		}
		
		public function setSource(value:String):*
		{
			_source = value;
			return this;
		}
		
		public function setBufferTime(value:Number):*
		{
			bufferTime = value;
			return this;
		}
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		
		public function play (videoSource:String = null):* {
			return this;
		}
		
		//---------------------------------------
		// PUBLIC METHODS
		//---------------------------------------
		
		override public function draw ():void
		{
			scrollRect = new Rectangle(0, 0, __width, __height);
			
			_background.setSize(__width, __height);
			
			super.draw();
		}
		
		
		public function stop ():void {}		
		public function pause ():void {}
		public function togglePause ():void {}
		public function resume ():void {}
		public function seekTo (seconds:Number):void {}
		public function reset (fireVideoFinishEvent:Boolean = false, fireVideoResetEvent:Boolean = true):void {}
		
		public function mute ():void
		{
			if (_mute) 
				return;
			
			_mute = true;
			_volumeCache = _volume;
			volume = 0;
		}
		
		public function unmute ():void
		{
			if (!_mute) return;
			
			_mute = false;
			volume = _volumeCache;
		}
		
		override public function destroy ():void 
		{
			_background.destroy();
			super.destroy();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		protected function changeState (newStatus:String):void
		{
			var eventData:Object = {
				target: this, 
				status: newStatus, 
				previousState: _state
			};
			
			_state = newStatus;
			
			dispatchEvent(new PlayerEvent(PlayerEvent.STATUS_CHANGE, eventData, _bubbles));
		}
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		public function get type ():String 
		{
			return _type;
		}
		
		public function set source (value:String):void 
		{
			_source = value;
		}
		
		public function get source ():String 
		{
			return _source;
		}
		
		public function set volume (value:Number):void 
		{
			_volume = value;
		}
		
		public function get volume ():Number 
		{
			return _volume;
		}
		
		public function set scaleMode (value:String):void 
		{
			setScaleMode(value);
		}
		
		public function get scaleMode ():String 
		{
			return _scaleMode;
		}
		
		public function set loop (value:Boolean):void 
		{
			setLoop(value);
		}
		
		public function get loop ():Boolean 
		{
			return _loop;
		}
		
		public function set bubbles (value:Boolean):void 
		{
			_bubbles = value;
		}
		
		public function get bubbles ():Boolean 
		{
			return _bubbles;
		}
		
		public function get state ():String 
		{
			return _state;
		}
		
		public function set backgroundColor (value:uint):void 
		{
			setBackgroundColor(_backgroundColor);
		}
		
		public function get backgroundColor ():uint 
		{
			return _backgroundColor;
		}
		
		public function set backgroundAlpha (value:Number):void 
		{
			_background.alpha = value;
		}
		
		public function get backgroundAlpha ():Number 
		{
			return _background.alpha;
		}
		
		public function get currentTime ():Number
		{
			return _currentTime;
		}
		
		public function get bytesLoaded ():uint
		{
			return _bytesLoaded;
		}
		
		public function get bytesTotal ():uint
		{
			return _bytesTotal;
		}
		
		public function get duration ():uint
		{
			return _duration;
		}
		
		/**
		 * Sets / gets the buffer time in seconds
		 */
		public function set bufferTime(value:Number):void
		{
			_bufferTime = value;
		}
				
		public function get bufferTime():Number
		{
			return _bufferTime;
		}
	}
}