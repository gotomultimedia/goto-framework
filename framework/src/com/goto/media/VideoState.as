/*
VideoState.as
vPlayer

Created by Silverfenix on 22/09/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
*/
package com.goto.media
{
	public class VideoState 
	{
		public static const PLAYING:String = "playing";
		public static const BEGINNING_PLAY:String = "beginningPlay";
		public static const STOPPED:String = "stoped";
		public static const PAUSED:String = "paused";
		public static const BUFFERING:String = "buffering";
		public static const BUFFER_FULL:String = "bufferFull";
		public static const FINISHED:String = "finished";
		public static const LOADING:String = "loading";
		public static const DISCONNECTED:String = "disconected";
		public static const NO_INIT:String = "noInit";
		public static const LOOPING:String = "looping";
		
		/**
		 * // No implemented yet
		 * public static const CONNECTION_ERROR:String = "connectionError";
		 * public static const DISCONNECTED:String = "disconected";
		 * public static const LOADING:String = "loading";
		 * public static const REWINDING:String = "rewinding";
		 * public static const SEEKING:String = "seeking";
		 */
	}
}