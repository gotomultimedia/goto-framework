//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.media
{
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Alexander Ruiz Ponce
	 * @since  03.08.2012
	 */
	public class PlayerType
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const CHROMELESS:String = "chromeless";
		public static const YOUTUBE:String = "youtube";
				
	}
}