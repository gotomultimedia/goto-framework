//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.media
{
	/**
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Alexander Ruiz Ponce
	 * @since  03.08.2012
	 */
	public class ScaleMode
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		public static const NONE:String = "none";
		public static const PROPORTIONAL_INSIDE:String = "proportionalInside";
		public static const PROPORTIONAL_OUTSIDE:String = "proportionalOutside";
		
	}
}

