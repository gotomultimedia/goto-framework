/*
 YouTubePlayer.as
 vPlayer
 
 Created by Silverfenix on 14/09/10.
 Copyright 2010 goTo! Multimedia. All rights reserved.
 */
package com.goto.media
{
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.system.Security;
	
	public class YouTubePlayer extends VideoPlayerCore implements IVideoPlayer
	{
		private var _loader:Loader;
		private var _player:Object;
		private var _apiPlayerURL:String = "http://www.youtube.com/apiplayer?version=3";
		private var _isReady:Boolean;
		private var _playVideoOnReady:Boolean;
		
		public function YouTubePlayer () 
		{
			Security.allowDomain("www.youtube.com");  
			Security.allowDomain("youtube.com");  
			Security.allowDomain("s.ytimg.com");  
			Security.allowDomain("i.ytimg.com");
			
			super();
			
			_type = PlayerType.YOUTUBE;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////

		override protected function onComponentReady ():void
		{
			load();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		override public function draw ():void
		{
			if (_player) {
				_player.setSize(__width, __height);
			}
			
			super.draw();
		}
		
		override public function play (videoSource:String = null):*
		{
			// validates resume
			if (_wasPlayed && !videoSource) {
				resume();
				return;
			}
			
			if (videoSource) {
				_source = validateURL(videoSource);
			}
			
			if (_source) 
			{	
				if (_isReady) {
					_wasPlayed = true;
					_player.loadVideoById(_source);
				} else {
					_playVideoOnReady = true;
				}
				//dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_PLAY, {target:this, source:_source}, _bubbles));
				
			} else {
				trace(this + " Warning: not video URL found or invalid youtube video URL");
			}
			
			return this;
		}
		
		override public function stop ():void
		{
			_playVideoOnReady = false;
			if (!_isReady) return;
			_player.stopVideo();
			changeState(VideoState.STOPPED);
		}
		
		override public function pause ():void
		{
			if (!_isReady) return;
			_player.pauseVideo();
			changeState(VideoState.PAUSED);
		}
		
		override public function togglePause ():void
		{
			if (!_isReady) return;
			
			if (_state == VideoState.PAUSED) {
				_player.playVideo();
				changeState(VideoState.PLAYING);
			} else {
				_player.pauseVideo();
				changeState(VideoState.PAUSED);
			}
		}
		
		override public function resume ():void
		{
			if (!_isReady) return;
			_player.playVideo();
			changeState(VideoState.PLAYING);
		}
		
		override public function seekTo (seconds:Number):void
		{
			if (!_isReady) return;
			_player.seekTo(seconds, true);
		}
		
		override public function reset (fireVideoFinishEvent:Boolean = false, fireVideoResetEvent:Boolean = true):void
		{
			_wasPlayed = false;
			
			//if (fireVideoFinishEvent) dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_VIDEO_FINISH, {target:this}, _bubbles));
			//if (fireVideoResetEvent) dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_RESET, {target:this}, _bubbles));
			if (fireVideoFinishEvent) changeState(VideoState.FINISHED);
		}
		
		override public function destroy ():void
		{
			if (!_isReady) return;
			_player.destroy();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		private function onLoaderInit (e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.INIT, onLoaderInit);
			
			_loader.content.addEventListener("onReady", onPlayerReady);
			_loader.content.addEventListener("onError", onPlayerError);
			_loader.content.addEventListener("onStateChange", onPlayerStateChange);
			//_player.addEventListener("onPlaybackQualityChange", onVideoPlaybackQualityChange);
		}
		
		private function onPlayerReady (e:Event):void
		{
			_isReady = true;
			
			_player = _loader.content;
			addChild(_player as DisplayObject);
			
			// A quick transition
			tweenFrom({target:_player, alpha:0}, 0.5);
			
			draw();
			
			if (_playVideoOnReady) {
				play(_source);
			}
			
			// Set the actual volume
			_player.setVolume(_volume * 100);
		}
		
		private function onPlayerError (e:Event):void
		{
			dispatchEvent(new PlayerEvent(PlayerEvent.STREAM_ERROR, {target:this, message:"the video stream '" + _source + "' wasn't found"}, _bubbles));
		}
		
		private function onPlayerStateChange (e:Event):void
		{
			// finalizado
			switch (_player.getPlayerState()) {
				case -1:
					//reset(true);
					changeState(VideoState.NO_INIT);
					break;
				case 0:
					reset(false);
					changeState(VideoState.FINISHED);
					break;
				case 1:
					changeState(VideoState.PLAYING);
					break;
				case 2:
					changeState(VideoState.PAUSED);
					break;
				case 3:
					changeState(VideoState.BUFFERING);
					break;
					/*
				case 5:
					changeState(VideoState.BUFFERING);
					break;*/
			}
		}
		
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		private function load ():void
		{
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.INIT, onLoaderInit, false, 0, true);
			_loader.load(new URLRequest(_apiPlayerURL));
		}
		
		private function validateURL (URL:String):String
		{
			if (!URL) return null;
			
			// Simple ID
			if (URL.length == 11) return URL;

			// Extract ID from URL
			var matchFullURL:Array = URL.match(/youtube.com\/watch\?v=([-a-zA-Z0-9_-]+)*/i);
			if (matchFullURL) return matchFullURL[1];
			
			return null;
		}
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		override public function set volume (value:Number):void
		{
			_volume = value;
			
			if (_player)
				_player.setVolume(_volume * 100);
		}
		
		override public function get currentTime ():Number
		{
			if (!_player) return 0;
			return _player.getCurrentTime();
		}
		
		override public function get bytesLoaded ():uint
		{
			if (!_player) return 0;
			return _player.getVideoBytesLoaded();
		}
		
		override public function get bytesTotal ():uint
		{
			if (!_player) return 0;
			return _player.getVideoBytesTotal();
		}
		
		override public function get duration ():uint
		{
			if (!_player) return 0;
			return _player.getDuration();
		}
	}
}