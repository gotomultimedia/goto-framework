/*
IVideoPlayer.as
vPlayer

Created by Silverfenix on 20/09/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
*/
package com.goto.media
{
	public interface IVideoPlayer
	{
		function play (videoSource:String = null):*;
		function pause ():void;
		function togglePause ():void;
		function resume ():void;
		function stop ():void;
		function mute ():void;
		function unmute ():void;
		function destroy ():void;
		function seekTo (seconds:Number):void;
		function set source (value:String):void;
		function get source ():String;
		function set volume (value:Number):void;
		function get volume ():Number;
		function set scaleMode (value:String):void
		function get scaleMode ():String;
		function set loop (value:Boolean):void
		function get loop ():Boolean;
		/*
		 getBytesLoaded
		 getBytesTotal
		 getCurrentTime
		 getDuration
		 */
	}
}