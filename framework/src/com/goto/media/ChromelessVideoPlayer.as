/*
GenericPlayer.as
vPlayer

Created by Silverfenix on 20/09/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
*/
package com.goto.media
{
	import flash.media.SoundTransform;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Video;
	import com.goto.net.EasyNetStream;
	import com.goto.utils.DisplayObjectUtils;
	import fl.motion.easing.Linear;
	
	public class ChromelessVideoPlayer extends VideoPlayerCore implements IVideoPlayer 
	{
		
		protected var _ns:EasyNetStream;
		protected var _video:Video;
		protected var _videoWidth:Number = 320;
		protected var _videoHeight:Number = 240;
		protected var _startDelay:Number = 0.5;
		
		public function ChromelessVideoPlayer () 
		{
			super();
			
			_type = PlayerType.CHROMELESS;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////
		
		override protected function addChildren():void
		{
			super.addChildren();
			
			_ns = new EasyNetStream;
			_ns.bufferTime = _bufferTime;
			_ns.addEventListener(EasyNetStream.ON_META_DATA, nsHandler, false, 0, true);
			_ns.addEventListener(EasyNetStream.STATUS, nsHandler, false, 0, true);
			_ns.addEventListener(EasyNetStream.ON_CUE_POINT, nsHandler, false, 0, true);
			
			createVideo();
		}
		
		//---------------------------------------
		// PUBLIC CHAIN METHODS
		//---------------------------------------
		public function onCuePoint(listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			addListener(PlayerEvent.ON_CUE_POINT, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function onMetaData(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(PlayerEvent.ON_META_DATA, listener, sendEventToListener, args, once);
			return this;
		}
		
		public function setStartDelay(value:Number):*
		{
			_startDelay = value;
			return this;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		override public function draw ():void
		{
			setProporcionalSize(_video, __width, __height, _videoWidth, _videoHeight, _scaleMode == ScaleMode.PROPORTIONAL_INSIDE);
			DisplayObjectUtils.align(_video, this, "center");
			
			super.draw();
		}
		
		override public function play (videoSource:String = null):*
		{
			// validates resume
			if (_wasPlayed && !videoSource) 
			{
				resume();
				return;
			}

			if (videoSource) 
			{
				_source = videoSource;
			}
			
			if (_source) 
			{					
				_wasPlayed = true;
				_ns.play(_source);
				// Init paused
				_ns.pause();
				
				changeState(VideoState.BEGINNING_PLAY);
				changeState(VideoState.BUFFERING);
			} else {
				trace(this + " Warning: not video URL found");
			}
			
			return this;
		}
		
		override public function stop ():void
		{
			reset(false, false);
			changeState(VideoState.STOPPED);
		}
		
		override public function pause ():void
		{
			_paused = true;
			_ns.pause();
			changeState(VideoState.PAUSED);
		}
		
		override public function togglePause ():void
		{
			_ns.togglePause();
			changeState(_state == VideoState.PAUSED ? VideoState.PLAYING : VideoState.PAUSED);
		}
		
		override public function resume ():void
		{
			_paused = false;
			_ns.resume();
			changeState(VideoState.PLAYING);
		}
		
		override public function seekTo (seconds:Number):void
		{
			_ns.seek(seconds);
		}
		
		override public function reset (fireVideoFinishEvent:Boolean = false, fireVideoResetEvent:Boolean = true):void
		{
			try {
				if (!fireVideoFinishEvent) deleteVideo();
				_ns.close();
			} catch (e) {}
			
			_wasPlayed = false;
			
			if (fireVideoFinishEvent) {
				//dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_VIDEO_FINISH, {target:this}, _bubbles));
			} else {
				createVideo();
			}
			//if (fireVideoResetEvent) dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_RESET, {target:this}, _bubbles));
			
			changeState(VideoState.FINISHED);
		}
		
		override public function destroy ():void
		{
			if (_ns)
			{
				_ns.removeEventListener(EasyNetStream.ON_META_DATA, nsHandler);
				_ns.removeEventListener(EasyNetStream.STATUS, nsHandler);
				_ns.removeEventListener(EasyNetStream.ON_CUE_POINT, nsHandler);

				_wasPlayed = false;

				try {
					deleteVideo();
					_ns.close();
				} catch (e) {
					//trace(e);
				}
			}
			
			_ns = null;
			
			super.destroy();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		protected function nsHandler (e:*):void 
		{
			if (e.type == "status") 
			{
				switch (_ns.status) {
					case "full" :
						//changeState(VideoState.PLAYING);
						changeState(VideoState.BUFFER_FULL);
						if (!_paused) changeState(VideoState.PLAYING);
						break;
						
					case "empty" :
						changeState(VideoState.BUFFERING);
						break;
						
					case "flush" :
						//
						break;
						
					case "start" :
						// Fade first fram
						delayedCall(_startDelay, function () {
							// Fade in transition
							tweenTo({target:_video, alpha:1, delay:0.2, ease:Linear.easeNone}, 0.5);
							// Resume after pause
							_ns.resume();
						});
						
						break;
						
					case "stop" :
						onNsComplete();
						break;
						
					case "streamnotfound":
						trace(this + " Warning: the video stream '" + _source + "' wasn't found");
						dispatchEvent(new PlayerEvent(PlayerEvent.STREAM_ERROR, {target:this, message:"the video stream '" + _source + "' wasn't found"}, _bubbles));
						
						break;
					default :
						// Nothing
					break;
				}
			}
			else if (e.type == EasyNetStream.ON_CUE_POINT) 
			{
				dispatchEvent(new PlayerEvent(PlayerEvent.ON_CUE_POINT, _ns.cuePoint, _bubbles));
			}
			else 
			{
				//---------------------------------------
				// On Metadata Case
				//---------------------------------------
				if (_looping) return;
				
				_looping = false;
				
				_duration = _ns.metaData.duration;
				_videoWidth = _ns.metaData.width;
				_videoHeight = _ns.metaData.height;
				
				_video.width = _videoWidth;
				_video.height = _videoHeight;
				
				draw();
				
				dispatchEvent(new PlayerEvent(PlayerEvent.ON_META_DATA, _ns.metaData, _bubbles));
			}
	
		}
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		protected function onNsComplete ():void
		{
			if (_loop) 
			{
				_looping = true;
				//changeState(VideoState.LOOPING);
				_ns.seek(0);
				//dispatchEvent(new VPlayerEvent(VPlayerEvent.VPLAYER_LOOP, {target:this}, _bubbles));
				
			} else {
				reset(true);
			}
		}
		
		protected function createVideo ():void
		{
			if (_video) deleteVideo();
			
			_video = new Video();
			_video.name = "VideoObject";
			_video.attachNetStream(_ns);
			_video.smoothing = true;
			_video.alpha = 0;
			addChild(_video);
		}
		
		protected function deleteVideo ():void
		{
			if (_video && _video.parent) removeChild(_video);
			_video = null;
		}
		
		protected function setProporcionalSize (target, w:int, h:int, ow:Number, oh:Number, fit:Boolean = false):void 
		{
			if (fit) {
				if (ow > oh) {
					target.width = w;
					target.height = (w / ow) * oh;
					if (target.height > h) {
						target.height = h;
						target.width = (h / oh) * ow;
					}
				} else {
					target.height = h;
					target.width = (h / oh) * ow;
					if (target.width > w) {
						target.width = w;
						target.height = (w / ow) * oh;
					}
				}
			} else {
				var endWidh:Number = (h / oh) * ow;
				var endHeight:Number = (w / ow) * oh;
				
				if (h > endHeight) {
					target.height = h;
					target.width = endWidh;
				} else {
					target.width = w;
					target.height = endHeight;
				}
			}
		}
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		
		override public function set volume (value:Number):void
		{
			super.volume = value;
			_ns.soundTransform = new SoundTransform(value);
		}
		override public function get currentTime ():Number
		{
			return _ns.time;
		}
		override public function get bytesLoaded ():uint
		{
			return _ns.bytesLoaded;
		}
		override public function get bytesTotal ():uint
		{
			return _ns.bytesTotal;
		}
		
		public function get ns():EasyNetStream
		{
			return _ns;
		}
	}
}