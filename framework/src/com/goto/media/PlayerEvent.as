/*
PlayerEvent.as
vPlayer

Created by Silverfenix on 24/09/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
*/
package com.goto.media
{

    import flash.events.Event;
   
    public class PlayerEvent extends Event
    {
		public var params:Object;
		
		public static const STATUS_CHANGE:String = "statusChange";
		public static const STREAM_ERROR:String = "streamError";
		public static const ON_CUE_POINT:String = "cuepoint";
		public static const ON_META_DATA:String = "onMetaData";
		
   
        public function PlayerEvent(_type:String, _params:Object = null, _bubbles:Boolean = false, _cancelable:Boolean = false)
        {
            super(_type, _bubbles, _cancelable);
            params = _params;
        }
		
        public override function clone():Event
        {
            return new PlayerEvent(type, params, bubbles, cancelable);
        }
       
        public override function toString():String
        {
            return formatToString("PlayerEvent", "params", "type", "bubbles", "cancelable");
        }
    }
}