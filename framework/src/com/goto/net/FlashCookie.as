//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.net
{
	import flash.net.SharedObject;
	import flash.net.SharedObjectFlushStatus;
	import flash.events.NetStatusEvent;
	
	/**
	 * A SharedObject Util
	 * Usage
	 * <code>
	 * import com.goto.net.Cookie;
	 * 
	 * var c:Cookie = new Cookie("mycookie");
	 * 
	 * c.valueForKey("mykey", "myvalue");
	 * c.valueForKey("user", "fenixkim");
	 * c.valueForKey("age", 20);
	 * 
	 * trace(c.valueOfKey("user")); // fenixkim
	 * 
	 * c.clear(); // Clear All
	 * </code>
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  13.10.2011
	 */
	public class FlashCookie
	{
			
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
				
		//---------------------------------------
		// CONSTRUCTOR
		//---------------------------------------
	
		/**
		 * @constructor
		 */
		public function FlashCookie(name:String, size:uint = 10000)
		{
			_name = name;
			_size = size;
			_so = SharedObject.getLocal(name);
		}
	
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _name:String;
		protected var _so:SharedObject;
		protected var _size:uint;
		
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			if (value !== _name)
			{
				_name = value;
			}
		}
		
		/**
		 * @private
		 */
		public function get data():Object
		{
			return _so.data;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Send a value to the current Cookie instance
		 * @param key String Clave donde se almacena el valor
		 * @param value * Valor de la clave, puede ser cualquier objeto
		 * @param flushImmediately Boolean determina si se deben guardar los datos inmediatamente, de lo contrario FlashPlayer espera que la aplicacion sea cerrada para guardar los datos.
		 * @return Cookie Instancia de Cookie donde se ha almacenado el valor
		 */
		public function valueForKey(key:String, value:*, flushImmediately:Boolean = true):FlashCookie
		{
			_so.data[key] = value;
			
			if (!flushImmediately) return this;
			
			var status:String;
			
			try
			{
				status = _so.flush(_size);
			} 
			catch (e:Error)
			{
				trace("Flash needs permissions to save local data.");
			}
			
			if (status && status == SharedObjectFlushStatus.PENDING)
			{
				// Requesting permission to user to save local data. 
				_so.addEventListener(NetStatusEvent.NET_STATUS, shareObjectEventHandler);
			}
			
			return this;
		}
		
		/**
		 * Recupera un valor almacenado en la Cookie
		 * @return  * Valor almacenado
		 */
		public function valueOfKey(key:String):*
		{
			return _so.data[key];
		}
		
		/**
		 * Borra una clave almacenada
		 * @param key Nombre de la clave a borrar
		 * @return Boolean Retorna verdadero si se borro satisfactoriamente la clave
		 */
		public function clearKey(key:String):Boolean
		{
			if (_so.data[key]) 
			{
				delete _so.data[key];
				return true;
			}
			
			return false;
		}
		
		/**
		 * Borra todos los datos de la Cookie
		 */
		public function clear():void
		{
			_so.clear();
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function shareObjectEventHandler(event:NetStatusEvent):void
		{
			if (event.info.code == "SharedObject.Flush.Success")
			{
				trace("Granted Permission");
			}
			
			_so.removeEventListener(NetStatusEvent.NET_STATUS, shareObjectEventHandler);
		}
		
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * Crea una nueva instancia de Cookie con un nombre y un tamaño
		 * Usage:
		 * <code>
		 * import com.goto.net.Cookie;
		 * var c:Cookie = Cookie.cookieWithSize("mycookie", 50000);
		 * </code>
		 * @param name String Nombre de la cookie
		 * @return Cookie
		 */
		public static function cookieWithSize(name:String, size:uint):FlashCookie
		{
			return new FlashCookie(name, size);
		}
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
	
	}

}