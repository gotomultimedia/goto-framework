//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.net
{
	import flash.events.EventDispatcher;
	
	public class GTURL extends Object
	{	

		//--------------------------------------
		// Private & Protected properties
		//--------------------------------------
		protected var __originalURLString:Object;
		protected var __URL:Object;
		
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
	
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		public function GTURL(aURLString:String)
		{
			create(aURLString);
		}
	
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
	
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		public function get absolutString():String { return __URL.absolutString; }
		public function get baseURL():String { return __URL.baseURL; }
		public function get scheme():String { return __URL.scheme; }
		public function get user():String { return __URL.user; }
		public function get password():String { return __URL.password; }
		public function get host():String {	return __URL.host; }
		public function get port():String { return __URL.port; }
		public function get path():String { return __URL.path; }
		public function get filePath():String { return __URL.filePath; }
		public function get file():String { return __URL.file; }
		public function get fileExtension():String { return __URL.fileExtension; }
		public function get params():String { return __URL.params; }
		public function get query():String { return __URL.query; }
		public function get fragment():String { return __URL.fragment; }
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		public function toString():String
        {
			if (!__URL) return "No URL Match";
			
			var result:String = "";
			result += "[object GTURL: \n";
			//result += "\tstructure: <scheme>://<user>:<password>@<host>:<port>/<path>;<params>?<query>#<fragment>\n";
			result += "\tabsolutString: "	+ absolutString + "\n";
			result += "\tbaseURL: " 		+ baseURL + "\n";
			result += "\tscheme: " 			+ scheme + "\n";
			result += "\tuser: " 			+ user + "\n";
			result += "\tpassword: " 		+ password + "\n";
			result += "\thost: " 			+ host + "\n";
			result += "\tport: " 			+ port + "\n";
			result += "\tpath: " 			+ path + "\n";
			result += "\tfilePath: "		+ filePath + "\n";
			result += "\tfile: " 			+ file + "\n";
			result += "\tfileExtension: "	+ fileExtension + "\n";
			result += "\tparams: "			+ params + "\n";
			result += "\tquery: "			+ query + "\n";
			result += "\tfragment: "		+ fragment + "\n";
			result += "]";
			
            return String(result);
        }
		
		//--------------------------------------
		//  PUBLIC STATIC METHODS
		//--------------------------------------
		public static function createURLWithString (aURLString:String):GTURL
		{
			return new GTURL(aURLString);
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		protected function create(aURLString:String):void
		{
			var URLPattern:RegExp = /(?:(\w+):\/\/)?(?:(\w+)(?::(\w+))?@)?([^\/;\?:#]+)(?::(\d+))?(?:\/?([^;\?#]+))?(?:;([^\?#]+))?(?:\?([^#]+))?(?:#(\w+))?/;
			var u:Array = aURLString.match(URLPattern);

			if (!u) return;
			
			__originalURLString = aURLString; // Save the original url string
			
			__URL = {};
			
			/*
			ToDo
			– pathComponents
			– pathExtension
			– resourceSpecifier
			– standardizedURL
			*/
			__URL.absolutString = new String(u[0]);
			__URL.scheme = new String(u[1] || "");
			__URL.user = new String(u[2] || "");
			__URL.password = new String(u[3] || "");
			__URL.host = new String(u[4] || "");
			__URL.port = new String(u[5] || "");
			__URL.path = getPath();
			__URL.filePath = getFilePath();
			__URL.file = getFile();
			__URL.fileExtension = getFileExtension();
			__URL.params = new String(u[7] || "");
			__URL.query = new String(u[8] || "");
			__URL.fragment = new String(u[9] || "");
			__URL.baseURL = getBaseURL();
			
			// Helpers
			function getBaseURL ():String 
			{
				var s:String = __URL.scheme;
				return s + (s.length ? "://" : "") + host;
			};
			function getPath ():String 
			{
				// buscar una forma mejor de validar cuando una URL no tiene host ni scheme osea URLs locales o relativas
				// actualmente se valida: si el host no tiene "." entonces es porque es una carpeta relativa 
				var h:String = __URL.host;			
				return !h.match(/\./) ? h +"/"+ u[6] : (u[6] || "");
			}
			function getFilePath ():String
			{ 
				var p:String = __URL.path,
					m:Array = p.match(/.*\//);		
				return m ? m[0] : "";
			}
			function getFile ():String
			{ 
				var result:String = "";
				
				// Extrae el nombre del archivo del directorio
				if (__URL.path.length)
				{
					var p:String = __URL.path.length ? __URL.path : null,
						m:Array = p ? p.match(/[^\/]+$/) : null;
					result = m ? m[0] : "";
				}
				// Valida si host es el nombre de un archivo: ej: file.jpg y no un directorio
				else if (!__URL.host.match(/\//) && __URL.host.match(/./)) 
				{
					result = __URL.host;
				}
				
				return result;
			}
			function getFileExtension ():String
			{ 
				var f:String = __URL.file;
				f = f.length ? f : null;
				return f ? f.match(/(?<=\.)([^\.]+)$/)[0] : "";
			}
			
			// Release
			u = null;
		}
	}
}