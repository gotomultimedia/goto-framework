﻿package com.goto.utils {
	
	import flash.geom.ColorTransform;
	import flash.filters.ColorMatrixFilter;
	import flash.display.DisplayObject;
	
	public class ColorUtils {
		
		public static function tint(target:Object, color:Number):void {
			/*var colorTransform:ColorTransform = new ColorTransform();
			colorTransform.color = color;
			var alpha:Number = target.alpha;
			target.transform.colorTransform = colorTransform;
			target.alpha = alpha;
			*/
			target.transform.colorTransform = colorTransform(color, 1.0, target.alpha);
		}
		
		public static function exposure (target:DisplayObject, exposure:Number):void {
			var colorTransform:ColorTransform = new ColorTransform;
			colorTransform.redOffset = exposure; 
			colorTransform.greenOffset = exposure; 
			colorTransform.blueOffset = exposure;
			
			target.transform.colorTransform = colorTransform;
		}
		
		public static function hexStringToNumber(hexStr:String):Number{
			if(hexStr.length != 7){
				return -1;
			}
			if(hexStr.charAt(0) != "#"){
				return -1;
			}
			var newStr:String = hexStr.substr(1,6);
			var numStr:String = "0x"+newStr;
			var num:Number = Number(numStr);
			return num;
		}
		
		public static function numberToHexString(number:Number):String
		{
			var hexStr:String = number.toString(16);
			
			while(hexStr.length < 6){
				hexStr = "0"+hexStr;
			}
			
			hexStr = "#"+hexStr;
			
			return hexStr;
		}
		
		public static function desaturation(target:DisplayObject):void{
			var myFilter:ColorMatrixFilter=new ColorMatrixFilter([
																   .5, .5, .5, 0, 0,
																   .5, .5, .5, 0, 0,
																   .5, .5, .5, 0, 0,
																   0, 0, 0, 1, 0
																   ]);
			target.filters=[myFilter];
		}
		
		/**
		 * RGBColorTransform Create an instance of the information.
		 * Full credit for this class goes to Deva Raj (@nsdevaraj). Many thanks.
		 * 
		 * @param rgb RGB integer value that indicates (0x000000 - 0xFFFFFF)
		 * @param amount amount of fill adaptive value (0.0 - 1.0)
		 * @param alpha transparency (0.0 - 1.0)
		 * @return a new instance ColorTransform
		 */
		public static function colorTransform (rgb:uint = 0, amount:Number = 1.0, alpha:Number = 1.0):ColorTransform
		{
			amount = (amount> 1)? 1: (amount <0)? 0: amount;
			alpha = (alpha> 1)? 1: (alpha <0)? 0: alpha;
			var r: Number = ((rgb>> 16) & 0xff) * amount;
			var g: Number = ((rgb>> 8) & 0xff) * amount;
			var b: Number = (rgb & 0xff) * amount;
			var a: Number = 1-amount;
			return new ColorTransform (a, a, a, alpha, r, g, b, 0);
		}
	}
}