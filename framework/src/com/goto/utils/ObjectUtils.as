﻿package com.goto.utils {
	
	import flash.geom.ColorTransform;
	import flash.utils.ByteArray;
	
	public class ObjectUtils {
		
		public static function forIn(target:Object):void {
			trace("--------------------------------------------");
			trace("ForIn: "+target+"\n");
			for (var i:String in target) {
				trace("propiedad: "+i+" valor: "+target[i]);
			}
			trace("\n");
		}
		
		public static function traceObject(obj:Object, indent:uint = 0):void { 
			var indentString:String = ""; 
			var i:uint; 
			var prop:String; 
			var val:*; 
			for (i = 0; i < indent; i++) 
			{ 
				indentString += "\t"; 
			} 
			for (prop in obj) 
			{ 
				val = obj[prop]; 
				if (typeof(val) == "object") 
				{ 
					trace(indentString + " " + prop + ": [Object]"); 
					traceObject(val, indent + 1); 
				} 
				else 
				{ 
					trace(indentString + " " + prop + ": " + val); 
				} 
			} 
		}
		
		public static function traceObjectAsString (obj:Object, indent:uint = 0):String { 
			var result:String = "";
			
			var indentString:String = ""; 
			var i:uint; 
			var prop:String; 
			var val:*; 
			for (i = 0; i < indent; i++) 
			{ 
				indentString += "\t"; 
			} 
			for (prop in obj) 
			{ 
				val = obj[prop]; 
				if (typeof(val) == "object") 
				{ 
					result += indentString + " " + prop + ": [Object]"+"\n"; 
					result += traceObjectAsString(val, indent + 1) + "\n";
				} 
				else 
				{ 
					result += indentString + " " + prop + ": " + val + "\n"; 
				} 
			}
			
			return result;
		}		
		
		public static function setPropertiesFrom (target:*, object:Object, filter:Array = null, showError:Boolean = true):void
		{
			var propertie:String;
			
			if (filter) {
				for (var i:uint; i < filter.length; i++) {
					propertie = filter[i];
					try {
						target[propertie] = object[propertie];
					} catch (e:Error){
						if (showError) {
							trace(e.getStackTrace());
						}
					}
				}
			} else {
				for (propertie in object) {
					try {
						target[propertie] = object[propertie];
					} catch (e:Error){
						if (showError) {
							trace(e.getStackTrace());
						}
					}
				}
			}
		}
		
		public static function setProperties (target:*, object:Object, propertyList:Array = null, excludeList:Array = null, forceSet:Boolean = false, showError:Boolean = false):void
		{
			var prop:String;
			var propIsExclude:Boolean;
			if (propertyList) {
				for (var i in propertyList) {
					prop = propertyList[i];
					propIsExclude = excludeList && (excludeList.indexOf(prop) > -1);
					if (!propIsExclude)
						if (target.hasOwnProperty(prop) || forceSet) 
						{
							target[prop] = object[prop];
						}
						else if (showError) {
							trace("[ObjectUtils] Warning: Property '"+ prop +"' is missing in "+ target);
						}
				}
			} else {
				for (prop in object) {
					propIsExclude = excludeList && (excludeList.indexOf(prop) > -1);
					if (!propIsExclude)
						if (target.hasOwnProperty(prop) || forceSet) 
						{
							target[prop] = object[prop];
						}
						else if (showError) {
							trace("[ObjectUtils] Warning: Property '"+ prop +"' is missing in "+ target);
						}
				}
			}
		}
		
		/**
		 * Extends or replaces the properties of the object received in the parameter <code>target</code> with the properties of the object received in the parameter  <code>propertiesForTarget</code>
		 * If target is <code>undefined</code> or <code>null</code> then is returned the same object received in the second parameter.
		 * @param target The target object
		 * @param propertiesForTarget The properties for the object received in the parameter <code>target</code>
		 * @param ignore An array of strings that contains a list of properties wich must be excluded or ignored from <code>target</code>
		 * @return The extended object
		 */
		public static function extend(target:Object, propertiesForTarget:Object, ignore:Array = null):Object
		{
			if (!target) return propertiesForTarget;
			
			setProperties(target, propertiesForTarget, null, ignore, true);
			
			return target;
		}
		
		/**
		 * @private
		 */
		public static function propsStartingIn(startString:String, target:Object, strip:Boolean = true, ignore:Array = null):Object
		{
			var result:Object;
			var pName:String;
			
			for (var p:String in target)
			{
				if (ignore && ignore.indexOf(p) != -1) continue;
				
				if (!result) result = {};
				
				pName = p;
				
				if (p.indexOf(startString) == 0)
				{
					if (strip)
					{
						// strip the string
						pName = p.replace(startString, "");
						// to lower case the first char
						pName = pName.replace(/./, function () {
							return arguments[0].toLowerCase();
						});
					}
					
					result[pName] = target[p];
				}
			}
			
			return result;
		}
		
		public static function getProperties (target:Object, propertyList:Array = null, object:Object = null):Object
		{
			if (!object) object = {};
			
			var prop:String;
			
			if (propertyList) 
			{
				for each (prop in propertyList) 
				{
					if (target.hasOwnProperty(prop)) 
					{
						object[prop] = target[prop];
					}
				}
			} else {
				for (prop in target) 
				{
					object[prop] = target[prop];
				}
			}
			
			return object;
		}
		
		public static function compare(obj1:Object, obj2:Object):Boolean
		{
			var buffer1:ByteArray = new ByteArray();
			buffer1.writeObject(obj1);
			
			var buffer2:ByteArray = new ByteArray();
			buffer2.writeObject(obj2);
			
			// compare the lengths
			var size:uint = buffer1.length;
			
			if (buffer1.length == buffer2.length) {
				buffer1.position = 0;
				buffer2.position = 0;
				
				// then the bits
				while (buffer1.position < size) {
					
					var v1:int = buffer1.readByte();
					if (v1 != buffer2.readByte()) {
						return false;
					}
				}    
				return true;                        
			}
			return false;
		}
		
		public static function clone(object:Object):* {
			var byteArray:ByteArray = new ByteArray();
			byteArray.writeObject(object);
			byteArray.position = 0;
			return byteArray.readObject();
		}
		
	}
}