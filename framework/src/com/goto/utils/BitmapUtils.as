//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2012 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.utils
{	
	import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.Shader;
    import flash.display.ShaderJob;
    import flash.geom.Matrix;

	/**
	 * Class.
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Alexander Ruiz Ponce
	 * @since  01.08.2012
	 */
	public class BitmapUtils extends Object
	{
	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		/**
		 * @constructor
		 */
		public function BitmapUtils()
		{
			super();
		}
	
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Resamples a bitmapData object
		 * <p>This method is based on http://www.brooksandrus.com/blog/2009/03/11/bilinear-resampling-with-flash-player-and-pixel-bender/</p>
		 * 
		 * @param input The Original BitmapData
		 * @param desiredWidth uint
		 * @param desiredHeight uint
		 * @param cleanup Boolean
		 * @return BitmapData 
		 */
		public static function resampleBitmap(input:BitmapData, desiredWidth:uint, desiredHeight:uint, cleanup:Boolean = true):BitmapData
        {
			var aspectRatio:Number = input.width / input.height;
			var outputWidth:int;
			var outputHeight:int;
			var scale:Number;
			
            // Determine output bitmap dimensions and Matrix scale
            if (input.width > input.height)
            {
                outputWidth = desiredWidth;
                outputHeight = desiredWidth / aspectRatio;
				scale = desiredWidth / input.width;
            }
            else
            {
                outputWidth = desiredHeight * aspectRatio;
                outputHeight = desiredHeight;
				scale = desiredHeight / input.height;
            }
			
			var matrix:Matrix = new Matrix();
            matrix.scale(scale, scale);
			
			var output:BitmapData = new BitmapData(outputWidth, outputHeight);
            output.draw(input, matrix, null, null, null, true);
			
			if (cleanup)
            {
                input.dispose();
            }

			return output;
        }
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
	
	}

}