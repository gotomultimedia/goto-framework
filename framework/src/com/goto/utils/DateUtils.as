﻿package com.goto.utils {

	public class DateUtils {
		
		public static const SECONDS:String = "seconds";
		public static const MINUTES:String = "minutes";
		public static const HOURS:String = "hours";
		public static const DAYS:String = "days";
		
		public static function daysAgo (date:String):Object
		{
			var result:Number;
			var unit:String;
			var currentDate:Date = new Date();
			var at:Number = Date.parse(date);
			
			var hoursLeft:int = Math.round((currentDate.time - at) / 3600000);
			
			if (hoursLeft > 24) 
			{
				result = Math.floor(hoursLeft * 0.041666667);
				unit = DAYS;
			}
			else if (hoursLeft < 1) 
			{
				var seconds:Number = Math.round((currentDate.time - at) / 1000);
				if (seconds < 60) {
					result = seconds;
					unit = SECONDS;
				} else {
					result = Math.floor(seconds / 60);
					unit = MINUTES;
				}
			}
			else {
				result = hoursLeft;
				unit = HOURS;
			}
			
			return {time:result, unit:unit};
		}
		
		/**
		* Converts millisecond figure into timecode
		* @author http://snipplr.com/view/22412/
		* @param millisecond total number of milliseconds - accurate up to one hour
		*/
		public static function toTimeCode(milliseconds:int) : String {
			var isNegative:Boolean = false;
			if (milliseconds < 0) {
				isNegative = true;
				milliseconds = Math.abs(milliseconds);			
			}

			var seconds:int = Math.round((milliseconds/1000) % 60);
			var strSeconds:String = (seconds < 10) ? ("0" + String(seconds)) : String(seconds);
			if(seconds == 60) strMinutes = "00";
			var minutes:int = Math.round(Math.floor((milliseconds/1000)/60));
			var strMinutes:String = (minutes < 10) ? ("0" + String(minutes)) : String(minutes);

			if(minutes > 60) {
				strSeconds = "60";
				strMinutes = "00";
			}

			var timeCodeAbsolute:String = strMinutes + ":" + strSeconds;
			var timeCode:String = (isNegative) ? "-" + timeCodeAbsolute : timeCodeAbsolute;
			return timeCode;
		}
		
	}
}