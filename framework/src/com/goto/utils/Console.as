/*
Console.as
Labs

Created by Silverfenix on 20/07/10.
Copyright 2010 goTo! Multimedia. All rights reserved.
 
@version 1.0.0
*/
package com.goto.utils
{
	
	import flash.events.EventDispatcher;
	import flash.external.ExternalInterface;
	import flash.utils.getQualifiedClassName;
	
	public class Console extends EventDispatcher 
	{
		
		public static var showExternal:Boolean = true;
		public static var showInternal:Boolean = true;
		public static var showTime:Boolean = false;
		public static var showCount:Boolean = false;
		public static var enabled:Boolean = true;
		private static var __customOutput:* = null;
		private static var __logCount:uint;
		
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		/**
		 * Display a log message in the flash output, customOutput or via ExternalInterfaz
		 */
		public static function log (... args:Array):String
		{
			// Check if enabled
			if (!showExternal && !showInternal || !enabled) return null;
			
			// Create a holder var for messaje
			var message:String = "";
			
			// Defines regExp to match strings as $1 $2 ... $10 and $1[property] for objects...
			var pattern:RegExp = /(\$(\d*)\[(\w*)\]|\$\d*)/g;
			
			// Check whether to apply format to the first string
			var useFormat:Boolean = String(args[0]).match(pattern).length > 0 && args.length > 0;

			if (useFormat) {
				// With format
				message = String(args[0]).replace(pattern, setFormat);
			} else {
				// Without format
				message = getContent();
			}
			
			// Internal function to format a string
			function setFormat (capture:String, ... rest:Array)
			{				
				var index:uint, prop:String;
				index = capture.match(/\$(\d*)/)[1];
				
				try {
					// If is an Object or Array
					prop = capture.match(/\[(\w*)\]/)[1];
					
					// if the object does not have the requested property then returns [undefined]
					try {
						
						if (typeof(args[index]) == "object") {
							return (args[index][prop]).toString();
						}
					} 
					catch (e:Error) {
						return "[undefined]";
					}
					
				} 
				catch (e:Error) {
					// If is String or Number
					return args[index].toString();
				}
			}
			
			// Internal function to get the content
			function getContent ()
			{
				var str:String = "", sep:String = "", result:String, qualifiedClassName:String;
				
				// Runs through all the arguments and if any is an 'Object' or 'Array'
				// displays its own contents recursively
				for (var i:String in args) 
				{
					sep = (uint(i) < args.length - 1) ? ", " : "";
					if (args[i] || args[i] is Boolean) 
					{	
						qualifiedClassName = getQualifiedClassName(args[i]);
						
						if (qualifiedClassName == "Object" || qualifiedClassName == "Array") {
							sep = "";
							result = "\n" + i + ":" + qualifiedClassName + "\n" + traceObject(args[i]);
						} else {
							result = args[i].toString();
						}
					} else {
						result = "null";
					}

					str += result + sep;
				}
				
				return str;
			}
			
			message = show(message);
			
			if (__customOutput) {
				__customOutput.appendText("\n" + message);
			}
			
			return message;
		}
		
		/**
		 * Displays the object contents recursively
		 */
		public static function traceObject (obj:Object, indent:uint = 0):String { 
			var result:String = "";
			
			var indentString:String = "\t"; 
			var i:uint; 
			var prop:String; 
			var val:*; 
			for (i = 0; i < indent; i++) 
			{ 
				indentString += "\t"; 
			} 
			for (prop in obj) 
			{ 
				val = obj[prop];
				if (typeof(val) == "object") 
				{ 
					var qualifiedClassName:String, isArrayOrObject:Boolean, toStringResult:String = "", stringResult:String;
					qualifiedClassName = getQualifiedClassName(val);
					
					var m:Array = qualifiedClassName.split("::");
					if (m[1]) qualifiedClassName = m[1];
					
					isArrayOrObject = qualifiedClassName == "Object" || qualifiedClassName == "Array";
					
					if (!isArrayOrObject) {
						toStringResult = " " + ((val == null) ? "null" : val.toString());
					}
					
					if (qualifiedClassName == "null" || toStringResult == "null") {
						stringResult = " null";
					} else {
						stringResult = " [" + qualifiedClassName + "]"+ toStringResult;
					}
					
					result += indentString + "" + prop + ":" + stringResult + "\n";
					
					if (isArrayOrObject) result += traceObject(val, indent + 1);
				} 
				else 
				{ 
					result += indentString + "" + prop + ": " + val + "\n";
				} 
			}
			
			return result;
		}
				
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		
		/**
		 * Display the final log message
		 */
		private static function show (message:String):String
		{
			var r:String;
			var t:String = (showTime) ? " [" + getCurrentTime() + "]" : "";
			var c:String = (showCount) ? " #" + ++__logCount : "";
			
			r = "Log"+ c + t +": " + message;
			
			if(showExternal && ExternalInterface.available) ExternalInterface.call("log", r);
			if(showInternal) trace(r);
			
			return r;
		}
		
		private static function getCurrentTime ():String
		{
			var currentDate = new Date();
			var mm = String((currentDate.minutes < 10) ? "0"+currentDate.minutes : currentDate.minutes);
			var ss = String((currentDate.seconds < 10) ? "0"+currentDate.seconds : currentDate.seconds);
			var ms = String(currentDate.milliseconds);
			
			return mm+":"+ss+":"+ms;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		
		/**
		 * Specifies a custom output
		 * Must be a TextField or an Object that implements 'appentText' method
		 */
		public static function set customOutput (value:*):void
		{			
			if (value.hasOwnProperty("appendText")) {
				__customOutput = value;
			} else {
				trace("Log: Invalid customOutput, must be a TextField or an Object that implements 'appentText' method");
			}
		}
	}
}