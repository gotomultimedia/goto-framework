/*
SpriteSheet.as
Labs

Created by Silverfenix on 24/07/10.
Copyright 2010 goTo! Multimedia. All rights reserved.

*/
package com.goto.display 
{	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	
	import com.goto.utils.Console;
	
	public class SpriteSheet extends EventDispatcher {
		
		// Privates
		private var __texture:BitmapData;
		private var __coordinates:Object;
		private var __urlLoader:URLLoader;
		private var __loader:Loader;
		private var __ready:Boolean;
		
		// Public vars
		public var variables:Object;
		public var name:String;
		
		// Public static vars
		public static var __spriteSheetList:Object = {};
		public static var __spriteSheetCount:uint;
		
		public static const COORDINATES_LOAD_COMPLETE:String = "coordinatesLoadComplete";
		public static const TEXTURE_LOAD_COMPLETE:String = "textureLoadComplete";
		public static const READY:String = "ready";
		
		public function SpriteSheet (coordinates:Object, texture:Object, vars:Object = null) 
		{
			variables = vars;
			
			// Saving the instance
			if (!variables.name) variables.name = "@spriteSheetID" + __spriteSheetCount++;
			name = variables.name;
			__spriteSheetList[name] = this;
			
			
			
			// Load or set coordinates
			if (coordinates is XML) parseCoordinates(coordinates as XML) else loadCoordinates(coordinates as String);
			
			// Load or set texture
			if (texture is BitmapData) __texture = (texture as BitmapData) else loadTexture(texture as String);
			
			// Check if ready
			if (__coordinates && __texture) {
				__ready = true;
				dispatch(READY);
			}
		}
		
		//////////////////////////////////////////////////////////
		//
		// static functions
		//
		//////////////////////////////////////////////////////////
		
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		public function getSpriteNames ():Array
		{
			if (!__coordinates) return null;
			
			var result:Array;
			
			for (var spriteName:String in __coordinates) {
				if (!result) result = [];
				result.push(spriteName);
			}
			
			return result;
		}
		
		public function getSprite (name:String, trim:Boolean = false, transparent:Boolean = true, color:uint = 0xffffff):Bitmap
		{
			if (!__ready) {
				trace("SpriteSheet is not ready");
				return null;
			}
			
			if (!(name in __coordinates)) {
				trace("Sprite '"+name+"' not found");
				return null;
			} else {
				//return new Bitmap(getSpriteBitmapData(name, trim, transparent, color));
				return new Bitmap(getSpriteBitmapData.apply(null, arguments));
			}
		}
		
		public function getMultiple (spriteNames:Array, trim:Boolean = false, transparent:Boolean = true, color:uint = 0xffffff):Array
		{
			var result:Array;
			
			for (var i:String in spriteNames) 
			{
				var bitmap:Bitmap = getSprite(spriteNames[i]);
				if (bitmap) {
					if (!result) result = [];
					result.push(bitmap);
				}
			}
			
			return result;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Static Funtions
		//
		//////////////////////////////////////////////////////////
		public static function create(name:String, coordinates:Object, texture:Object = null, vars:Object = null):SpriteSheet 
		{
			// name validation
			if (!name) throw new Error("Could not create an instance of SpriteSheet because the argument 'name' is not valid");
			if (name in __spriteSheetList) throw new Error("There is already a SpriteSheet instance named '" + name +"'");
			
			vars.name = name;
			return new SpriteSheet(coordinates, texture, vars);
		}
		
		public static function get(name:String):SpriteSheet 
		{
			if (!(name in __spriteSheetList)) {
				trace("There is not a SpriteSheet instance named '" + name +"'");
				return null;
			}
			
			return __spriteSheetList[name];
		}
		
		public static function getSpriteFrom(sheetName:String, spriteName:String, trim:Boolean = false, transparent:Boolean = true, color:uint = 0xffffff):Bitmap 
		{
			return __spriteSheetList[sheetName].getSprite(spriteName, trim, transparent, color);
		}
		
		//////////////////////////////////////////////////////////
		//
		// Private Methods
		//
		//////////////////////////////////////////////////////////
		
		private function loadCoordinates (file:String):void
		{
			__urlLoader = new URLLoader(new URLRequest(file));
			__urlLoader.addEventListener(Event.COMPLETE, onCoordinateFileLoadComplete, false, 0, true);
		}
		
		private function parseCoordinates (plist:XML):void
		{
			var framesPath:XML = plist.dict.dict[0];
			var count:uint;
			
			for each (var frame:XML in framesPath.children()) 
			{
				if (!__coordinates) __coordinates = {};
				
				if (frame.name() == "key") {
					
					var obj:Object, m:Array, dict:XML, rectPatter:RegExp, pointPatter:RegExp;
					
					rectPatter = /\{\{(\d*|-\d*), (\d*|-\d*)\}, \{(\d*|-\d*), (\d*|-\d*)\}\}/;
					pointPatter = /\{(\d*|-\d*), (\d*|-\d*)\}/;
					
					obj = {};			
					dict = framesPath.children()[count + 1];
					
					// Frame
					m = dict.children()[1].text().toString().match(rectPatter);
					obj.frame = new Rectangle(m[1], m[2], m[3], m[4]);
										
					// Offset
					m = dict.children()[3].text().toString().match(pointPatter);
					obj.offset = new Point(m[1], m[2]);
					
					// Rotate
					obj.rotate = dict.children()[5].name() == "true";
					
					// Source color rectangle
					m = dict.children()[7].text().toString().match(rectPatter);
					obj.sourceColorRect = new Rectangle(m[1], m[2], m[3], m[4]);
					
					// Source size
					m = dict.children()[9].text().toString().match(pointPatter);
					obj.sourceSize = new Point(m[1], m[2]);
					
					m = null;
					
					var name:String = obj.name = frame.text().toString().replace(/\.([\w]*)/gi, "");
					__coordinates[name] = obj;
				}
				
				count++;
			}
		}
		
		private function loadTexture (file:String):void
		{
			__loader = new Loader;
			__loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onTextureFileLoadComplete, false, 0, true);
			__loader.load(new URLRequest(file));
		}
		
		private function getSpriteBitmapData (name:String, trim:Boolean = false, transparent:Boolean = true, color:uint = 0xffffff):BitmapData
		{
			var co:Object, bd:BitmapData, w:uint, h:uint, point:Point, rect:Rectangle;
			
			co = __coordinates[name];
			
			if (trim) {
				w = co.sourceColorRect.width;
				h = co.sourceColorRect.height;
				rect = new Rectangle(co.frame.x + co.sourceColorRect.x, co.frame.y + co.sourceColorRect.y, co.sourceColorRect.width, co.sourceColorRect.height);
				point = new Point(co.frame.x, co.frame.y);
			} else {
				w = co.sourceSize.x;
				h = co.sourceSize.y;
				rect = new Rectangle(co.frame.x, co.frame.y, co.sourceSize.x, co.sourceSize.y);
				point = new Point(0, 0);
			}
			
			bd = new BitmapData(w, h, transparent, color);
			bd.copyPixels(__texture, rect, point);
			
			return bd;
		}
		
		private function dispatch (func:String):void
		{	
			// execute the callback
			var callBackName:String = toCamelCase("on " + func);
			var e:Event = new Event(func);
			
			if (variables[callBackName]) {
				variables[callBackName].apply(null, variables[toCamelCase(callBackName + " params")]);
				/*
				try {
					variables[callBackName].apply(null, variables[toCamelCase(callBackName + " params")]);
				} catch (error:Error) {
					variables[callBackName].apply(null, [e]);
				}*/
			}
			
			// dispatch the event
			dispatchEvent(e);
		}
		
		private function toCamelCase (str:String):String 
		{
			return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
							   if (+match === 0) return "";
							   return index == 0 ? match.toLowerCase() : match.toUpperCase();
							   });
		}
		
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		
		private function onCoordinateFileLoadComplete (e:Event):void
		{
			__urlLoader.removeEventListener(Event.COMPLETE, onCoordinateFileLoadComplete);
			__urlLoader = null;
			
			parseCoordinates(new XML(e.currentTarget.data));
			
			dispatch(COORDINATES_LOAD_COMPLETE);
			
			if (__texture) {
				__ready = true;
				dispatchEvent(new Event(READY));
			}
		}
		
		private function onTextureFileLoadComplete (e:Event):void
		{
			__loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onTextureFileLoadComplete);
			__loader = null;
			
			__texture = e.currentTarget.content.bitmapData;
			
			dispatch(TEXTURE_LOAD_COMPLETE);
			
			if (__coordinates) {
				__ready = true;
				dispatch(READY);
			}
		}
		
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		
		public function get texture ():BitmapData
		{
			return __texture;
		}
		
		public function get coordinates ():Object
		{
			return __coordinates;
		}
	}
}
