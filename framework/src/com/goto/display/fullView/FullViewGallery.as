/*
FullViewGallery.as

Created by Silverfenix on 05.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.display.fullView
{
	import com.goto.display.components.GenericComponent;
	import com.goto.display.ImageView;
	import com.goto.controls.PiePreloader;
	import com.goto.utils.DisplayObjectUtils;
	import com.goto.utils.ObjectUtils;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import com.greensock.TweenLite;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.display.DisplayObject;
	import flash.display.LoaderInfo;
	import fl.motion.easing.Exponential;
	
	//import com.goto.utils.Console;

	public class FullViewGallery extends FullView 
	{
		// Privtae & Protected Instance Vatiables
		protected var __sourceList:Array;
		protected var __nextButton:DisplayObject;
		protected var __previousButton:DisplayObject;
		protected var __currentIndex:uint;
		protected var __imageLoader:ImageView;
		protected var __defaultSettings:Object;
		protected var __preloader:DisplayObject;
		
		/**
	 	* @constructor
	 	*/
		public function FullViewGallery(sett:Object = null)
		{
			//settings = new Object;
			__defaultSettings = {
				easeForSwap: Exponential.easeInOut,
				navigationButtonController: FullViewNavigationButton,
				navigationButtonView: null,
				navigationButtonXMargin: 0,
				preloaderView: PiePreloader,
				preloaderColor: 0xffffff,
				preloaderAlign: "center",
				preloaderXMargin: 0,
				preloaderYMargin: 0,
				paginationView: null
			}
			
			super(sett);
		}

		//////////////////////////////////////////////////////////
		// Generic Component implemented methods
		//////////////////////////////////////////////////////////
		
		override protected function init():void 
		{
			for (var prop in __defaultSettings) {
				if (!(prop in settings)) settings[prop] = __defaultSettings[prop];
			}
			
			super.init();
		}
		
		/*
		override protected function onComponentReady():void {
			// sentences
		}
		*/
		
		override protected function addChildren():void
		{
			super.addChildren();
			
			__imageLoader = new ImageView;
			__imageLoader.addEventListener(ImageView.LOAD_COMPLETE, imageLoadComplete, false, 0, true);
			__imageLoader.addEventListener(ImageView.LOAD_PROGRESS, imageLoadProgress, false, 0, true);
			
			//__nextButton = new FullViewNavigationButton("right", s.navigationButtonAsset);
			__nextButton = new s.navigationButtonController("right", s.navigationButtonView);
			__nextButton.addEventListener(MouseEvent.CLICK, navigationButtonHandler, false, 0, true);
			
			//__previousButton = new FullViewDefaultNavigationButtonAssets;
			__previousButton = new s.navigationButtonController("left", s.navigationButtonView);
			__previousButton.addEventListener(MouseEvent.CLICK, navigationButtonHandler, false, 0, true);
			
			__preloader = new s.preloaderView;
			__preloader.visible = false;
			__preloader.alpha = 0;
			callSetter(__preloader, "color", s.preloaderColor);

			addChild(__preloader);
			
			DisplayObjectUtils.addChildren(this, __nextButton, __previousButton);
		}
		
		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////

		override public function draw ():void
		{
			super.draw();
			
			DisplayObjectUtils.align(__nextButton, this, "right", s.navigationButtonXMargin);
			DisplayObjectUtils.align(__previousButton, this, "left", s.navigationButtonXMargin);
			DisplayObjectUtils.align(__preloader, this, s.preloaderAlign, s.preloaderXMargin, s.preloaderYMargin);
		}

		public function openWithSourceList(imageViewReference:ImageView, sourceList:Array, indexInSourceList:uint):void
		{
			__sourceList = sourceList;
			__currentIndex = indexInSourceList;
			openWithImageView(imageViewReference);
		}
		
		public function next():void
		{
			if (__currentIndex < __sourceList.length-1)
			{
				__currentIndex++;
				
				dispatchEvent(new FullViewEvent(FullViewEvent.NEXT, {target:this, index:__currentIndex}));
				
				load();
			}	
		}
		
		public function previous():void
		{
			if (__currentIndex > 0)
			{
				__currentIndex--;
				
				dispatchEvent(new FullViewEvent(FullViewEvent.PREVIOUS, {target:this, index:__currentIndex}));
				
				load();
			}
		}
		
		public function syncReference(newImageView:ImageView):void
		{
			s.currentImageViewReference = newImageView;
		}
		
		override public function close():void
		{
			callFunction(__nextButton, "hide");
			callFunction(__previousButton, "hide");
			super.close();
		}
		
		override public function softDestroy():void
		{
			callFunction(__previousButton, "hide");
			callFunction(__nextButton, "hide");
			
			super.softDestroy();
		}
		
		override public function destroy():void
		{
			callFunction(__preloader, "destroy");
			
			__imageLoader.removeEventListener(ImageView.LOAD_COMPLETE, imageLoadComplete);
			__imageLoader.removeEventListener(ImageView.LOAD_PROGRESS, imageLoadProgress);
			__imageLoader.destroy();
			__imageLoader = null;
			
			__sourceList = null;
			__defaultSettings = null;
			__currentIndex = 0;
			
			super.destroy();
		}
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		protected function navigationButtonHandler(e:MouseEvent):void
		{
			switch (e.currentTarget)
			{
				case __nextButton :
					next();
					break;
				case __previousButton :
					previous();
					break;
			}
			
			checkNavigationButtons();
		}
		
		protected function imageLoadComplete(e:Event):void
		{
			swapImage();
			hidePreloader();
			dispatchEvent(new FullViewEvent(FullViewEvent.IMAGE_LOAD_COMPLETE, {target:this, index:__currentIndex}));
			//callSetter(__preloader, "progress", 0);
		}
		
		protected function imageLoadProgress(e:Event):void
		{
			var loader:LoaderInfo = __imageLoader.loader.contentLoaderInfo;
			if (__preloader.visible) callSetter(__preloader, "progress", int((100 * loader.bytesLoaded) / loader.bytesTotal));
		}
		
		protected function swapImage():void
		{
			__imageCanResize = false;
			mouseChildren = false;
			
			// hide current image
			TweenLite.to(__imageView.image, s.time * .3, {alpha:0, onComplete:swap});
			
			// NOTA: El boton usa dos tipos de transiciones, la primera se mueve junto cuando SI hay borde
			// y la segundo se disuelve y aparece con la imagen cuando NO hay borde 
			
			// Esconde el boton de cerrar si no hay borde
			if (!__border && s.alignCloseButtonWithImage) 
				TweenLite.to(__closeButtonView, s.time * .3, {alpha:0});
			
			// swap images
			function swap () 
			{
				// Determina si las imágenes tienen la misma proporcion
				var sameProportion:Boolean = (__imageView.image.bitmapData.width / __imageView.image.bitmapData.height) == (__imageLoader.image.bitmapData.width / __imageLoader.image.bitmapData.height);
				
				__imageView.image = __imageLoader.image;
				__imageView.image.alpha = 0;
				
				// validates if border enabled
				// En caso que las imagenes tengan la misma proporcion se omite la transicion del borde
				if (__border && !sameProportion) 
				{
					var position:Point = DisplayObjectUtils.getGlobalPosition(__imageView.image);
					var rect:Rectangle = new Rectangle(position.x - s.borderSize, position.y - s.borderSize, Math.round(__imageView.image.width) + s.borderSize * 2, Math.round(__imageView.image.height) + s.borderSize * 2);

					TweenLite.to(__border, s.time * .5, {x:rect.x, y:rect.y, width:rect.width, height:rect.height, ease:s.easeForSwap, onComplete:function () {
						showNewImage();
					}});
					
					// Mueve el boton cerrar junto con el borde
					if (s.alignCloseButtonWithImage) {
						var initCloseButtonPosition:Point = DisplayObjectUtils.getGlobalPosition(__closeButtonView);
						DisplayObjectUtils.align(__closeButtonView, __imageView.image, s.closeButtonAlign, s.closeButtonHMargin, s.closeButtonVMargin, true, true);
						TweenLite.from(__closeButtonView, s.time * .5, {x:initCloseButtonPosition.x, y:initCloseButtonPosition.y, ease:s.easeForSwap});
					}
				}
				// show new image immediately
				else
				{
					showNewImage();
					
					// Alinea y Muestra el boton cerrar junto con la imagen
					if (s.alignCloseButtonWithImage) {
						DisplayObjectUtils.align(__closeButtonView, __imageView.image, s.closeButtonAlign, s.closeButtonHMargin, s.closeButtonVMargin, true, true);
						TweenLite.to(__closeButtonView, s.time * .5, {alpha:1});
					}
				}
			}
			
			function showNewImage () {
				TweenLite.to(__imageView.image, s.time, {alpha:1});
				__imageCanResize = true;
				mouseChildren = true;
			}
		}
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		protected function load():void
		{
			showPreloader();
			__imageLoader.source = __sourceList[__currentIndex];
			dispatchEvent(new FullViewEvent(FullViewEvent.IMAGE_LOAD_START, {target:this, source:__sourceList[__currentIndex]}));
		}
		
		override protected function openComplete():void
		{
			super.openComplete();
			checkNavigationButtons();
			
			load(); // reload the first image
		}
		
		protected function checkNavigationButtons():void
		{
			// previous
			if (__currentIndex == 0)
				callFunction(__previousButton, "hide");
			else 
				callFunction(__previousButton, "show");
			
			// next
			if (__currentIndex == __sourceList.length-1)
				callFunction(__nextButton, "hide");
			else 
				callFunction(__nextButton, "show");
		}
		
		protected function showPreloader():void
		{
			callFunction(__preloader, "show", null, function () 
			{
				//TweenLite.to(__preloader, s.time * .5, {autoAlpha:1, delay:.3});
				TweenLite.to(__preloader, s.time * .5, {autoAlpha:1});
			});
		}
		
		protected function hidePreloader():void
		{
			callFunction(__preloader, "hide", null, function () 
			{
				TweenLite.to(__preloader, s.time * .5, {autoAlpha:0, onComplete:function () {
					callSetter(__preloader, "progress", 0); // reset preloader
				}});
			});
		}
		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
	}
}