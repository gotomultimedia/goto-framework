/*
FullViewNavigationButton.as

Created by Silverfenix on 05.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.display.fullView
{
	import com.goto.display.components.GenericComponent;
	import flash.display.DisplayObject;
	import flash.utils.getDefinitionByName;
	import flash.events.MouseEvent;
	import com.greensock.TweenLite;
	import flash.display.Sprite;
	import fl.motion.easing.Exponential;
	import flash.geom.Rectangle;
	//import com.goto.utils.Console;

	public class FullViewNavigationButton extends GenericComponent 
	{
		
		// Privtae & Protected Instance Vatiables
		protected var __direction:String;
		protected var __assets:DisplayObject;
		protected var __assetHolder:Sprite;
		protected var __overAlpha:Number = .5;
		protected var __outAlpha:Number = 1;
		protected var __isHide:Boolean = true;
		
		/**
	 	* @constructor
	 	*/
		public function FullViewNavigationButton(direction:String = "left", asset:Class = null)
		{
			mouseChildren = false;
			buttonMode = true;
			
			__direction = direction;
			
			if (!asset)
			{
				__assets = new FullViewDefaultNavigationButtonAssets;
			} else {
				__assets = new asset;
			}
			
			super();
		}
		
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////

		override protected function onComponentReady():void 
		{
			addEventListener(MouseEvent.MOUSE_OVER, mouseHandler, false, 0, true);
			addEventListener(MouseEvent.MOUSE_OUT, mouseHandler, false, 0, true);
		}

		override protected function addChildren():void
		{
			super.addChildren();
			
			__assetHolder = new Sprite;
			addChild(__assetHolder);
			
			__assets.alpha = __outAlpha;
			__assets.x = -__assets.width;
			__assetHolder.addChild(__assets);
			
			if (__direction == "right") 
			{
				__assetHolder.scaleX = -1;
				__assetHolder.x = __assets.width;
			}
			
			// automasking
			scrollRect = new Rectangle(0, 0, __assets.width, __assets.height);
		}

		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////
		override public function draw ():void
		{
			//super.draw();
			__width = __assets.width;
			__height = __assets.height;
		}
		
		public function show():void
		{
			if (__isHide)
			{
				__isHide = false;
				showButton();
			}
		}
		
		public function hide():void
		{
			if (!__isHide)
			{
				__isHide = true;
				hideButton();
			}
		}
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		protected function mouseHandler(e:MouseEvent):void
		{
			//if (__isHide) return;
			
			switch (e.type)
			{
				case MouseEvent.MOUSE_OVER :
					mouseOver();
					break;
				case MouseEvent.MOUSE_OUT :
					mouseOut();
					break;
			}
		}
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		protected function showButton():void
		{
			TweenLite.to(__assets, .5, {x:0, ease:Exponential.easeInOut});
		}
		
		protected function hideButton():void
		{
			TweenLite.to(__assets, .5, {x:-__assets.width, ease:Exponential.easeInOut});
		}
		
		protected function mouseOver():void
		{
			TweenLite.to(__assetHolder, .5, {alpha:__overAlpha});
		}
		
		protected function mouseOut():void
		{
			TweenLite.to(__assetHolder, .5, {alpha:__outAlpha});
		}
		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
	}
}