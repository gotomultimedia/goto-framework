/*
FullViewEvent.as

Created by Silverfenix on 05.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.display.fullView 
{
    import flash.events.Event;  
	/**
	 * Contains all the information of the VPlayer events.
	 */
    public class FullViewEvent extends Event
    {
		/** Un objeto con parametros extras */
		public var params:Object; 
		
		public static const OPEN_START:String = "openStart";
		public static const OPEN_COMPLETE:String = "openComplete";
		public static const CLOSE_START:String = "closeStart";
		public static const CLOSE_COMPLETE:String = "closeComplete";
		public static const IMAGE_LOAD_COMPLETE:String = "imageLoadComplete";
		public static const IMAGE_LOAD_START:String = "imageLoadStart";
		public static const NEXT:String = "next";
		public static const PREVIOUS:String = "previous";
		
				
		/**
		 * @param _type Event type.
		 * @param _params An object with the extra parameters that you want to send when a FullViewEvent is dispatched.;
		 * @param _bubbles Determines if the event object participates in the event flux propagation phase. Default value is <code>false</code>.;
		 * @param _cancelable  Determines if the Event object can be cancelled. Default value is <code>false</code>.;
		 */
        public function FullViewEvent(_type:String, _params:Object = null, _bubbles:Boolean = false, _cancelable:Boolean = false)
        {
            super(_type, _bubbles, _cancelable);
            params = _params;
        }
		
		/**
		 * Duplicates an instance of the Event subclass.
		 */
        public override function clone():Event
        {
            return new FullViewEvent(type, params, bubbles, cancelable);
        }
		
		/**
		 * A useful function to implement the toString() method in the customized Event class of ActionScript 3.0. It is recommended to anulate the toString() method, but isn't necessary.
		 */
        public override function toString():String
        {
            return formatToString("FullViewEvent", "params", "type", "bubbles", "cancelable");
        }
    }
}