/*
FullViewDefaultCloseButtonView.as

Created by Silverfenix on 04.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.display.fullView
{
	import com.goto.display.components.GenericComponent;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	//import com.goto.utils.Console;
	import com.greensock.TweenLite;
	
	public class FullViewDefaultCloseButtonView extends GenericComponent 
	{	
		protected var __assets:Sprite;
		
		/**
	 	* @constructor
	 	*/
		public function FullViewDefaultCloseButtonView()
		{
			buttonMode = true;
			super();
		}

		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		/*
		override protected function onComponentReady():void {
			// sentences
		}
		*/
		override protected function addChildren():void
		{
			super.addChildren();
			__assets = new FullViewDefaultCloseButtonViewAssets;
			addChild(__assets);
			
			//__assets.addEventListener(MouseEvent.CLICK, handler, false, 0, true);
			__assets.addEventListener(MouseEvent.MOUSE_OVER, handler, false, 0, true);
			__assets.addEventListener(MouseEvent.MOUSE_OUT, handler, false, 0, true);
		}

		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////

		override public function draw ():void
		{
			super.draw();
			__width = __assets.width;
			__height = __assets.height;
		}
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		private function handler(e:MouseEvent):void
		{
			switch (e.type)
			{
				case MouseEvent.MOUSE_OVER :
					TweenLite.to(__assets, .5, {alpha:.5});
					break;
				case MouseEvent.MOUSE_OUT :
					TweenLite.to(__assets, .5, {alpha:1});
					break;
			}
		}
		//////////////////////////////////////////////////////////
		// Private Methods
		//////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
	}
}