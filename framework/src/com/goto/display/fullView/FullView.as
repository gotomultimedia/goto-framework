/*
FullView.as

Created by Silverfenix on 02.05.2011.
Copyright 2011 goTo! Multimedia.
*/
package com.goto.display.fullView
{
	import com.goto.display.components.GenericComponent;
	import com.goto.display.ImageView;
	import com.goto.display.Draw;
	import com.goto.utils.DisplayObjectUtils;
	import com.goto.utils.Console;
	import com.goto.utils.ObjectUtils;
	import flash.display.Stage;
	import flash.display.Sprite;
	import flash.utils.getDefinitionByName;
	import fl.motion.easing.Exponential;
	
	import com.greensock.TweenLite;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.plugins.AutoAlphaPlugin;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.display.DisplayObject;
	
	public class FullView extends GenericComponent 
	{
		
		// Privtae & Protected Instance Vatiables
		protected var __imageView:ImageView;
		protected var __background:Sprite;
		protected var __border:Sprite;
		protected var __imageCanResize:Boolean;
		protected var __closeButtonView:DisplayObject;
		protected var __privateSettings:Object; // private settings
		protected var __ps:Object; 				// Shortcut of privateSettings
		
		protected var s:Object;
		
		// Public Variables
		public var settings:Object;
		
		public static const VERSION:String = "1.0.0";
		
		/**
	 	* @constructor
	 	*/
		public function FullView(sett:Object = null)
		{	
			TweenPlugin.activate([AutoAlphaPlugin]);
			
			// default settings
			settings = {
				currentImageViewReference: null,
				time: 1.2,
				easeForOpen: Exponential.easeInOut,
				easeForClose: Exponential.easeInOut,
				backgroundAlpha: 0.9,
				backgroundColor: 0x00,
				backgroundCanClose: true,
				cloneImage: true,
				margin: 30,
				borderSize:0,
				borderColor:0xffffff,
				closeButtonView: "com.goto.display.fullView.FullViewDefaultCloseButtonView",
				closeButtonAlign: "topRight",		// recommended top, topRight, topLeft, center
				alignCloseButtonWithImage: true, 	// or window
				closeButtonHMargin: -1,
				closeButtonVMargin: -1
			}
			
			// A quick access to settings object
			s = settings;
			
			if (sett) ObjectUtils.setProperties(settings, sett, null, null, true); 
			
			// Private settings
			__privateSettings = {
				scaleMode: "fitToWidthAndHeight"
			}
			__ps = __privateSettings;
			
			super();
		}

		//////////////////////////////////////////////////////////
		// Generic Component implemented methods
		//////////////////////////////////////////////////////////
		
		override protected function onStage():void
		{
			stage.addEventListener(Event.RESIZE, onStageResize, false, 0, true);
			onStageResize(); // Set initial size
		}

		override protected function onRemovedFromStage():void
		{
			stage.removeEventListener(Event.RESIZE, onStageResize);
		}
		
		override protected function addChildren():void
		{
			super.addChildren();
			
			__background = Draw.rectangle(960, 610, s.backgroundColor);
			
			if (s.borderSize > 0) 
				__border = Draw.rectangle(960, 610, s.borderColor);
				
			__imageView = new ImageView(false);
			//__imageView.showMask = true;
			addChild(__imageView);
			
			if (s.closeButtonView) {
				FullViewDefaultCloseButtonView;
				__closeButtonView = new (getDefinitionByName(s.closeButtonView) as Class);
				__closeButtonView.addEventListener(MouseEvent.CLICK, onCloseButtonClick, false, 0, true);
				__closeButtonView.visible = false;
				__closeButtonView.alpha = 0;
			}
		}
		
		override public function draw ():void
		{
			super.draw();
			
			if (__background.parent) 
			{
				__background.width = __width;
				__background.height = __height;
			}

			if (__imageCanResize) 
			{
				resizeImageView();
				
				// update CloseView position
				if (__closeButtonView && __closeButtonView.visible) 
				{
					var closeButtonAlignTarget:* = (s.alignCloseButtonWithImage) ? __imageView.image : this;
					DisplayObjectUtils.align(__closeButtonView, closeButtonAlignTarget, s.closeButtonAlign, s.closeButtonHMargin, s.closeButtonVMargin, true, true);
				}
					
			}
		}
		
		protected function resizeImageView():void
		{
			__imageView.setSize(__width - s.margin * 2, __height - s.margin * 2);
			DisplayObjectUtils.align(__imageView, this); // Align to center
			
			if (__border) 
			{
				__border.width = Math.round(__imageView.image.width) + s.borderSize * 2;
				__border.height = Math.round(__imageView.image.height) + s.borderSize * 2;
				//DisplayObjectUtils.align(__border, this, "center", 0, 0, true);
				DisplayObjectUtils.align(__border, __imageView.image, "center", 0, 0, true, true);
			}
		}
		//////////////////////////////////////////////////////////
		// Public Methods
		//////////////////////////////////////////////////////////
		
		public function openWithImageView(imageViewReference:ImageView):void
		{
			s.currentImageViewReference = imageViewReference;
			
			if (!s.currentImageViewReference.image.stage) // Check Stage
			{
				Console.log("[FullView ERROR: La imagen que desean apliar no esta agregada al escenario]");
				return;
			}
			
			imageViewReference.stage.addChild(this); // Add To Stage
			
			open();
		}

		protected function open():void
		{
			dispatchEvent(new FullViewEvent(FullViewEvent.OPEN_START, {target:this}));
			
			if (!s.currentImageViewReference) 
			{
				Console.log("[FullView WARNING: No es posible usar el método 'open()' si no existe una imagen de referencia para ampliar]");
				return;
			}
			
			mouseChildren = false;
			__imageCanResize = false;
			showBackground();
			
			// end state
			__imageView.image = s.cloneImage ? new Bitmap(s.currentImageViewReference.image.bitmapData.clone()) : s.currentImageViewReference.image;
			__imageView.scaleMode = __ps.scaleMode;
			resizeImageView();
			
			// save end state
			var endRect:Rectangle = new Rectangle(__imageView.x, __imageView.y, __imageView.width, __imageView.height);
			var endImgRect:Rectangle = new Rectangle(__imageView.image.x, __imageView.image.y, __imageView.image.width, __imageView.image.height);
			var initRect:Rectangle = new Rectangle(s.currentImageViewReference.globalX, s.currentImageViewReference.globalY, s.currentImageViewReference.width, s.currentImageViewReference.height);
			
			// initial state
			__imageView.scaleMode = s.currentImageViewReference.scaleMode;
			__imageView.move(initRect.x, initRect.y);
			__imageView.setSize(initRect.width, initRect.height);
			__imageView.updatePosition = false;
			__imageView.updateSize = false;
			
			// tween
			TweenLite.to(__imageView, s.time, {x:endRect.x, y:endRect.y, width:endRect.width, height:endRect.height, ease:s.easeForOpen});
			TweenLite.to(__imageView.image, s.time, {x:endImgRect.x, y:endImgRect.y, width:endImgRect.width, height:endImgRect.height, ease:s.easeForOpen, onComplete:function () {
				mouseChildren = true;
				__imageCanResize = true;
				
				__imageView.updatePosition = true;
				__imageView.updateSize = true;
				__imageView.scaleMode = __ps.scaleMode;
				
				if (__border) 
				{
					addChildAt(__border, 1);
					TweenLite.from(__border, s.time, {
						alpha:0/*, 
						x:__imageView.x + __imageView.image.x, 
						y:__imageView.y + __imageView.image.y, 
						width:__imageView.image.width, 
						height:__imageView.image.height, 
						ease:s.easeWhenOpen*/});
				}
				
				openComplete();
			}});
			
			// release rectangles
			endRect = null;
			endImgRect = null;
			initRect = null;
		}

		public function close():void
		{
			dispatchEvent(new FullViewEvent(FullViewEvent.CLOSE_START, {target:this}));
			
			if (__background.hasEventListener(MouseEvent.CLICK)) 
				__background.removeEventListener(MouseEvent.CLICK, onBackgroundClicked);
			
			mouseChildren = false;
			__imageCanResize = false;
		
			// set imageview to end values		
			__imageView.scaleMode = s.currentImageViewReference.scaleMode;
			__imageView.setSize(s.currentImageViewReference.width, s.currentImageViewReference.height);
			__imageView.move(s.currentImageViewReference.globalX, s.currentImageViewReference.globalY);

			// save end values
			var endRect:Rectangle = new Rectangle(__imageView.x, __imageView.y, __imageView.width, __imageView.height);
			var endImgRect:Rectangle = new Rectangle(__imageView.image.x, __imageView.image.y, __imageView.image.width, __imageView.image.height);
			
			// reset to actual values
			__imageView.scaleMode = __ps.scaleMode;
			resizeImageView();
			
			// avoid imageview redraw
			__imageView.updatePosition = false;
			__imageView.updateSize = false;
			
			// close border
			if (__border) 
			{
				TweenLite.to(__border, s.time, {
					alpha:0, 
					x:s.currentImageViewReference.globalX + s.currentImageViewReference.width * .5, 
					y:s.currentImageViewReference.globalY + s.currentImageViewReference.height * .5,
					width:0, 
					height:0, 
					ease:s.easeForClose,
					delay:s.time * 0.3,
					onComplete:function () {
						removeChild(__border);
						__border.alpha = 1;
					}});
			}
			
			TweenLite.to(__imageView, s.time, {x:endRect.x, y:endRect.y, width:endRect.width, height:endRect.height, ease:s.easeForClose, delay:s.time * 0.3});
			TweenLite.to(__imageView.image, s.time, {x:endImgRect.x, y:endImgRect.y, width:endImgRect.width, height:endImgRect.height, ease:s.easeForClose, delay:s.time * 0.3, onComplete:function () {
				mouseChildren = true;
				
				__imageView.updatePosition = true;
				__imageView.updateSize = true;
				
				if (s.cloneImage) 
					__imageView.clear();
				else 
					s.currentImageViewReference.image = __imageView.image;
				
				closeComplete();
			}});
			
			hideBackground();
			hideCloseButton();
			
			// Release immediately
			endRect = null;
			endImgRect = null;
		}
		
		public function softDestroy():void
		{
			// check if is open
			if (__imageView.image)
			{
				hideBackground();
				hideCloseButton();
				
				if (__border) TweenLite.to(__border, 0.5, {autoAlpha:0});
				
				TweenLite.to(__imageView.image, 0.5, {autoAlpha:0, onComplete:function () {
					destroy();
				}});
			}
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			if (this.parent) this.parent.removeChild(this);
			
			if (__imageView.image)
			{
				if (s.cloneImage) 
					__imageView.clear();
				else 
					s.currentImageViewReference.image = __imageView.image;
					
					
				__imageView.destroy();
				__imageView = null;
			}
			
			super.destroy();
		}
				
		//////////////////////////////////////////////////////////
		// Events Handler
		//////////////////////////////////////////////////////////
		protected function onStageResize(e:Event = null):void
		{
			// Set stage size for this FullView instance
			__width = stage.stageWidth;
			__height = stage.stageHeight;
			
			draw();
		}

		protected function onBackgroundClicked(e:MouseEvent):void
		{
			close();
		}
		
		protected function onCloseButtonClick(e:MouseEvent):void
		{
			close();
		}
		//////////////////////////////////////////////////////////
		// Protected Methods
		//////////////////////////////////////////////////////////
		protected function showBackground():void
		{
			if (s.backgroundAlpha == 0) return;
			
			addChildAt(__background, 0);
			
			// set initial properties
			__background.width = __width;
			__background.height = __height;
			__background.alpha = 0;
			
			TweenLite.to(__background, s.time, {alpha:s.backgroundAlpha});
		}
		
		protected function hideBackground():void
		{
			if (s.backgroundAlpha == 0) return;

			TweenLite.to(__background, s.time, {alpha:0, delay:s.time * 0.3, onComplete:function () {
				removeChild(__background);
			}});
		}
		
		protected function openComplete():void
		{
			if (s.backgroundCanClose && s.backgroundAlpha > 0) 
				__background.addEventListener(MouseEvent.CLICK, onBackgroundClicked, false, 0, true);
				
			showCloseButton();
			
			dispatchEvent(new FullViewEvent(FullViewEvent.OPEN_COMPLETE, {target:this}));
		}
		
		protected function closeComplete():void
		{
			//s.currentImageViewReference = null;
			dispatchEvent(new FullViewEvent(FullViewEvent.CLOSE_COMPLETE, {target:this}));
		}
		
		protected function showCloseButton():void
		{
			addChild(__closeButtonView);
			
			var closeButtonAlignTarget:* = (s.alignCloseButtonWithImage) ? __imageView.image : this;
			DisplayObjectUtils.align(__closeButtonView, closeButtonAlignTarget, s.closeButtonAlign, s.closeButtonHMargin, s.closeButtonVMargin, true, true);
			
			callFunction(__closeButtonView, "show", null, function () {
				TweenLite.to(__closeButtonView, s.time, {autoAlpha:1});
			});
		}
		
		protected function hideCloseButton():void
		{
			callFunction(__closeButtonView, "hide", null, function () {
				TweenLite.to(__closeButtonView, s.time * 0.5, {autoAlpha:0, onComplete:function () {
					__closeButtonView.parent.removeChild(__closeButtonView);
				}});
			});
		}
		
		protected function callFunction(target:*, property:String, params:Array = null, fallBack:Function = null, fallBackParams:Array = null):void
		{
			if (target.hasOwnProperty(property)) 
				(target as Object)[property].apply(null, params);
			else
				if (fallBack != null) fallBack.apply(null, fallBackParams);
		}
		
		protected function callSetter(target:*, accessor:String, value:*, fallBack:Function = null, fallBackParams:Array = null):void
		{
			if (target.hasOwnProperty(accessor)) 
				(target as Object)[accessor] = value;
			else
				if (fallBack != null) fallBack.apply(null, fallBackParams);
		}
		
		//////////////////////////////////////////////////////////
		// Private Methods
		//////////////////////////////////////////////////////////

		//////////////////////////////////////////////////////////
		// Setters && Getters
		//////////////////////////////////////////////////////////
	}
}