//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2012 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.display
{
	import com.goto.utils.XMLUtils;
	
	import flash.utils.Dictionary;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.display.Bitmap;
	
	/**
	 * Class.
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  06.01.2012
	 */
	public class Atlas extends Object
	{
	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
	
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
	
		/**
		 * @constructor
		 */
		public function Atlas(texture:BitmapData, atlas:XML)
		{
			_texture = texture;
			_atlas = atlas;
			
			parse();
			super();
		}
	
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		protected var _data:Dictionary;
		public function get data():Dictionary
		{
			return _data;
		}
		
		protected var _texture:BitmapData;
		public function get texture():BitmapData
		{
			return _texture;
		}
		
		protected var _atlas:XML;
		public function get atlas():XML
		{
			return _atlas;
		}
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function destroy():void
		{
			_texture.dispose();
			_atlas = null;
			_data = null;
		}
		
		/**
		 * @private
		 */
		public function getBitmap(name:String):Bitmap
		{
			return new Bitmap(getBitmapData(name), "auto", true);
		}
		
		/**
		 * @private
		 */
		public function getBitmapData(name:String):BitmapData
		{
			if (!_texture || !_atlas) return null;
			
			if (!_data[name])
			{
				throw new Error("[Atlas] There is not sprite with the name '"+name+"'");
			}
			
			var s:Object = _data[name];
			
			// Todo, trim, rotate, pool
			var bd:BitmapData = new BitmapData(s.w, s.h, true, 0xFF0000);
			bd.copyPixels(_texture, new Rectangle(s.x, s.y, s.w, s.h), new Point(0, 0));
			
			return bd;
		}
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
	
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		
		/**
		 * @private
		 */
		protected function parse():void
		{
			if (!_data) 
				_data = new Dictionary();

			for each (var sprite:XML in _atlas.sprite)
			{
				var name:String = sprite.@n.toString();
				
				_data[name] = {};
				
				XMLUtils.attributesToObject(sprite, _data[name]);
			}
		}
	}

}