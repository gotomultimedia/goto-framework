/*
FlowLayout.as
labs

Created by Silverfenix on 11/01/11.
Copyright 2011 goTo! Multimedia. All rights reserved.
*/
package com.goto.display.layout
{
	import flash.display.Sprite;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	
	public class FlowLayout extends GenericComponent {
		
		static public const HORIZONTAL:String = "horizontal";
		static public const VERTICAL:String = "vertical";
		
		protected var __verticalGap:int;
		protected var __horizontalGap:int;
		protected var __direction:String = HORIZONTAL;
		
		public function FlowLayout () 
		{
			addEventListener(Event.ADDED, onAddedOrRemovedChild, false, 0, true);
			addEventListener(Event.REMOVED, onAddedOrRemovedChild, false, 0, true);
			super();
		}
		//////////////////////////////////////////////////////////
		//
		// Public Methods
		//
		//////////////////////////////////////////////////////////
		
		override public function draw ():void
		{
			var xPos:Number = 0, yPos:Number = 0, isBiggerThatWidth:Boolean, isBiggerThatHeight:Boolean, maxHeightInRow:Number = 0, maxWidthInColumn:Number = 0;
			
			var i:uint = 0, child:DisplayObject;
			const l:uint = numChildren;
			
			if (__direction == HORIZONTAL) 
			{
				// Horizontal Layout
				for (i; i < l; i++) 
				{
					child = getChildAt(i);
					
					isBiggerThatWidth = (child.width + xPos) > __width;
					
					if (isBiggerThatWidth && xPos != 0) 
					{
						xPos = 0;
						yPos = yPos + maxHeightInRow + __verticalGap;
					}
									
					moveChild(child, xPos, yPos);
					
					xPos += child.width + __horizontalGap;
					maxHeightInRow = Math.max(child.height, maxHeightInRow);
				}
				
				__height = yPos + maxHeightInRow; // Auto suggest height
				
			} else {
				// Vertical Layout
				for (i; i < l; i++) 
				{
					child = getChildAt(i);
					
					isBiggerThatHeight = (child.height + yPos) > __height;
					
					if (isBiggerThatHeight && yPos != 0) 
					{
						yPos = 0;
						xPos = xPos + maxWidthInColumn + __horizontalGap;
					}
					
					moveChild(child, xPos, yPos);
					
					yPos += child.height + __verticalGap;
					maxWidthInColumn = Math.max(child.width, maxWidthInColumn);
				}
				
				__width = xPos + maxWidthInColumn; // Auto suggest width
			}
		}
				
		public function addMultiple (childrem:Array):void
		{
			var i:String;
			for (i in childrem) {
				addChild(childrem[i]);
			}
		}
		
		public function setGap (v:int, h:int):void
		{
			__verticalGap = v, __horizontalGap = h;
			draw();
		}
		//////////////////////////////////////////////////////////
		//
		// Events Handler
		//
		//////////////////////////////////////////////////////////
		protected function onAddedOrRemovedChild (e:Event = null):void
		{
			draw();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////
		protected function moveChild (child:DisplayObject, xPos:Number, yPos:Number):void
		{
			child.x = xPos;
			child.y = yPos;
		}
		
		//////////////////////////////////////////////////////////
		//
		// Setters && Getters
		//
		//////////////////////////////////////////////////////////
		public function set verticalGap (value:int):void
		{
			__verticalGap = value;
			draw();
		}
		
		public function get verticalGap ():int
		{
			return __verticalGap;
		}
		
		public function set horizontalGap (value:int):void
		{
			__horizontalGap = value;
			draw();
		}
		
		public function get horizontalGap ():int
		{
			return __horizontalGap;
		}
		
		public function set direction (value:String):void
		{
			__direction = value;
			draw();
		}
		
		public function get direction ():String
		{
			return __direction;
		}
	}
}