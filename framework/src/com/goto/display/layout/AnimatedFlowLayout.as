/*
AnimatedFlowLayoud.as
labs

Created by Silverfenix on 11/01/11.
Copyright 2011 goTo! Multimedia. All rights reserved.
*/
package com.goto.display.layout
{	
	import flash.display.DisplayObject;
	import fl.motion.easing.Exponential;
	
	import com.greensock.TweenLite;
	
	public class AnimatedFlowLayout extends FlowLayout {
		
		public var settings:Object;
		protected var __allowAnimations:Boolean = true;
		
		public function AnimatedFlowLayout () {
			
			settings = {
				time: .5,
				ease: Exponential,
				delay: 0
			}
			
			super();
		}
		
		//////////////////////////////////////////////////////////
		//
		// Protected Methods
		//
		//////////////////////////////////////////////////////////
		override protected function moveChild (child:DisplayObject, xPos:Number, yPos:Number):void
		{
			if (__allowAnimations) {
				TweenLite.to(child, settings.time, {x:xPos, y:yPos, ease:settings.ease, delay:settings.delay});
				return;
			}
			
			super.moveChild(child, xPos, yPos);
		}
		
		public function set allowAnimations (value:Boolean):void
		{
			__allowAnimations = value;
			draw();
		}
		
		public function get allowAnimations ():Boolean
		{
			return __allowAnimations;
		}
	}
}