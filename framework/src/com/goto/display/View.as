//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.display
{
	import com.goto.display.components.GenericComponent;
	import com.goto.utils.ObjectUtils;
	import com.goto.utils.Console;
	
	import com.greensock.*;
	
	import fl.motion.easing.Exponential;
	import fl.motion.easing.Linear;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * This class extends the functionality of GenericComponent, adds tween engine natively
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author goTo! Multmedia
	 * @since  31.12.2011
	 */
	public class View extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function View(snapToPixels:Boolean = true) 
		{
			TweenEngine = TweenMax;
			TweenLite.defaultEase = Exponential.easeInOut;
			
			super(snapToPixels);
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _isHidden:Boolean;
		/** @private */
		protected var TweenEngine:*;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function get isHidden():Boolean
		{
			return _isHidden;
		}
		
		/**
		 * @private
		 */
		public function get tweenEngine():*
		{
			return TweenEngine;
		}
		
		//--------------------------------------
		//  PUBLIC CHAIN METHODS
		//--------------------------------------
		
		/**
		 * Hides the current instance with an optional tween
		 * @param duration The tween duration, 0 is immediatelly
		 * @param options The optional tween's properties, put here special TweenMax's properties like: onComplete, onUpdate, etc
		 * @private
		 * @return self
		 */
		public function hide(duration:Number = 0.7, options:Object = null):*
		{
			if (_isHidden) return this;
			
			enabled = false;
			_isHidden = true;
			
			// Default props
			var props:Object = {autoAlpha:0};
			// Add options's props to props
			if (options) ObjectUtils.extend(props, options);
			
			// Start animation
			tween(props, duration);
			
			return this;
		}
		
		/**
		 * Shows the current instance with an optional tween
		 * @param duration The tween duration, 0 is immediatelly
		 * @param options The optional tween's properties, put here special TweenMax's properties like: onComplete, onUpdate, etc
		 * @private
		 * @return self
		 */
		public function show(duration:Number = 0.7, options:Object = null):*
		{	
			if (!_isHidden) return this;
					
			enabled = true;
			_isHidden = false;
			
			// Default props
			var props:Object = {autoAlpha:1};
			// Add options's props to props
			if (options) ObjectUtils.extend(props, options);
			
			// Start animation
			tween(props, duration);
			
			return this;
		}
		
		public function fadeTo(value:Number = 1, duration:Number = 0.5, options:Object = null):*
		{
			if (_isHidden) return this;
			
			// Default props
			var props:Object = {autoAlpha:value, ease:Linear.easeNone};
			// Add options's props to props
			if (options) ObjectUtils.extend(props, options);
			
			// Starts the animation
			tween(props, duration);
			
			return this;
		}
		/**
		 * Allows to tweens any public property of and specific object or the current instance
		 * @param props Props that will be tweened like: {x:10, y:20}, you can use special TweenMax properties like: {ease, onComplete, etc}
		 * the target can be set in 'props' param ie: {target:mc, x:10}, if there isn't target then the target is the current View instance
		 * @param duration Duration on interpolation in seconds
		 * @param tweenFunction The TweenMax function, ie: to, from, allFrom, allTo, etc.
		 * @private
		 * @return  
		 */
		public function tween(props:Object, duration:Number = 0.7, tweenFunction:String = "to"):*
		{
			TweenEngine[tweenFunction](resolveTarget(props), duration, props);
			
			return this;
		}
		
		/**
		 * TweenMax.to wrapper
		 * @private
		 * @return  
		 */
		public function tweenTo(props:Object, duration:Number = 0.7):*
		{
			return tween(props, duration, "to");
		}
		
		/**
		 * TweenMax.from wrapper
		 * @private
		 * @return  
		 */
		public function tweenFrom(props:Object, duration:Number = 0.7):*
		{
			tween(props, duration, "from");
			return this;
		}
		
		/**
		 * TweenMax.from wrapper
		 * @private
		 * @return  
		 */
		public function tweenFromTo(fromProps:Object, endProps:Object, duration:Number = 0.7):*
		{
			TweenEngine.fromTo(this, duration, fromProps, endProps);
			
			return this;
		}
		
		/**
		 * Performs a specific method after delay with optional params.<br/>
		 * 
		 * Example:
		 * 
		 * <code>
		 * function greetings(who:String) {
		 *     trace("Hello word", who);
		 * }
		 * 
		 * // Executes the greetings function with a delay of 1 second, plus, sends "kathe" as argument for the function
		 * view.delayedCall(1.0, greetings, ["kathe"]); // Hello word kathe
		 * 
		 * // Executes its own 'draw' method after 1 seconds of delay 
		 * view.delayedCall(1.0, "draw");
		 * </code>
		 * 
		 * @param delay Delay in seconds, 0 for call immediatelly 
		 * @param closure The closure function, use string for execute a public method of the current instance.
		 * @param closureParams An Array with the params for the function specified in closure param
		 * @private
		 * @return  
		 */
		public function delayedCall(delay:Number, closure:Object, closureParams:Array = null):*
		{
			TweenEngine.delayedCall(delay, getClosure(closure), closureParams);
			
			return this;
		}
		
		/**
		 * Kills delayed call
		 * @param closure The closure function that must be killed.
		 * @private
		 */
		public function killDelayedCall(closure:Object):*
		{
			TweenEngine.killTweensOf(getClosure(closure));
			
			return this;
		}
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------

		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			TweenEngine.killTweensOf(this);
			super.destroy();
		}
		
		
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * Gets the target, and remove from props 
		 * @private
		 */
		private function resolveTarget(props:Object):*
		{
			var target:* = props.target;
			
			if (!target) 
				return this;
			
			// delte target property fro object
			//props.target = "";
			delete props.target;
			
			return target;
		}
		
		/**
		 * Obtains a method pointer, if the argument for parameter 'closure' is a String,
		 * then returns an object's method who made the call
		 * @private
		 */
		private function getClosure(closure:Object):Function
		{
			var clos:Function;
			
			if (closure is String)
			{
				clos = this[closure];
			}
			else if (closure is Function)
			{
				clos = closure as Function;
			}
			else
			{
				throw new Error("The closure param must be a String or Function object");
			}
			
			return clos;
		}		
	}
}

