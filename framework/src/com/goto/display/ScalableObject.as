//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.display
{
	
	import flash.display.Sprite;
	import com.goto.display.components.GenericComponent;
	import org.bytearray.display.ScaleBitmap;
	import flash.geom.Rectangle;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Bitmap;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 * This class is based on ScaleBitmapSprite
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  15.10.2011
	 */
	public class ScalableObject extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 * @constructor
		 * @param skin Objeto BitmapData
		 * @param innerRect Representa la zona que puede ser transformada del objeto escalable
		 * 					puede ser un Rectangulo o un Arreglo con las cordenadas del rectángulo Ej: [x, y, width, height]
		 * @param outerRect Un rectangulo que representa la parte externa de un objeto que puede ser escalado.
		 * 					Esta propiedad suele ser útil para delimitar ignorar la sombra y arroje el tamaño real del objeto
		 * @param smooth -
		 */
		public function ScalableObject(skin:BitmapData = null, innerRect:Object = null, outerRect:Object = null, smooth:Boolean = false) 
		{
			enabled = false;
			
			_skin = skin;
			_innerRect = validateRect(innerRect);
			_outerRect = validateRect(outerRect);
			_smooth = smooth;
			
			super();
		}
		
		//
		// Alberto romero
		// 
		
		//---------------------------------------
		// INIT
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function init():void
		{
			setSkin(_skin, _innerRect, _outerRect, _smooth, false);
										
			super.init();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _skin:BitmapData;
		protected var _innerRect:Rectangle;
		protected var _outerRect:Rectangle;
		protected var _smooth:Boolean;
		protected var _outerWidth:Number;
		protected var _outerHeight:Number;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
		
		//---------------------------------------
		// CLASS METHODS
		//---------------------------------------
		
		/**
		 * Crea un objeto ScalableObject desde un objeto BitmapData
		 * @param bd BitmapData
		 * @param inerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @param outerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @return ScalableObject
		 */
		public static function fromBitmapData (bd:BitmapData, inerRect:Object = null, outerRect:Object = null):ScalableObject
		{
			return new ScalableObject(bd, inerRect, outerRect);
		}
		
		/**
		 * Crea un objeto ScalableObject desde un objeto Bitmap
		 * @param bitmap Bitmap
		 * @param inerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @param outerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @return ScalableObject 
		 */
		public static function fromBitmap (bitmap:Bitmap, inerRect:Object = null, outerRect:Object = null):ScalableObject
		{
			return fromBitmapData(bitmap.bitmapData, inerRect, outerRect);
		}
		
		/**
		 * Crea un objeto ScalableObject desde un objeto Sprite
		 * El sprite debe tener obligatoriamente la propiedad scale9Grid asignada.
		 * @param sprite Sprite
		 * @param outerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @return ScalableObject 
		 * @throws Error Si el sprite recibido en el parametro sprite no tiene definida la propiedad scale9Grid.
		 */
		public static function fromSprite (sprite:Sprite, outerRect:Object = null):ScalableObject
		{
			if (!sprite.scale9Grid)
				throw new Error("[ScalableObject] scale9Grid property must be assigned to the sprite");
			
			var inner:Rectangle = sprite.scale9Grid;
			var outer:Rectangle;
			
			if (outerRect)
				outer = validateRect(outerRect);
			else
				outer = null;
			
			return fromBitmapData(snap(sprite), inner, outer);
		}
		
		/**
		 * Devuelve una representación exacta como un objeto BitmapData del objeto de visualización enviado en el parametro target 
		 * @param target DisplayObject
		 * @return BitmapData 
		 */
		public static function snap(target:DisplayObject):BitmapData
		{
			var bd:BitmapData = new BitmapData((target.width) >> 0, 	//width
											   (target.height) >> 0,	//height
											   true,					//transparent
											   0xFF0000);				//fillcolor
			bd.draw(target);
			return bd;
		}
		
		/**
		 * Convierte un Arreglo en un Rectangulo.
		 * Si el parametro r es null entonces es devuleto ese mismo valor
		 * @param r Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height].
		 * @return Rectangle 
		 */
		public static function validateRect (r:Object):Rectangle
		{
			if (r is Rectangle || r == null) 
			{
				return r as Rectangle; // returns the same value
			}
			else if (r is Array) 
			{
				var rect:Rectangle = new Rectangle();
				if (r[0]) rect.x = r[0];
				if (r[1]) rect.y = r[1];
				if (r[2]) rect.width = r[2];
				if (r[3]) rect.height = r[3];
				return rect;
			}
			else
			{
				throw new Error("[ScalableObject] Invalid value for a rectangle of skin, must be a rectangle or an Array like this: [x, y, width, height]");
			}			
		}
		
		/**
		 * @private
		 */
		public function get innerRect():Rectangle
		{
			return _innerRect;
		}

		public function set innerRect(value:Rectangle):void
		{
			if (value !== _innerRect)
			{
				_innerRect = value;
				draw();
			}
		}
		
		public function get outerRect():Rectangle
		{
			return _outerRect;
		}

		public function set outerRect(value:Rectangle):void
		{
			if (value !== _outerRect)
			{
				_outerRect = value;
				draw();
			}
		}
		
		/**
		 * @private
		 */
		public function get skin():BitmapData
		{
			return _skin;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			if (_skin)
				_skin.dispose();
			
			super.destroy();
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Actualiza la piel
		 * @param newSkin BitmapData or Sprite
		 * @param innerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @param outerRect Rectángulo ó Array con las coordenadas del un rectangulo así: [x, y, width, height]
		 * @param smooth Boolean
		 * @return ScalableObject 
		 */
		public function changeSkin(newSkin:Object, innerRect:Object = null, outerRect:Object = null, smooth:Boolean = false):ScalableObject
		{
			var s:BitmapData;
			
			if (newSkin is Sprite)
			{
				if (!innerRect && !newSkin.scale9Grid)
					throw new Error("[ScalableObject] scale9Grid property must be assigned to the sprite");
				
				innerRect = newSkin.scale9Grid as Object;
				s = snap(newSkin as DisplayObject);
			}
			else if (newSkin is BitmapData)
			{
				s = newSkin as BitmapData;
			}
			else
			{
				throw new Error("[ScalableObject] newSkin solo puede ser un objeto Sprite con scale9Grid ó un objeto BitmapData");
			}
			
			setSkin(s, innerRect, outerRect, smooth, true);
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function setRectangles(inner:Object, outer:Object = null, needsLayout:Boolean = true):ScalableObject
		{
			_innerRect = validateRect(inner);
			_outerRect = validateRect(outer);
			
			if (_outerRect)
			{
				_outerWidth = _skin.width - _outerRect.width;
				_outerHeight = _skin.height - _outerRect.height;
			}
			else
			{
				_outerWidth = 0;
				_outerHeight = 0;
			}
			
			setMinSize(_skin.width - _innerRect.width - _outerWidth + 2,
					   _skin.height - _innerRect.height - _outerHeight + 2);
					
			if (needsLayout) draw();
			
			return this;
		}
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		
		/**
		 * Lo mismo que changeSkin
		 * @private
		 */
		private function setSkin(skin:BitmapData = null, innerRect:Object = null, outerRect:Object = null, smooth:Boolean = false, needsLayout:Boolean = true):void
		{
			//
			//if (!_skin) return;
			
			_skin = skin;
			_innerRect = validateRect(innerRect);
			_outerRect = validateRect(outerRect);
			_smooth = smooth;
			
			if (!_skin)
			{
				_skin = drawBox(15, 15, 0xffffff);
				_innerRect = new Rectangle(5, 5, 5, 5);
			}
			
			if (!_innerRect)
			{
				_innerRect = new Rectangle(1, 1, _skin.width-2, _skin.height-2);
			}
			
			if (_outerRect)
			{
				_outerWidth = _skin.width - _outerRect.width;
				_outerHeight = _skin.height - _outerRect.height;
			}
			else
			{
				_outerWidth = 0;
				_outerHeight = 0;
			}
			
			setMinSize(_skin.width - _innerRect.width - _outerWidth + 2,
					   _skin.height - _innerRect.height - _outerHeight + 2);
					
			// Redraw if needed
			if (needsLayout) draw();
		}
		
		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
				
		/**
		 * @inheritDoc
		 */
		override public function draw ():void
		{
			if (!_skin) return;
			
			super.draw();
			
			
			graphics.clear();
					
			ScaleBitmap.draw(_skin, 
							 graphics, 
							 (__width + _outerWidth) >> 0, 		// Faster Math.floor() replacement
							 (__height + _outerHeight) >> 0, 	// Faster Math.floor() replacement
							 _innerRect, 
							 _outerRect, 
							 _smooth);
		}
		
		/**
		 * Dibuja un rectangulo blanco es devuelto como un objeto BitmapData
		 * @param w Ancho
		 * @param h Alto
		 * @param color Color
		 * @private
		 * @return BitmapData representación exacta del rectangulo 
		 */
		protected function drawBox (w:uint = 10, h:uint = 10, color:uint = 0):BitmapData
		{
			var sp:Sprite = new Sprite();
			sp.graphics.beginFill(color);
			sp.graphics.drawRect(0, 0, w, h);
			sp.graphics.endFill();

			return snap(sp);
		}
		
	}
}