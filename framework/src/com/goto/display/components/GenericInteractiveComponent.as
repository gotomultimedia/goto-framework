//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.display.components
{

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
		
	/**
	 * Sprite subclass.
	 * 
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  20.10.2011
	 */
	public class GenericInteractiveComponent extends Sprite
	{
	
		//--------------------------------------
		// CLASS CONSTANTS
		//--------------------------------------
	
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------
		
		/**
		 * @example	El siguiente código demuestra el uso adecuado del método onClick():
		 * <listing version="3.0">
		 * var c1:GenericComponentSubClass = new GenericComponentSubClass().
		 *	   onClick(handler1).						// Send mouse event.
		 * 	   onClick(handler2, false, ["c1", 1]).		// Send arguments to handler without event.
		 * 
		 * c1.onClick(c1.move, false, [100, 100]); 		// move c1 to x:100 y:100.
		 * 
		 * function handler1(event:MouseEvent):void {
		 * 	trace(event);
		 * }
		 * 
		 * function handler2(name:String, index:uint):void {
		 * 	trace("handler 2:", name, "index:", index); // handler 2: c1 index: 1
		 * }
		 * </listing>
		 *	@constructor
		 */
		public function GenericInteractiveComponent()
		{
			_self = this;
			super();
		}
		
		
		//---------------------------------------
		// PROTECTED VARIABLES
		//---------------------------------------
		/** @private Pointer to this*/
		protected var _self:*;
		
		//--------------------------------------
		//  PRIVATE VARIABLES
		//--------------------------------------
		
		/** @private Store all listeners in an object with this proporties: type, listener, args and send. */
		private var _listeners:Dictionary;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------
	
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * Escucha el evento MouseEvent.CLICK
		 * @param listener El escuchador del evento.
		 * @param sendEventToListener Determina si el evento debe ser enviado al escuchador.
		 * @param args Son los argumentos para el escuchador.
		 * @return La instancia quien ejecuta el método.
		 * @see #addListener
		 */
		public function onClick(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(MouseEvent.CLICK, listener, sendEventToListener, args, once);
			return this;
		}
		
		/**
		 * Escucha el evento MouseEvent.MOUSE_OVER
		 * @see #onClick
		 * @return La instancia quien ejecuta el método.
		 */
		public function onMouseOver(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(MouseEvent.MOUSE_OVER, listener, sendEventToListener, args, once);
			return this;
		}
		
		/**
		 * Escucha el evento MouseEvent.MOUSE_OUT
		 * @see #onMouseOver
		 * @return La instancia quien ejecuta el método.
		 */
		public function onMouseOut(listener:Function, sendEventToListener:Boolean = false, args:Array = null, once:Boolean = false):*
		{
			addListener(MouseEvent.MOUSE_OUT, listener, sendEventToListener, args, once);
			return this;
		}
		
		/**
		 * Registra un escuchador de eventos
		 * @param type El tipo de evento.
		 * @param listener El escuchador del evento.
		 * @param sendEventToListener Determina si el evento debe ser enviado al escuchador.
		 * 		  De forma predeterminada el evento es enviado al escuchador, pero esto puede ser opcional.
		 * 		  Determinar este parametro como false puede resultar útil cuando se requiera ejecutar una función que no espere necesariamente un evento
		 * @param args Son los argumentos para el escuchador.
		 * 		  Si el parámetro sendEventToListener es true este es agrega de primero a los argumentos de forma que el evento siempre es el primer argumento.
		 * @throws Error Listener can't be a 'null' object
		 * @return La instancia quien ejecuta el método.
		 */
		public function addListener(type:String, listener:Function, sendEventToListener:Boolean = true, args:Array = null, once:Boolean = false):*
		{
			if (listener == null)
				throw new Error("Listener can't be a 'null' object");
			
			// Create listener's holder
			if (!_listeners) 
				_listeners = new Dictionary(true);
			
			// Clear listener if exist
			var index:int = findListenerIndex(type, listener);
			if (index != -1) 
			{
				trace("Replacing listener with type:", type, "at index:", index);
				removeListenerAt(type, index);
			}
			
			var listenerInfo = {listener:listener,
								send:sendEventToListener,
								args:args,
								once:once};
														
			var handler = function (event:*):void
			{
				if (listenerInfo.send)
				{
					// Create the argument objet
					if (!listenerInfo.args) listenerInfo.args = [];
					
					if (listenerInfo.event)
					{
						// replace the event if exists
						listenerInfo.args[0] = event;
					}
					else
					{
						// add the event as the first argument
						listenerInfo.event = true;
						listenerInfo.args.unshift(event);
					}
				}
				
				// Call the listener
				listenerInfo.listener.apply(null, listenerInfo.args);
				
				if (listenerInfo.once)
				{
					removeListener(type, listenerInfo.listener);
				}
			}
			
			// Save Event Handler
			listenerInfo.handler = handler;
			
			
			if (!_listeners[type])
			{
				// create and array for key type
				_listeners[type] = [];
			}
			
			// Push listener					
			_listeners[type][_listeners[type].length] = listenerInfo;
			
			// Nota: Analizar si es necesario la referencia devil: false, 0, true
			this.addEventListener(type, handler);
			
			return this;
		}
		
		/**
		 * Agrega un escuchador, cuando el evento se desencadene este se removerá automáticamente
		 * @private
		 * @return  
		 */
		public function addListenerOnce(type:String, listener:Function, sendEventToListener:Boolean = true, args:Array = null):*
		{
			addListener(type, listener, sendEventToListener, args, true);
			return this;
		}
		
		/**
		 * Borra un escuchador previamente registrado
		 * @return La instancia quien ejecuta el método 
		 */
		public function removeListener(type:String, listener:Function):*
		{
			if (!_listeners[type]) return;
			
			var index:int = findListenerIndex(type, listener);
			if (index == -1) 
			{
				trace("Ese listener no esta asociado a este objeto");
				return this;
			}

			var listenerInfo:Object = _listeners[type][index];
			
			_listeners[type].splice(index, 1);
			this.removeEventListener(type, listenerInfo.handler);
			listenerInfo = null;
			
			return this;
		}
		
		/**
		 * Borra todos los escuchadores previamente registrados
		 * @return La instancia quien ejecuta el método 
		 */
		public function removeListeners():*
		{
			var i:uint = 0;
			for (var type:String in _listeners)
			{
				i = _listeners[type].length;
				while (i--)
				{
					removeListenerAt(type, i);
				}
				delete _listeners[type];
			}
					
			_listeners = null;
			
			return this;
		}
		
		/**
		 * Verifica si un escuchador ya esta registrado
		 * @param listener Escuchador
		 * @return Boolean 
		 */
		public function listenerIsAdded(type:String, listener:Function):Boolean
		{
			return findListenerIndex(type, listener) != -1;
		}
		
		/**
		 * @private
		 */
		public function destroy():void
		{
			removeListeners();
		}
		
		/**
		 * Agrega varios eventos para un solo escuchador
		 */
		public function addEventsTolistener(listener:Function, ...events):*
		{
			for (var p:String in events)
			{
				addListener(events[p], listener);
			}
			
			return this;
		}
		
		/**
		 * Agrega un evento en uno ó más escuchadores
		 * @private
		 */
		public function addListenersToEvent(event:String, ...listeners):*
		{
			for (var p:String in listeners)
			{
				addListener(event, listeners[p]);
			}
			
			return this;
		}
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------
		
		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------
		
		/**
		 * Retorna la posición (indice) en la que se encuetra registrado el escuchador.
		 * @param listener Function
		 * @private
		 * @return -1 si no esta registrado. 
		 */
		private function findListenerIndex(type:String, listener:Function):int
		{
			if (_listeners[type])
			{
				var i:uint = _listeners[type].length;
				while (i--)
				{
					if (_listeners[type][i].listener == listener) return i;
				}
			}
			
			return -1;
		}
		
		/**
		 * Borra un escuchador por su posición 
		 * @param index uint
		 * @private
		 */
		private function removeListenerAt(type:String, index:uint):void
		{
			removeListener(type, _listeners[type][index].listener);
		}
	}
}