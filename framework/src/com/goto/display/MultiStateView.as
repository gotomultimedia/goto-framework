//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.display
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	import com.goto.display.View;
	//import com.goto.utils.Console;
	
	/**
	 * Generic Component Subclass
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author fenixkim
	 * @since  17.10.2011
	 */
	public class MultiStateView extends View 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function MultiStateView() 
		{
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _states:Object;
		protected var _currentState:String = "normal";
		protected var _view:ScalableObject;
		protected var _innerRect:Object;
		protected var _outerRect:Object;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function registerState(stateName:String, skin:Object):*
		{
			if (!_states) _states = {};
			_states[stateName] = skin;
			
			return this;
		}
		
		/**
		 * @private
		 */
		public function getState(stateName:String):*
		{
			if (!_states) 
				return null; 
				
			return _states[stateName];
		}
		
		/**
		 * @private
		 */
		public function setRectangles(inner:Rectangle, outer:Rectangle = null, needsLayout:Boolean = true):*
		{
			if (!inner && !outer) 
				return this;
			
			_innerRect = inner;
			_outerRect = outer;
			
			_view.setRectangles(_innerRect, _outerRect, false);
			if (needsLayout) draw();
			
			return this;
		}
		
		/**
		 * Cambia el estado del componente, ejemplo: normal, over, focus dependiendo del tipo de componente
		 * @return  
		 */
		public function changeState(stateName:String):*
		{
			var state:* = getState(stateName);
			if (!state) return;
			
			_view.changeSkin(getState(stateName), _innerRect, _outerRect);
			_currentState = stateName;
			
			draw();
			
			return this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function destroy():void
		{
			_states = null;
			_view.destroy();
			super.destroy();
		}
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_view = new ScalableObject().addInto(this);
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			_view.setSize(__width, __height);
			
			super.draw();
		}
				
	}
}