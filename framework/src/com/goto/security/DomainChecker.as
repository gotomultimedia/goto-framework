//AS3///////////////////////////////////////////////////////////////////////////
// 
// Copyright 2011 goTo! Multimedia
// 
////////////////////////////////////////////////////////////////////////////////

package com.goto.security
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import com.goto.display.components.GenericComponent;
	//import com.goto.utils.Console;
	
	/**
	 * Verifies if the main DisplayObject is executed from a secure domain
	 *
	 * @langversion ActionScript 3.0
	 * @playerversion Flash 9.0
	 * 
	 * @author Silverfenix
	 * @since  22.08.2011
	 */
	public class DomainChecker extends GenericComponent 
	{
		//---------------------------------------
		// CLASS CONSTANTS
		//---------------------------------------
		
		//--------------------------------------
		//  CONSTRUCTOR
		//--------------------------------------

		/**
		 *	@constructor
		 */
		public function DomainChecker(reference:*, domains:String, showDisclaimer:Boolean = true) 
		{
			_reference = reference;
			_secureDomains = domains;
			_showDisclaimer = showDisclaimer;
			
			super();
		}
		
		//---------------------------------------
		// PRIVATE & PROTECTED VARIABLES
		//---------------------------------------
		
		protected var _disclaimer:Disclaimer;
		protected var _secureDomains:String = "";
		protected var _showDisclaimer:Boolean;
		protected var _reference:*;
		protected var _isLegal:Boolean;
		
		//--------------------------------------
		//  GETTER/SETTERS
		//--------------------------------------

		public function get isLegal():Boolean
		{	
			_isLegal = validate();
			return _isLegal;
		}
		
		public function get isIllegal():Boolean
		{	
			return (_isLegal = validate()) == false;
		}
		
		//--------------------------------------
		//  PUBLIC METHODS
		//--------------------------------------
		
		/**
		 * @private
		 */
		public function validate():Boolean
		{
			if (!_reference) 
				throw new Error("Init First");
				
			var url:String = _reference.stage.loaderInfo.url;
			var pattern:RegExp = new RegExp(_secureDomains, "g");			
			return (url.match(pattern).length > 0);
		}
		
		public static function init(reference:*, domains:String, showDisclaimer:Boolean = true):DomainChecker
		{
			return new DomainChecker(reference, domains, showDisclaimer);
		}
		
		//--------------------------------------
		//  EVENT HANDLERS
		//--------------------------------------

		//--------------------------------------
		//  PRIVATE & PROTECTED INSTANCE METHODS
		//--------------------------------------

		//---------------------------------------
		// PROTECTED METHODS
		//---------------------------------------
		
		/**
		 * @inheritDoc
		 */
		override protected function onStage():void
		{
			super.onStage();

			stage.addEventListener(Event.RESIZE, resize, false, 0, true);
			resize();
		}
		
		/**
		 * @inheritDoc
		 */
		override protected function onComponentReady():void
		{
			super.onComponentReady();
			
			if (!isLegal && _showDisclaimer)
			{
				_reference.stage.addChild(this);
			}
		}
		/**
		 * add subviews
		 */
		override protected function addChildren():void
		{
			super.addChildren();
			
			//Add subviews here
			_disclaimer = new Disclaimer;
			addChild(_disclaimer);
		}
		
		/**
		 * Redraw views
		 */
		override public function draw ():void
		{
			super.draw();
			
			_disclaimer.x = int((__width - _disclaimer.width) >> 1);
			_disclaimer.y = int((__height - _disclaimer.height) >> 1);
		}
		
		private function resize(e:Event = null):void
		{
			setSize(_reference.stage.stageWidth, _reference.stage.stageHeight);
		}		
	}
}

