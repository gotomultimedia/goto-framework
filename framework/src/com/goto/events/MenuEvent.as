/*
MenuEvent.as


Created by Alexander Ruiz Ponce on 23/12/09.
Copyright 2009 goTo! Multimedia. All rights reserved.
*/
package com.goto.events 
{	
	import flash.events.Event;
	
    public class MenuEvent extends CustomEvent
    {	
		public static const ITEM_CLICKED:String = "itemClicked";
   
        public function MenuEvent(_type:String, _params:Object = null, _bubbles:Boolean = false, _cancelable:Boolean = false)
        {
            super(_type, _params, _bubbles, _cancelable);
        }
		
        override public function clone():Event
        {
            return new MenuEvent(type, params, bubbles, cancelable);
        }
       
        override public function toString():String
        {
            return formatToString("MenuEvent", "params", "type", "bubbles", "cancelable");
        }
    }
}