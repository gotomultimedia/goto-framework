/*
CustomEvent.as


Created by Alexander Ruiz Ponce on 23/12/09.
Copyright 2009 goTo! Multimedia. All rights reserved.
*/
package com.goto.events
{
    import flash.events.Event;
   
    public class CustomEvent extends Event
    {
		public var params:Object;
		   
        public function CustomEvent(_type:String, _params:Object = null, _bubbles:Boolean = false, _cancelable:Boolean = false)
        {
            super(_type, _bubbles, _cancelable);
            params = _params;
        }
		
        override public function clone():Event
        {
            return new CustomEvent(type, params, bubbles, cancelable);
        }
       
        override public function toString():String
        {
            return formatToString("CustomEvent", "params", "type", "bubbles", "cancelable");
        }
    }
}