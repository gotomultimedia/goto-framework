
0.5.5 / 2012-10-01 
==================

  * Added SWFAddress and MacoMouseWheel into the framework
  * Removed vPlayer_160.swc and fixed the import path of vPlayer for ReelVideoItem
  * Merge branch 'hotfix/0.5.4' into develop
  * Fixed a little bug in the package of VPlayer
  * Merge branch 'release/0.5.3' into develop
  * Merge branch 'release/0.5.3'
  * Bumped version to 0.5.3
  * Added VPlayer.swc to framework libs
  * Merge branch 'release/0.5.2'
  * Merge branch 'release/0.5.2' into develop
  * Bumped History to 0.5.2
  * Added Kenburnslider, ImageViewer, ADViewer and reel extensions
  * Removed Stats and ScaleBitmap git submodules and added it as static files as dependencies.
  * Updated History.md
  * Added History.md

0.5.3 / 2012-09-22 
==================

  * Added VPlayer.swc to framework libs
  * Merge branch 'release/0.5.2' into develop
  * Bumped History to 0.5.2
  * Added Kenburnslider, ImageViewer, ADViewer and reel extensions
  * Removed Stats and ScaleBitmap git submodules and added it as static files as dependencies.
  * Updated History.md
  * Added History.md

0.5.0 / 2012-09-22 
==================

  * Added submodule Stats (gist 3750454)
  * Remove Stats classes
  * Added ScaleBitmap Submodule
  * Added Readme files
  * initial commit