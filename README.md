# gT Framework
This framework is cool for create ActionsScript 3.0 Templates and AS3 Applications

## Features
* Chain methods.
* Extended Sprite features throw a custom View component.
* TweenMax integrated in View by default. 
* Layout.
* A bunch of utilities.
* Easy destroy and garbage collection.
* Light weight

## Dependencies
* Greensock's tween engine ([TweenMax](http://tweenmax.com))
* Mr.doob's [HI-res-stats](https://github.com/mrdoob/Hi-ReS-Stats)
* [ScaleBitmap](https://github.com/mrdoob/Hi-ReS-Stats)
* Asual's SWFAddress
* Pixelbraker's MacMouseWheel